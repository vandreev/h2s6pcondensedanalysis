Velocity-extrapolated 2S(J=1/2,F=0)-6P(J=3/2,F=1) frequency, corrected for Light Force Shift, Second-order Doppler Shift and Quantum Interference (Big Model):
nu_e = 730690517826679(601) Hz
Correcting for recoil shift of -1176027 Hz and correcting for dc-Stark shift of -20 Hz and BBR-induced shift of 250 Hz, adding systematic uncertainty of 281 Hz in quadrature: 
nu_e = 730690516650882(663) Hz
Hyperfine centroid value obtained from above value with -132620165.2 Hz correction: 
nu_e = 730690384030716(663) Hz
