# Condensed data set of hydrogen 2S-6P 2019 measurement campaign

- [Condensed data set of hydrogen 2S-6P 2019 measurement campaign](#condensed-data-set-of-hydrogen-2s-6p-2019-measurement-campaign)
  - [Description of data files](#description-of-data-files)
    - [Metadata](#metadata)
      - [Data group metadata](#data-group-metadata)
      - [Freezing cycles (FCs) metadata](#freezing-cycles-fcs-metadata)
      - [Metadata of experimental scans](#metadata-of-experimental-scans)
      - [Boolean masks for experimental scan selection](#boolean-masks-for-experimental-scan-selection)      
    - [Experimental scans](#experimental-scans)
    - [Simulated line shapes](#simulated-line-shapes)
    - [Simulated delay speeds](#simulated-delay-speeds)
  - [Example analysis](#example-analysis)
  - [References](#references)  

This is the description of the condensed data set of the hydrogen 2S-6P 2019 measurement campaign ("H2S6P2019") at the Laser Spectroscopy Division of the Max Planck Institute of Quantum Optics (MPQ).

The condensed data set was exported from the main data analysis located at [H2S6P2019](https://gitlab.mpcdf.mpg.de/lmaisen/H2S6P2019), using the script [export_condensed_data_set.py](https://gitlab.mpcdf.mpg.de/lmaisen/H2S6P2019/-/blob/main/export_condensed_data_set.py).

The condensed data set, along with this README, are stored in the Git repository [H2S6P2019Condensed](https://gitlab.mpcdf.mpg.de/lmaisen/h2s6p2019condensed).

Created by Lothar Maisenbacher/MPQ (<lothar.maisenbacher@mpq.mpg.de>). Last updated on 2023-12-06.

## Changelog

### 2023-12-22

- **Added**: AOM RF frequencies of 2S-6P spectroscopy laser for each experimental scan point.

- **Added**: Angle of detector cylinder in metadata of experimental scans.

### 2023-12-04

- Initial release.

## Description of data files

All data files are comma-separated text (CSV) files.

For example, using Python, they can be read using the [pandas](https://pandas.pydata.org/) package:
```
import pandas as pd

# Path to CSV file
filepath = 'test.csv'

# Index column(s):
# Either None to generate numerical index, the name of the column to use as index,
# or a list of columns to generate a multiindex from.
index_columns = None

# Explicit definition of column dtypes (data types):
# Either None to automatically deduce from the CSV file for all columns,
# a dtype to use for all columns,
# or a dict of shape `{column_name: dtype}`
# to assign dtype `dtype` to column `column_name`.
column_dtypes = None

# Read CSV file into pandas DataFrame `df`
df = pd.read_csv(filepath, index_col=index_columns, dtype=column_dtypes)
```

### Metadata

#### Data group metadata

The spectroscopy data of the hydrogen 2S-6P 2019 measurement campaign are subdivided into measurement runs and data groups. See Section 6.1 of [\[1\]][1] for details. In particular, Table 6.1 and Table 6.2 therein list the measurement runs and data groups, respectively.

The metadata of all data groups are stored in the single CSV file "Metadata/datagroups.csv". There is a single entry (or row) per data group.

Using Python, read this file into the DataFrame `dfDataGroups` with the column "DataGroupID" as index with:
```
dfDataGroups = pd.read_csv(filepath, index_col='DataGroupID')
# Fill NA values of string columns with empty strings
columns_str = ['DataGroupTags', 'DataGroupComments']
dfDataGroups[columns_str] = dfDataGroups[columns_str].fillna("")
```

These metadata are taken from [H2S6P2019Metadata/2S-6P 2019 measurement_datagroups.xlsx](https://gitlab.mpcdf.mpg.de/lmaisen/h2s6p2019metadata/-/blob/main/2S-6P%202019%20measurement_datagroups.xlsx).

| Column | Type | Unit | Description  |
|---|---|---|---|
| DataGroupID | Str | | Name of data group. |
| MeasRun | Str | | Measurement run (A, B, or C). |
| Isotope | Str | | Hydrogen isotope; here always "H" for atomic hydrogen. |
| FS | Str | | Upper-level fine-structure component of interrogated 2S-*n*P transition. Here either "6P12" or "6P32" for the 2S-6P$_{1/2}$ or 2S-6P$_{3/2}$ transition, respectively. |
| AlphaOffset | Float | mrad | Offset angle $\alpha_0$ between the atomic beam and the 2S-*n*P spectroscopy laser beams. |
| ThetaL | Float | deg | Linear laser polarization angle $\theta_\mathrm{L}$: angle between the polarization vector of the 2S-6P spectroscopy laser beams and the axis of the detector cylinder. |
| 2SnPLaserPower | Float | μW | 2S-6P spectroscopy laser power $P_\mathrm{2S-6P}$. This is the power per beam (forward- and backward-traveling) at the interaction point with the atoms. |
| ScanType | Str | | Scan tpye: either "Precision" for data groups where $\alpha_0$ = 0 mrad or "PrecisionAlphaOffset" where $\alpha_0 \neq$ 0 mrad. |
| FitFuncID | Str | | ID of the line shape function used. "Voigt" for Voigt line shape function and "VoigtDoublet" for Voigt doublet line shape function. |
| AFRBackreflectionMinCutoff | Float | V | Cutoff voltage of backreflection APD (avalanche photodetector) of 2S-6P spectroscopy laser, see Fig. 4.15 of [\[1\]][1]. Scans containing points with a signal on this detector below the cutoff voltage are excluded. |
| 2SnPPMTMinCutoff | Float | V | Cutoff voltage of four-quadrant PMT (photomultiplier tube) sum of 2S-6P spectroscopy laser, see Fig. 4.15 of [\[1\]][1]. Scans containing points with a signal on this detector below the cutoff voltage are excluded. |
| SpeedDistExpSupprCutOff_Sim | Float | m/s | Cutoff speed $v_\mathrm{cutoff}$ used in simulation of atomic beam, from which the delay speeds are derived. See Sections 4.5.2.2 and 5.2 of [\[1\]][1]. |
| SpeedDistExpSupprCutOff_Mean | Float | m/s | Mean cutoff speed $v_\mathrm{cutoff}$ of experimental data. See Fig. 6.1 of [\[1\]][1]. |
| SpeedDistExpSupprCutOff_Sigma | Float | m/s | (Scaled) standard deviation of cutoff speed $v_\mathrm{cutoff}$ of experimental data. |
| SpeedDistExpSupprCutOff_Min | Float | m/s | Minimum value of $v_\mathrm{cutoff}$ of experimental data. |
| SpeedDistExpSupprCutOff_Max | Float | m/s | Maximum value of $v_\mathrm{cutoff}$ of experimental data. |
| CountWeightingRelOffset | Float | | The relative offset applied to the simulated line shapes to match the offsets observed in the experimental data. The offset is applied relative to the peak of the simulated line shapes. |
| DataGroupTags | Str | | Data group tags. See [H2S6P2019Metadata/def_tags.xlsx](https://gitlab.mpcdf.mpg.de/lmaisen/h2s6p2019metadata/-/blob/main/def_tags.xlsx) for tag definitions. |
| DataGroupComments | Str | | Data group comments. |

#### Freezing cycles (FCs) metadata

The spectroscopy data were taken in batches referred to as freezing cycles (FCs), see Section 4.1 of [\[1\]][1] for details. For each data group, the metadata of the contributing FCs are stored in a single CSV file "`DataGroupID`/Metadata/`DataGroupID`_fcs.csv".

Note that a single FC can contain data for multiple data groups.

Using Python, read a single such file into the DataFrame `dfFCDays` with the column "FCUID" as index with:
```
dfFCDays = pd.read_csv(filepath, index_col='FCUID')
# Fill NA values of string columns with empty strings
columns_str = ['FCName', 'FCTags', 'FCComments', 'DayTags', 'DayComments']
dfFCDays[columns_str] = dfFCDays[columns_str].fillna("")
```

These metadata are taken from [H2S6P2019Metadata/2023-10-06_2S-6P 2019 measurement_fcs.xlsx](https://gitlab.mpcdf.mpg.de/lmaisen/h2s6p2019metadata/-/blob/main/2023-10-06_2S-6P%202019%20measurement_fcs.xlsx) and [H2S6P2019Metadata/2S-6P 2019 measurement_days.xlsx](https://gitlab.mpcdf.mpg.de/lmaisen/h2s6p2019metadata/-/blob/main/2S-6P%202019%20measurement_days.xlsx).

| Column | Type | Unit | Description  |
|---|---|---|---|
| FCUID | Str | | Unique ID of FC. |
| FCName | Str | | Name of FC. Only assigned to FCs that contribute to the main data analysis. |
| MeasDate | Datetime | | Measurement date in the format YYYY-MM-DD. |
| FC | Int | | FC sequence number, starting from 1 on each measurement day. |
| MeasRun | Str | | Measurement run (A, B, or C). |
| Isotope | Str | | Hydrogen isotope; here always "H" for atomic hydrogen. |
| FS | Str | | Upper-level fine-structure component of interrogated 2S-*n*P transition. Here either "6P12" or "6P32" for the 2S-6P$_{1/2}$ or 2S-6P$_{3/2}$ transition, respectively. |
| FC1S2SLaserPowerMean | Float | W | Measured mean value of power of 1S-2S preparation laser $P_\mathrm{1S-2S}$ during precision scans (circulating power per direction). |
| FC1S2SLaserPowerMin | Float | W | Measured minimum value of $P_\mathrm{1S-2S}$ during precision scans. |
| FC1S2SLaserPowerMax | Float | W | Measured maximum value of $P_\mathrm{1S-2S}$ during precision scans. |
| H2Flow | Float | ml/min | Nominal value of flow (rate) of molecular hydrogen into dissociator (see Section 4.5.1 of [\[1\]][1]). |
| FCDuration | Float | s | Duration of FC. |
| TimestampFCMin | Datetime | | Start time of FC in the format YYYY-MM-DD HH-mm-ss. |
| TimestampFCMax | Datetime | | End time of FC in the format YYYY-MM-DD HH-mm-ss. |
| FCNScansPrecision | Int | | Number of scans of type "Precision" in this FC. |
| FCNScansPrecisionAlphaOffset | Int | | Number of scans of type "PrecisionAlphaOffset" in this FC. |
| FCTags | Str | | FC tags. See [H2S6P2019Metadata/def_tags.xlsx](https://gitlab.mpcdf.mpg.de/lmaisen/h2s6p2019metadata/-/blob/main/def_tags.xlsx?ref_type=heads) for tag definitions. |
| FCComments | Str | | FC comments. |
| TimestampCryopumpOn1 | Datetime | | Switch on time of cryopump in the format YYYY-MM-DD HH-mm-ss. |
| TimestampCryopumpOff1 | Datetime | | Switch off time of cryopump in the format YYYY-MM-DD HH-mm-ss. |
| TimestampCryopumpValveClosed | Datetime | | Time at which bypass valve between cryopump (inner high-vacuum region) and main chamber (outer vacuum region) was closed, in the format YYYY-MM-DD HH-mm-ss. |
| TimestampH2ValvesOpened | Datetime | | Time at which valve controlling flow of molecular hydrogen into dissociator was opened, in the format YYYY-MM-DD HH-mm-ss. |
| TimestampH2ValvesOpened | Datetime | | Time at which valve controlling flow of molecular hydrogen into dissociator was opened, in the format YYYY-MM-DD HH-mm-ss. |
| NozzleType | Str | | Nozzle type. Always "1+2 mm isolated t-shaped nozzle" for the precision data of the 2S-6P 2019 campaign, see Section 4.5.2.1 of [\[1\]][1]. |
| NozzleFeedInsulation | Str | | Material and installation date (format: "<MATERIAL>YYYYMMMDD") of spacer between PTFE tubing and copper nozzle, see Section 4.5.2.1 of [\[1\]][1]. |
| NozzleShimThickness | Float | μm | Thickness of stainless steel shim between copper nozzle and cryostat cold head. |
| DetectorType | Str | | Detector configuration, see Section 4.6.1 of [\[1\]][1]. |
| ThetaL_Slow | Float | deg | Angle between the polarization of the slow axis of the AFR (active fiber-based retroreflector) fiber and the detector cylinder, see Section 4.4.2 of [\[1\]][1]. |
| ThetaL_Fast | Float | deg | Angle between the polarization of the fast axis of the AFR (active fiber-based retroreflector) fiber and the detector cylinder, see Section 4.4.2 of [\[1\]][1]. |
| DayNozzleTemp | Float | K | Nominal value of the nozzle temperature $T_\mathrm{N}$ on this measurement day. |
| 1S2SLaserPowerStabilized | Bool | | If True, the power of the 1S-2S preparation laser $P_\mathrm{1S-2S}$ was stabilized on this measurement day. False otherwise. See Section 4.3.3.5 of [\[1\]][1]. |
| Day1S2SLaserPower | Float | W | The power $P_\mathrm{1S-2S}$ to which the 1S-2S preparation laser was stabilized on this measurement day, if applicable. |
| DayTags | Str | | Day tags. See [H2S6P2019Metadata/def_tags.xlsx](https://gitlab.mpcdf.mpg.de/lmaisen/h2s6p2019metadata/-/blob/main/def_tags.xlsx?ref_type=heads) for tag definitions. |
| DayComments | Str | | Day comments. |

#### Metadata of experimental scans

For each data group, the metadata of the experimental scans contained in that data group are stored in a single CSV file "`DataGroupID`/Metadata/`DataGroupID`_scans.csv".

Using Python, read a single such file into the DataFrame `dfAll_scans` with the column "ScanUID" as index with:
```
dfAll_scans = pd.read_csv(filepath, index_col='ScanUID', dtype={'dfScans_ExpParams_FCUID': str})
```

These metadata are taken from the DataFrames `dfAll_scans_a` and `dfAll_scans_sd_a` used in the main data analysis [H2S6P2019](https://gitlab.mpcdf.mpg.de/lmaisen/H2S6P2019).

| Column | Type | Unit | Description  |
|---|---|---|---|
| ScanUID | Str | | Unique ID of scan. |
| dfScans_Timestamp | Datetime | | Time of scan, in the format YYYY-MM-DD HH-mm-ss.ffffff. |
| dfScans_ScanDID | Int | | Day ID of the scan, that is, an integer number that starts at `D`00 for every measurement day, where `D` is the day of the month without a leading zero. |
| dfScans_ExpParams_FCUID | Str | | Unique ID of FC. |
| dfScans_ExpParams_ChExcl_Ch0 | Bool | | If True, the data from the top detector ("Ch0") are excluded for this scan. |
| dfScans_ExpParams_ChExcl_Ch1 | Bool | | If True, the data from the bottom detector ("Ch1") are excluded for this scan. |
| dfScans_ExpParams_AlphaOffset | Float | mrad | Offset angle $\alpha_0$ between the atomic beam and the 2S-*n*P spectroscopy laser beams. |
| dfScans_ExpParams_ThetaL | Float | deg | Linear laser polarization angle $\theta_\mathrm{L}$: angle between the polarization vector of the 2S-6P spectroscopy laser beams and the axis of the detector cylinder. |
| dfScans_ExpParams_2SnPLaserPower | Float | μW | 2S-6P spectroscopy laser power $P_\mathrm{2S-6P}$. This is the power per beam (forward- and backward-traveling) at the interaction point with the atoms. |
| dfData_Motor_AlphaAlign_Pos | Float | mm | Position of $\alpha_0$ alignment actuator, see Section 4.1 of [\[1\]][1]. |
| dfData_ExpParams_AlphaAlign_Angle | Float | mrad | Angle of detector cylinder derived from position of $\alpha_0$ alignment actuator, see Section 4.1 of [\[1\]][1]. |
| dfCombFreqs_1S2SF0_Freq | Float | Hz | Frequency-comb-corrected detuning $\Delta\nu^{}_{\mathrm{1S-2S}}$ of 1S-2S preparation laser from unperturbed 1S-2S resonance. |
| dfData_ExpParams_1S2SLaserPower | Float | W | Mean value of power of 1S-2S preparation laser $P_\mathrm{1S-2S}$ during scan (circulating power per direction). |
| dfData_ExpParams_1S2SLaserPower_ScanSD | Float | W | Standard deviation of power of 1S-2S preparation laser $P_\mathrm{1S-2S}$ during scan (circulating power per direction). |
| dfData_NozzleTemp | Float | K | Mean value of nozzle temperature $T_\mathrm{N}$ during scan (circulating power per direction). |
| dfData_NozzleTemp_ScanSD | Float | K | Standard deviation of nozzle temperature $T_\mathrm{N}$ during scan (circulating power per direction). |
| dfData_PD2SnPAFR_WinAvg | Float | V | Mean value during scan of output voltage of backreflection APD (avalanche photodetector) of 2S-6P spectroscopy laser, see Fig. 4.15 of [\[1\]][1]. |
| dfData_PD2SnPAFR_WinAvg_ScanSD | Float | V | Standard deviation during scan of output voltage of backreflection APD (avalanche photodetector) of 2S-6P spectroscopy laser, see Fig. 4.15 of [\[1\]][1]. |
| dfData_PD2SnPIntStab2_WinAvg | Float | V | Mean value during scan of output voltage of four-quadrant PMT (photomultiplier tube) sum of 2S-6P spectroscopy laser, see Fig. 4.15 of [\[1\]][1]. |
| dfData_PD2SnPIntStab2_WinAvg | Float | V | Standard deviation during scan of output voltage of four-quadrant PMT (photomultiplier tube) sum of 2S-6P spectroscopy laser, see Fig. 4.15 of [\[1\]][1]. |

#### Boolean masks for experimental scan selection

To make the selection of valid scan-detector-delay combinations somewhat easier, boolean masks that are indexed by unique scan ID ("ScanUID"), detector ID ("MCSchannelID"), and delay ID ("DelayID") are provided.

Two masks are provided per data group:

- "`DataGroupID`/Metadata/`DataGroupID`\_scans_valid_mask.csv": valid scan-detector-delay combinations according to the exclusion criteria of the data group and scan metadata. For invalid data according to this mask, the counts have been set to -1 in the corresponding data files.

- "`DataGroupID`/Metadata/`DataGroupID`\_scans_valid_main_analysis_mask.csv": same as above, but also excluding scan-detector-delay combinations for which the line shape fit did not converge in the main data analysis. For invalid data according to this mask, the counts are *not* set to -1.

Using Python, read a single such file into the DataFrame `dfMask` with a multindex ("ScanUID", "MCSchannelID", "DelayID") with:
```
dfMask = pd.read_csv(filepath, index_col=['ScanUID', 'MCSchannelID', 'DelayID'], dtype={'DelayID': str})
```
The boolean value of each scan-detector-delay combination can then be retrieved with `dfMask.loc[(ScanUID, MCSchannelID, DelayID)]`

| Column | Type | Unit | Description  |
|---|---|---|---|
| ScanUID | Str | | Unique ID of scan. |
| MCSchannelID | Str | | Detector ID, either "Ch0" for top detector or "Ch1" for bottom detector. |
| DelayID | Str |  | Name of delay. For the delays used in the analysis, the names are integers between 1 and 16. |
| Value | Bool |  | True if the scan is included in the mask, False otherwise. |

### Experimental scans

The data for each experimental scan are saved in a single CSV file. The filenames have the format "`DataGroupID`\_`FCUID`\_`ScanDID`_exp.csv". The files are stored in the subdirectory "`DataGroupID`/Exp".

Using Python, read a single such file into the DataFrame `scan_data_exp` with:
```
scan_data_exp = pd.read_csv(filepath)
```

| Column | Type | Unit | Description  |
|---|---|---|---|
| Freq_Blind | Float | Hz | Comb-corrected frequency of 2S-6P spectroscopy laser. A blind offset has been added. |
| Detuning | Float | Hz | Detuning of the 2S-6P spectroscopy laser from the assumed resonance frequency. |
| Freq_AOM2S6P_Freq | Float | Hz | RF frequency of 2S-6P scan AOM ($f_\mathrm{FP3,Scan}$), see Section 4.4.1 and Fig. 4.14 of [\[1\]][1]. The AOM is operated in the +1st diffraction order and is doubled-passed, i.e., the resulting frequency shift of the laser light is twice the RF frequency. |
| Freq_AOM2S6PFSOffset_Freq | Float | Hz | RF frequency of 2S-6P fine-structure AOM ($f_\mathrm{FP3,FS}$), see Section 4.4.1 and Fig. 4.14 of [\[1\]][1]. The AOM is operated in the +1st diffraction order. |
| Ch0D*i* | Int | cts | Counts on top detector ("Ch0") for delay *i*, where *i* runs between 1 and 16. Set to -1 if no signal available for this scan and detector. |
| Ch1D*i* | Int | cts | Counts on bottom detector ("Ch1") for delay *i*, where *i* runs between 1 and 16. Set to -1 if no signal available for this scan and detector. |

### Simulated line shapes

For each data group, there is one CSV file each for the simulated line shapes for the big model (BM, `SimID` = "BigModel") and for the light force shift (LFS, `SimID` = "LFS") model. The filenames have the format "`DataGroupID`\_`SimID`_sim.csv" and "`DataGroupID`\_`SimID`_offset_sim.csv". For the latter files, an offset mimicking the experimental signal has been added to the simulated signal (see below). The files are stored in the subdirectory "`DataGroupID`/Sim".

Using Python, read a single such file into the DataFrame `scan_data_sim` with:
```
scan_data_sim = pd.read_csv(filepath)
```

| Column | Type | Unit | Description  |
|---|---|---|---|
| Detuning | Float | Hz | Detuning of the 2S-6P spectroscopy laser from the unperturbed resonance frequency. |
| Ch0D*i* | Float | arb. u. | Simulated fluorescence signal for top detector ("Ch0") for delay *i*, where *i* runs between 1 and 16. |
| Ch1D*i* | Float | arb. u. | Simulated fluorescence signal for bottom detector ("Ch1") for delay *i*, where *i* runs between 1 and 16. |

### Simulated delay speeds

For each data group, there is one CSV file containing the simulated delay speeds. The filename has the format "`DataGroupID`_delay_vels.csv". The files are stored in the subdirectory "`DataGroupID`/DelayVels". See Section 5.2 of [\[1\]][1] for details on the underlying simulation, including the delay definitions given in Table 5.4 therein.

Using Python, read a single such file into the DataFrame `df_delay_vels` with the column "DelayID" as index with:
```
df_delay_vels = pd.read_csv(filepath, index_col='DelayID', dtype={'DelayID': str})
```

| Column | Type | Unit | Description  |
|---|---|---|---|
| DelayID | Str |  | Name of delay. For the delays used in the analysis, the names are integers between 1 and 16. |
| V_Mean | Float | m/s | Mean speed $\bar{v}$ of atoms contributing to the fluorescence signal after 2S-6P excitation. |
| V_RMS | Float | m/s | Root mean squared speed $\bar{v}_\mathrm{RMS}$ of atoms contributing to the fluorescence signal after 2S-6P excitation. |

## Example analysis

The analysis is done for each data group separately. The data groups are listed in Table 6.2 of [\[1\]][1].

The data analysis is described in detail in Section 6.1 of [\[1\]][1]. Here, instructions to repeat this analysis are given.

For a given data group, the analysis proceeds as follows:

1. Fit all 16 delays for both detectors for all *N* experimental scans in the data group with the appropriate line shape function. For data groups G1-G12, this is the Voigt line shape (see Section 5.1.1.2 of [\[1\]][1]), while for data groups G13 and G14, the Voigt doublet line shape is used (see Section 5.1.1.3 of [\[1\]][1]). For convenience, Python implementations of these functions can be found in the package [pyhs](https://gitlab.mpcdf.mpg.de/lmaisen/pyhs) as `pyhs.fitfunc.Voigt` and `pyhs.fitfunc.VoigtDoublet`, respectively, along with their partial derivatives `pyhs.fitfunc.Df_Voigt` and `pyhs.fitfunc.Df_VoigtDoublet`.

    The independent variables are the comb-corrected frequencies of the 2S-6P spectroscopy laser (column "Freq_Blind"), and the dependent variables are the counts on the corresponding detector (columns "Ch0D*i*" or "Ch1D*i*"). The square root of the counts is used as uncertainty (and thus weighting) of the counts.

    Note that for some scans, only data from the top detector are available (see Table 6.2 of [\[1\]][1]), because of transient excess scatter and spikes on the bottom detector. For these scans, the column "dfScans_ExpParams_ChExcl_Ch1" of the scan metadata is True and the signal is set to -1. In addition, a small number of fits might not converge.

    This will give a maximum of 16 $\times$ 2 $\times$ *N* fit results, each of which has 5 (7) fit parameters for Voigt (Voigt doublet) line shape. The fit results of the Voigt line shape directly include the resonance frequency $\nu_0$ and its uncertainty $\sigma_{\nu_0}$, while for the Voigt doublet line shape $\nu_0$ and its uncertainty need to be found from the frequencies of the two resonances, $\nu_1$ and $\nu_2$, and their covariances (see Eq. (5.5) of [\[1\]][1]).

2. Similarly, fit all 16 delays for both detectors of the simulated line shape from the big model (BM), and for the simulated line shape from the light force shift (LFS) model (see Section 5.3.1 of [\[1\]][1]).

    The independent variables are the detuning of the 2S-6P spectroscopy laser (column "Detuning"), and the dependent variables are the simulated signal for the corresponding detector (columns "Ch0D*i*" or "Ch1D*i*") with an added offset. This is because, to mimic the treatment of the experimental data as closely as possible, the same square-root weighting should be used for the simulated line shape, which requires the simulated signal to have ideally the same constant offset (or "background") as the experimental data. To approximate this offset, a constant offset corresponding to $c_\mathrm{offset}$ times the maximum simulated signal can be added to the simulated signal. The factor $c_\mathrm{offset}$ is estimated from the relative offset of the fit to the experimental data as $x = y_0/A$. Finally, the square root of the offset simulated signal is used as weights. The values of $c_\mathrm{offset}$ used in the main analysis are given in the column "CountWeightingRelOffset" of the data groups metadata. The offset has already been added to the simulated line shapes in the files with the suffix "_offset".

    This will give 16 $\times$ 2 $\times$ 2 fit results, including the resonance frequencies $\nu_{0,\mathrm{BM}}$ and $\nu_{0,\mathrm{LFS}}$ of the BM and LFS model simulations. Note, since the BM mainly accounts for the quantum interference (QI) effect, the terms BM (the model) and QI (the modeled physical effect) are used interchangeably here.

3. Calculate the second-order Doppler shift $\Delta\nu_\mathrm{SOD}$ (see Section 6.2.4.1 of [\[1\]][1]) for each delay using Eq. (6.10) of [\[1\]][1]. This requires the 2S-6P transition frequency $\nu_{\mathrm{A},0}$, given in Table 2.1 of [\[1\]][1], and the simulated root mean square speed $\bar{v}_\mathrm{RMS}$ of the delays. This gives 16 values of $\Delta\nu_\mathrm{SOD}$.

4. Correct the experimental resonance frequency $\nu_0$ of each scan, detector, and delay for the SOD, QI, and LFS shifts using the appropriate simulation corrections:

    $$\nu_0 \rightarrow \nu_0 - (\Delta\nu_\mathrm{SOD} + \nu_{0,\mathrm{BM}} + \nu_{0,\mathrm{LFS}})$$

     The uncertainty $\sigma_{\nu_0}$ of $\nu_0$ remains unchanged, as this is the statistical uncertainty. The systematic uncertainty from the corrections is taken into account separately and is beyond the scope of this document.

5. Next, find the Doppler-free resonance frequency $\nu_{0,\mathrm{e}}$ and its uncertainty by extrapolating the corrected resonance frequencies $\nu_0$ of each experimental scan for each detector to zero velocity, using the simulated mean speeds $\bar{v}$ (see Section 5.1.2 of [\[1\]][1]). If not all fit results for all 16 delays are available for a given scan, the extrapolation is performed using the available fit results. This will give a maximum of 2 $\times$ *N* values of $\nu_{0,\mathrm{e}}$ and the associated uncertainties $\sigma_{\nu_{0,\mathrm{e}}}$.

6. Now, for each scan, find the average of the two detectors of the values of $\nu_{0,\mathrm{e}}$. However, the values of $\nu_0$, and therefore of $\nu_{0,\mathrm{e}}$, are correlated for the two detectors because of technical noise on the fluorescence signal common to both detectors (see Section 6.1.2.2 of [\[1\]][1]). This correlations needs to be taken into account for the uncertainties of the detector average. To this end, first find the linear correlation coefficient *r* (or Pearson's correlation coefficient) between the values of $\nu_{0,\mathrm{e}}$ of the top detector and the bottom detector.

     Having found *r*, we can do a weighted average over the two detectors for each scan. Let's call the values and uncertainties of $\nu_{0,\mathrm{e}}$ for the top and bottom detector $\nu_\mathrm{a}$, $\sigma_\mathrm{a}$ and $\nu_\mathrm{b}$, $\sigma_\mathrm{b}$, respectively. Their covariance is $\sigma_\mathrm{ab} = r \sigma_\mathrm{a} \sigma_\mathrm{b}$ With the weights $w_\mathrm{a}$ and $w_\mathrm{b} = 1-w_\mathrm{a}$, the value $\nu$ and uncertainty $\sigma$ of the weighted average are given by

     $$\nu = w_\mathrm{a} \nu_\mathrm{a} + w_\mathrm{b} \nu_\mathrm{b}$$

     $$\sigma = \sqrt{w_\mathrm{a}^2 \sigma_\mathrm{a}^2 + w_\mathrm{b}^2 \sigma_\mathrm{b}^2 + 2 w_\mathrm{a} w_\mathrm{b} \sigma_\mathrm{ab}}$$

     The minimum uncertainty is reached for

     $$w_\mathrm{a} = (\sigma_\mathrm{b}^2-\sigma_\mathrm{ab})/(\sigma_\mathrm{a}^2+\sigma_\mathrm{b}^2-2\sigma_\mathrm{ab})$$

     For scans for which only the top detector is available, the results from this detector only are used.

7. We can now start to average the detector-averaged, Doppler-free resonance frequencies $\nu_{0,\mathrm{e}}$ over the scans. First, the scans in each of the *M* freezing cycles (FCs) are averaged. To this end, a weighted average with weights given by $1/\sigma_{\nu_{0,\mathrm{e}}}^2$ is used, as the scans are assumed to be uncorrelated. Because of scan-to-scan excess scatter due to technical noise, the reduced chi-squared $\chi^2_\mathrm{red}$ of this weighted average is typically significantly above 1.

8. Before averaging the FCs, we account for the excess scan-to-scan scatter by scaling up the uncertainty of the FC-average by $\sqrt{\chi^2_\mathrm{red}}$ (for $\chi^2_\mathrm{red} \leq 1$, the uncertainty is not scaled). The reasoning behind this procedure is discussed in Section 6.1.2.3 of [\[1\]][1].

9. Finally, we average the resonance frequency over the FCs. Like the weighted average over the scans in each FC, the FCs are assumed to be uncorrelated.

This procedure is repeated for each data group. A weighted average over the results of data groups G1-G6 & G13-G14 (G7-G12) gives the blinded, partially corrected transition frequency $\tilde{\nu}^{}_{1/2}$ ($\tilde{\nu}^{}_{3/2}$) for the 2S-6P$_{1/2}$ (2S-6P$_{3/2}$) transition. At this point, some corrections, most importantly the recoil shift, still need to be added to retrieve an estimate of the unperturbed transition frequencies.

## References

[1]: https://www.dropbox.com/s/43rkd7qsdl3w4t1/Maisenbacher_Lothar.pdf?dl=0

- \[1\]: [L. Maisenbacher, PhD thesis, LMU Munich (2020)][1]
