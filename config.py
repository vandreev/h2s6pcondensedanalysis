# -*- coding: utf-8 -*-
"""
Created on Tue Mar 12 11:36:21 2024

@author: Vitaly Wirthl
"""

# Foldername where the comb, maser and metadata file are stored
folder_data='data\\'
# Foldername where the condensed H2S-6P dataset is stored
folder_all_groups='data\\h2s6p2019condensed-main\\'


#Switch to use the comb frequencies (i.e. True for unblinded analysis), or blind frequency values if False
use_comb_freq=True
#Switch for plotting laser frequencies from comb data (takes ~1min more time if laser frequencies are plotted for each day).
plot_comb_freq=True
#Switch to correct the laser frequencies for the maser drift.  Note that the calculated and plotted laser frequencies in ..\results\comb are NOT corrected by the maser drift. The maser correction takes place directly when the frequency values are read out for each line scan (i.e. in script01_fit_scans_and_sim, search for "if correct_for_maser:")
correct_for_maser=True
#Switch for possible plotting of maser data for each day (~30sec extra time)
plot_individual_maser_days=True
#Offset substracted for the result if the comb frequencies are used:
nu2s6p_offset_12=730690111486290+1176026
nu2s6p_offset_32=730690516650910+1176027

#### Datagroups to be analyzed: ###
## All datagroups 2S-6P12, without LFS groups:
#datagroups = ['G1A','G1B','G1C','G2','G3','G4','G5','G6']
## All datagroups 2S-6P32:
#datagroups = ['G7A','G7B','G8','G9','G10','G11','G12']
## All datagroups 2S-6P12+2S-6P32, without LFS groups:
#datagroups = ['G1A','G1B','G1C','G2','G3','G4','G5','G6','G7A','G7B','G8','G9','G10','G11','G12']
## LFS groups 2S-6P12:
# datagroups = ['G13','G14']
## All datagroups 2S-6P12+2S-6P32, including LFS groups:
datagroups = ['G1A','G1B','G1C','G2','G3','G4','G5','G6','G7A','G7B','G8','G9','G10','G11','G12','G13','G14']

#Testgroup with small amount of data for testing purposes:
#datagroups = ['G4'] 

# Specifying which datagroups are light force shift measurement datagroups (then it will be fitted with a Voigt Doublet)
LFSgroups = ['G13','G14']
# Specifying which datagroups measure the 2S6P32 fine structure component (others are then 2S6P12)
FS6P32groups = ['G7A','G7B','G8','G9','G10','G11','G12']


##### In the following the folder structure for results is generated, i.e. the "result" folder with subfolders ######
import os
cwd = os.getcwd()

def make_folder_if_not_exists(foldername):
    if not os.path.exists(foldername):
        os.makedirs(foldername)

folder_results='results\\'
make_folder_if_not_exists(folder_results)

subfolder_fits=folder_results+'H2S6P_fits\\'
make_folder_if_not_exists(subfolder_fits)

subfolder_plots=folder_results+'H2S6P_plots\\'
make_folder_if_not_exists(subfolder_plots)

subfolder_values=folder_results+'H2S6P_values\\'
make_folder_if_not_exists(subfolder_values)

folder_results_comb=folder_results+'comb\\'
make_folder_if_not_exists(folder_results_comb)

folder_results_comb_plot=folder_results_comb+'individual plots\\'
make_folder_if_not_exists(folder_results_comb_plot)

folder_results_maser=folder_results+'maser\\'
make_folder_if_not_exists(folder_results_maser)

folder_results_maser_plot=folder_results_maser+'individual plots\\'
make_folder_if_not_exists(folder_results_maser_plot)


folder_results_fits_sim=subfolder_fits+'fits_sim\\'
make_folder_if_not_exists(folder_results_fits_sim)

folder_results_fits_blind=subfolder_fits+'fits_blind\\'
make_folder_if_not_exists(folder_results_fits_blind)

folder_results_fits_comb_freq=subfolder_fits+'fits_comb_freq\\'
make_folder_if_not_exists(folder_results_fits_comb_freq)

folder_results_fits_comb_freq_maser_corr=subfolder_fits+'fits_comb_freq_maser_corr\\'
make_folder_if_not_exists(folder_results_fits_comb_freq_maser_corr)

folder_plots_blind=subfolder_plots+'plots_blind\\'
make_folder_if_not_exists(folder_plots_blind)

folder_plots_comb=subfolder_plots+'plots_comb_freq\\'
make_folder_if_not_exists(folder_plots_comb)

folder_plots_comb_maser_corr=subfolder_plots+'plots_comb_freq_maser_corr\\'
make_folder_if_not_exists(folder_plots_comb)


folder_results_values_blind=subfolder_values+'H2S-6P_blind\\'
make_folder_if_not_exists(folder_results_values_blind)

folder_results_values_comb_freq=subfolder_values+'H2S-6P_comb_freq\\'
make_folder_if_not_exists(folder_results_values_comb_freq)

folder_results_values_comb_freq_maser_corr=subfolder_values+'H2S-6P_comb_freq_maser_corr\\'
make_folder_if_not_exists(folder_results_values_comb_freq_maser_corr)

# If comb laser frequencies have already been fitted, i.e. if the 'CombResultsEachScan.pickle' file exists, then the repeated analysis of comb frequencies is skipped
if use_comb_freq:
    CombFile='CombResultsEachScan.pickle'
    analyze_comb_freq=True
    for file in os.listdir(folder_results_comb):
        if file==CombFile:
            analyze_comb_freq=False
else:
    analyze_comb_freq=False
# Filename for storing the maser drift results
MaserResultsFile='MaserResultsEachDay.pickle'