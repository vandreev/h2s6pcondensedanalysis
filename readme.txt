This code makes an analysis of the hydrogen 2S-6P condesed data set following the procedure described in README of the 'h2s6p2019condensed' dataset written by Lothar. In the config.py file one can set different options:

- use_comb_freq sets the option whether to analyze the blind frequencies or calculate the unblinded laser frequency given the AOM frequencies and the frequency comb counter data
- plot_comb_freq sets the option whether to plot the laser frequencies for each day or not (~1min extra time for plotting)
- correct_for_maser sets the option whether to correct the laser frequencies by the maser drift
- plot_individual_maser_days sets the option whether to plot the maser data for each day or not (~1min extra time for plotting)

With the Array datagroups = ['G1A', ... ] one can choose which datagroups to include in the analysis (all datagroups are included per default)

LFSgroups = ['G13','G14'] specifies which datagroups are used for light force shift measurement, such that the VoigtDoublet will be fitted for these datagroups.

FS6P32groups = ['G7A','G7B','G8','G9','G10','G11','G12'] specifies the datagroups for the 2S-6P32 fine-structure component (other datagroups are 2S-6P12).

Then there are settings for the results folder structure.

In order to run the analysis, one simply needs to run the file run_analysis_datagroups.py, which runs subsequently the relevant scripts contained in the scripts folder.
The results are then produced in the results folder:
- in results/comb all the frequency comb data for laser frequency determination is analyzed/plotted
- in results/maser the analysis of the maser data is saved
- in results/H2S6P_fits the fits for all line scans are saved
- in results/H2S6P_plots the plots for individual datagroups and the average are saved
- in results/H2S6P_values the averages of datagroups and the final results are saved

Depending on the switched use_comb_freq and correct_for_maser, all results/H2S6P... folders have the substructure of
- blind: blind analysis
- comb_freq: unblinded laser frequency analyis without maser correction
- comb_freq_maser_corr: unblinded laser frequency analyis with maser correction

All code in this analysis has been written redundantly by Vitaly with following exceptions:
- in scripts/fitfunctions.py the derivatives of the Voigt and VoigtDoublet functions Df_Voigt(x, *p) Df_VoigtDoublet(x, *p) have been taken from Lothars package.
- in scripts/statisticsfunctions.py some helpful statistics functions were copied from Lothars analysis package, but these have been checked separately
- scripts/sim_plts_thesis.py uses the nice plotting settings script from Lothar (only slightly modified)