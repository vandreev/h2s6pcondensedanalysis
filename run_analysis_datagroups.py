# -*- coding: utf-8 -*-
"""
Created on Tue Mar 12 12:12:56 2024

@author: Vitaly Wirthl
"""
from config import *

from scripts.ComputeLaserFreqEachScan import compute_laserfreq_eachscan
if analyze_comb_freq:
    print('No comb freq. results file available, analyzing comb data')
    compute_laserfreq_eachscan()

from scripts.maser_plot import maser_plot
if use_comb_freq and correct_for_maser:
    maser_plot()

from scripts.plot_FP3laserFreq_EachMeasDay import plot_FP3laserFreq_EachMeasDay
from scripts.plot_FP3laserFreq_whole_campaign import plot_FP3laserFreq_whole_campaign

if use_comb_freq and plot_comb_freq:
    plot_FP3laserFreq_EachMeasDay()
    plot_FP3laserFreq_whole_campaign('2019-01-01', '2019-09-01', 'FP3LaserFreq H2S-6P Campaign All Data')
    plot_FP3laserFreq_whole_campaign('2019-03-01', '2019-05-01', 'FP3LaserFreq H2S-6P Campaign March-April')
    plot_FP3laserFreq_whole_campaign('2019-05-01', '2019-07-01', 'FP3LaserFreq H2S-6P Campaign May-June')
    plot_FP3laserFreq_whole_campaign('2019-07-01', '2019-09-01', 'FP3LaserFreq H2S-6P Campaign July-August')


from scripts.script01_fit_scans_and_sim import fit_scans_and_sim
from scripts.script02_analyze_delay_average import analyze_delay_average
from scripts.script03_analyze_scans_individually import analyze_scans_individually
from scripts.script04_plot_analysis_scans_individually import plot_analysis_scans_individually
from scripts.script05_analyze_scans_separate_FC_with_corr import analyze_scans_separate_FC_with_corr
from scripts.script06_plot_analysis_scans_FCaver import plot_analysis_scans_FCaver


for current_group in datagroups:
    fit_scans_and_sim(current_group)
    analyze_delay_average(current_group)
    analyze_scans_individually(current_group)
    plot_analysis_scans_individually(current_group)
    analyze_scans_separate_FC_with_corr(current_group)
    plot_analysis_scans_FCaver(current_group)
    

from scripts.script07_plot_analysis_allFCaver import analysis_all_FCaver

filename_2S612_result='6P12_DatagroupsG1-G6+G13-G14'
filename_2S632_result='6P32_DatagroupsG7-G12'

datagroups = ['G1A','G1B','G1C','G2','G3','G4','G5','G6','G13','G14']
plotTitle='2S-6P12, data groups G1-G6+G13-14, freezing cycle averages'
analysis_all_FCaver(datagroups,filename_2S612_result,plotTitle)

datagroups = ['G7A','G7B','G8','G9','G10','G11','G12']
plotTitle='2S-6P32, data groups G7-G12, freezing cycle averages'
analysis_all_FCaver(datagroups,filename_2S632_result,plotTitle)

#Calculate proton radius and fine structure centroid from HFS centroid results
if use_comb_freq:
    from scripts.script08_calculate_prad_and_FS_centroid import calculate_prad_and_FS_centroid
    calculate_prad_and_FS_centroid(filename_2S612_result,filename_2S632_result)
