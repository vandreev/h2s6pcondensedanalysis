# -*- coding: utf-8 -*-
"""
Created on Thu Mar 20 10:36:18 2023

@author: Vitaly Wirthl
"""
import numpy as np
import scipy.optimize
import scipy.special
import pandas as pd
import matplotlib.pyplot as plt
import os
import matplotlib.gridspec as gridspec

import scripts.sim_plts_thesis as plts
plts.set_params()

delete_points=[]  

from scripts.fitfunctions import *
from scripts.statisticsfunctions import *
from config import *

def analyze_delay_average(current_group):

    print('*********** Analyzing delay averages in Group {:s} ***********'.format(current_group))
    
    folder_scans=folder_all_groups+current_group+'\\Exp'
    
    scanfiles=os.listdir(folder_scans)
       
    FitsFileName=current_group+'_VoigtFits'
    if use_comb_freq:
        if correct_for_maser:
             dfFits=pd.read_pickle(folder_results_fits_comb_freq_maser_corr+FitsFileName+'.pickle').convert_dtypes()
        else:
            dfFits=pd.read_pickle(folder_results_fits_comb_freq+FitsFileName+'.pickle').convert_dtypes()    
    else:
        dfFits=pd.read_pickle(folder_results_fits_blind+FitsFileName+'.pickle').convert_dtypes()
     
    folder_velocities=folder_all_groups+current_group+'\\DelayVels'
    velocities_df=pd.read_csv(folder_velocities+'\\'+current_group+'_delay_vels.csv', delimiter=",")
    velocities=np.array(velocities_df['V_Mean'].values)
    
    plot_params = {
        'FontsizeFitResults': plts.params['FontsizeReg'],
        'FontsizeLegend': plts.params['FontsizeReg'],
        }
    plot_params['DelayData'] = {
        'linestyle': '',
        'marker': 'o',
        'markersize': 2}
    
    plot_params['DelayFit'] = {
        'linestyle': '-',
        'marker': '',
        'linewidth': 1}
    
    figure_height=2.8#3.35
    myfig = plt.figure(
        figsize=(plts.pagewidth, figure_height), constrained_layout=True)
    myfig.clf()
    widths = [1]
    heights = [1]
    
    # Create main layout splitting figure:
    gs_main = gridspec.GridSpec(
        ncols=1, nrows=1, figure=myfig,
        height_ratios=heights, width_ratios=widths
        )
    
    ax1 = myfig.add_subplot(gs_main[0])
       
    delay_avg_list_Ch0=[]
    delay_uncert_list_Ch0=[]
    
    delay_avg_list_Ch1=[]
    delay_uncert_list_Ch1=[]
    
    nu0_BM_Ch0=[]
    nu0_BM_Ch1=[]
    
    nu0_LFS_Ch0=[]
    nu0_LFS_Ch1=[]
    
    nu0_BM=-1
    nu0_LFS=-1
    
    for CEM_ID in ['Ch0','Ch1']:
        print('CEM_ID={:s}'.format(CEM_ID))
        for DelayID in ['1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16']: 
            mask = (dfFits['CEM_ID']==CEM_ID) & (dfFits['Delay']==DelayID)  
                       
            avg=avg_uncorr_meas(dfFits[mask]['nu0_corr'].values,dfFits[mask]['nu0_uncert'])
            
            avg_delay=avg['Mean']
            uncert_delay=avg['Sigma']
                       
            if CEM_ID=='Ch0':
                nu0_BM_Ch0.append(nu0_BM)
                nu0_LFS_Ch0.append(nu0_LFS)
                delay_avg_list_Ch0.append(avg_delay)
                delay_uncert_list_Ch0.append(uncert_delay)
                
            if CEM_ID=='Ch1':
                nu0_BM_Ch1.append(nu0_BM)
                nu0_LFS_Ch1.append(nu0_LFS)
                delay_avg_list_Ch1.append(avg_delay)
                delay_uncert_list_Ch1.append(uncert_delay)    


    xvals=np.linspace(0,270,100)
    

    ax1.errorbar(velocities,delay_avg_list_Ch0,yerr=delay_uncert_list_Ch0,linestyle='',marker='o',linewidth=1, capsize=1.5, markersize=2,label='Top detector')
    
    ax1.errorbar(velocities,delay_avg_list_Ch1,yerr=delay_uncert_list_Ch1,linestyle='',marker='s',linewidth=1,capsize=1.5,markersize=2,label='Bottom detector')
    ax1.set_xlim(0,265)
    
    avg=avg_uncorr_meas(delay_avg_list_Ch0, delay_uncert_list_Ch0)
    yvals_min=np.full(len(xvals), avg['Mean']-avg['Sigma'])
    yvals_max=np.full(len(xvals), avg['Mean']+avg['Sigma'])
    ax1.fill_between(xvals,yvals_min,yvals_max,color='tab:blue',hatch='\\\\\\\\\\\\',edgecolor='tab:blue',alpha=0.2,label=r'Avg.: $\nu_\mathrm{0,a}$='
                     +compact_uncert_str(1e-3*avg['Mean'],1e-3*avg['Sigma'],3)+' kHz'
                     +', $\chi_\mathrm{red}^2$='+compact_uncert_str(avg['RedChiSq'],avg['RedChiSqSigma'],2))
        
    avg=avg_uncorr_meas(delay_avg_list_Ch1, delay_uncert_list_Ch1)
    yvals_min=np.full(len(xvals), avg['Mean']-avg['Sigma'])
    yvals_max=np.full(len(xvals), avg['Mean']+avg['Sigma'])
    ax1.fill_between(xvals,yvals_min,yvals_max,color='tab:orange',hatch='\\\\\\\\\\\\',edgecolor='tab:orange',alpha=0.2,label=r'Avg.: $\nu_\mathrm{0,a}$='
                     +compact_uncert_str(1e-3*avg['Mean'],1e-3*avg['Sigma'],3)+' kHz'
                     +', $\chi_\mathrm{red}^2$='+compact_uncert_str(avg['RedChiSq'],avg['RedChiSqSigma'],2))

    
    
    xdata=np.array(velocities)
    ydata=np.array(delay_avg_list_Ch0)
    ydata_err=np.array(delay_uncert_list_Ch0)
    
    popt, pcov = scipy.optimize.curve_fit(lin_func,xdata,ydata,(0,0),sigma=ydata_err, absolute_sigma=True,ftol=1e-15, xtol=1e-15, gtol=1e-15,epsfcn=1e-15,maxfev=1000)
    nu0,nu0_err=popt[1],np.sqrt(pcov[1][1])
    k, k_err=popt[0],np.sqrt(pcov[0][0])
    redchi2=np.sum((lin_func(xdata,*popt)-ydata)**2/ydata_err**2)/(len(ydata)-len(popt))
    redchi2_err=np.sqrt(2/(len(ydata)-len(popt)))
    
    
    extrapol_plot_Ch0_1=ax1.plot(xvals,lin_func(xvals,*popt), '-',color='tab:blue',label='Extrapol.: '
             +r'$\nu_\mathrm{0,e}$='+compact_uncert_str(1e-3*nu0,1e-3*nu0_err,4)+' kHz,'+'\n'
             +r'$\kappa$='+compact_uncert_str(k,k_err,4)+' Hz/(m/s)'
             +', $\chi_\mathrm{red}^2$='+str(np.round(redchi2,2))+'('+str(int(np.round(1e2*redchi2_err,0))) +')'
             )
    # ax1.fill_between(xvals, y_min, y_max,color='tab:blue',ec=None,alpha=0.2)
    var_y=pcov[1][1]+xvals**2*pcov[0][0]-2*xvals*np.abs(pcov[1][0])
    ax1.fill_between(xvals,lin_func(xvals,*popt)-np.sqrt(var_y), lin_func(xvals,*popt)+np.sqrt(var_y),color='tab:blue',ec=None,alpha=0.2)
    
    extrapol_plot_Ch0_2= ax1.fill(np.NaN, np.NaN,color='tab:blue',ec=None,alpha=0.2)
    
    xdata=np.array(velocities)
    ydata=np.array(delay_avg_list_Ch1)
    ydata_err=np.array(delay_uncert_list_Ch1)
    popt, pcov = scipy.optimize.curve_fit(lin_func,xdata,ydata,(0,0),sigma=ydata_err, absolute_sigma=True,ftol=1e-15, xtol=1e-15, gtol=1e-15,epsfcn=1e-15,maxfev=1000)
    nu0,nu0_err=popt[1],np.sqrt(pcov[1][1])
    k, k_err=popt[0],np.sqrt(pcov[0][0])
    # y_min=xvals*(k+k_err)+nu0-nu0_err
    # y_max=xvals*(k-k_err)+nu0+nu0_err
    redchi2=np.sum((lin_func(xdata,*popt)-ydata)**2/ydata_err**2)/(len(ydata)-len(popt))
    redchi2_err=np.sqrt(2/(len(ydata)-len(popt)))
    
    extrapol_plot_Ch1_1=ax1.plot(xvals,lin_func(xvals,*popt), '-',color='tab:orange',label='Extrapol.: '
             +r'$\nu_\mathrm{0,e}$='+compact_uncert_str(1e-3*nu0,1e-3*nu0_err,4)+' kHz,'+'\n'
             +r'$\kappa$='+compact_uncert_str(k,k_err,4)+' Hz/(m/s)'
             +', $\chi_\mathrm{red}^2$='+str(np.round(redchi2,2))+'('+str(int(np.round(1e2*redchi2_err,0))) +')'
             )
    # ax1.fill_between(xvals, y_min, y_max, color='tab:orange',ec=None,alpha=0.2)
    var_y=pcov[1][1]+xvals**2*pcov[0][0]-2*xvals*np.abs(pcov[1][0])
    ax1.fill_between(xvals,lin_func(xvals,*popt)-np.sqrt(var_y), lin_func(xvals,*popt)+np.sqrt(var_y),color='tab:orange',ec=None,alpha=0.2)
    
    extrapol_plot_Ch1_2= ax1.fill(np.NaN, np.NaN,color='tab:orange',ec=None,alpha=0.2)
    
    import matplotlib.ticker as ticker
    #Convert y axis :
    ticks = ticker.FuncFormatter(lambda y, pos: '${:.1f}$'.format( y*1e-3))
    ax1.yaxis.set_major_formatter(ticks)
      
    handles, labels = plt.gca().get_legend_handles_labels()
    order = [4,2,0,5,3,1]
     
    handles[0]=(extrapol_plot_Ch0_1[0],extrapol_plot_Ch0_2[0])
    handles[1]=(extrapol_plot_Ch1_1[0],extrapol_plot_Ch1_2[0])
    
    legend=ax1.legend([handles[idx] for idx in order],[labels[idx] for idx in order],#bbox_to_anchor=(1.1, 1.05),
            **plts.get_leg_params(**{
                  'loc': 'best',
                  'ncol': 2,
                  'frameon': True,
                'fontsize': plot_params['FontsizeLegend']}))
    frame = legend.get_frame()
    frame.set_edgecolor('gray')
    frame.set_linewidth(0.5)
    
    ax1.set_ylabel(r'$\nu_0$ (kHz)')
    ax1.set_xlabel(r'Mean speed $\overline{v}$ (m/s)')
    
    suptitle='Data group: '+current_group
    if use_comb_freq:
        if current_group in FS6P32groups:
            suptitle=suptitle+'\n'+r'Frequency values for $\nu_\mathrm{0,a}$ and $\nu_\mathrm{0,e}$ are shown with an offset of '+u"\u2212"+str(nu2s6p_offset_32)+' Hz'
        else:
            suptitle=suptitle+'\n'+r'Frequency values for $\nu_\mathrm{0,a}$ and $\nu_\mathrm{0,e}$ are shown with an offset of '+u"\u2212"+str(nu2s6p_offset_12)+' Hz'
    myfig.suptitle(suptitle)
    
    plotFile='DalayAvg_'+FitsFileName
          
    if use_comb_freq:
        if correct_for_maser:
            plotFolder = folder_plots_comb_maser_corr+current_group
            if not os.path.exists(plotFolder):
                os.makedirs(plotFolder)
        else:
            plotFolder = folder_plots_comb+current_group
            if not os.path.exists(plotFolder):
                os.makedirs(plotFolder)
    else:
        plotFolder = folder_plots_blind+current_group
        if not os.path.exists(plotFolder):
            os.makedirs(plotFolder)
       
    #import datetime
    filename = (
        #datetime.datetime.utcnow().strftime('%Y-%m-%d') + ' '+
        plotFile)
    filepath = os.path.join(
        plotFolder, filename)
    myfig.savefig(filepath + '.pdf', dpi=400)
    #myfig.savefig(filepath + '.png', dpi=400)

    plt.close('all')