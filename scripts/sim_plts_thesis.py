"""
July 2024: modified version of Lothar's plotting settings script by Vitaly 
Using standard Arial font instead of MyriadPro such that this font does not need to be installed
Using Computer Modern (TeX) font since otherwise v and nu look exactly the same

"""
# -*- coding: utf-8 -*-
"""
Created on Sun May  5 12:05:18 2019

@author: Lothar Maisenbacher/MPQ

Set plot style as used for thesis.

Check that all required fonts are installed on your system.
To check which fonts have been found by Matplotlib, it is easiest to check the
JSON cache file fontlist-vXXX.json, which is located in the cache folder found
by the command mpl.get_cachedir().
To refresh this file, use:
matplotlib.font_manager._rebuild()
If Matplotlib does not assign the correct parameters to the font files
(there is some guessing involved), one can either manually edit this file
(but it will overwritten the next time the cache is built, which does not seem
to happen often) or point Matplotlib explicitly to the font file instead of
setting parameters:
font_manual = font_manager.FontProperties(fname=fpath)
where fpath is the path to the font file, e.g.
'C:\\Windows\\Fonts\\MyriadPro-Cond.otf'.
It might also be necessary to monkey-patch as Matplotlib, as described in:
https://stackoverflow.com/a/33962423
Make sure that the fonts needed are actually installed in 'C:\\Windows\\Fonts',
which is only the case if the font is installed for all users in Windows. By
default, it is only installed for the active user and store elsewhere, where it
is not found by Matplotlib. To install for all users, right click on the font file
and select 'Install for all users' (on Windows 10).

List of fonts that need to be installed:
MYRIADPRO-BOLD.OTF
MYRIADPRO-BOLDCOND.OTF
MYRIADPRO-BOLDCONDIT.OTF
MYRIADPRO-BOLDIT.OTF
MYRIADPRO-COND.OTF
MYRIADPRO-CONDIT.OTF
MyriadPro-It.otf
MyriadPro-Light.otf
MYRIADPRO-REGULAR.OTF
MYRIADPRO-SEMIBOLD.OTF
MYRIADPRO-SEMIBOLDIT.OTF
Apple Chancery Regular.ttf
"""

#import os

import numpy as np
import matplotlib.pyplot as plt
from matplotlib import font_manager
from matplotlib import cm

# Page width (linewidth in Latex) of document (in mm)
# For \documentclass[a4paper,12pt]{book}
#pagewidth_mm = 161.9
# For \documentclass[a4paper,11pt]{book}
pagewidth_mm = 155.5
# In inch
pagewidth = pagewidth_mm/25.4

# Default Matplotlib parameters
params = {
    'FontsizeReg': 7,
    'FontsizeMed': 7,
    'FontsizeSmall': 7,
    }

params_rc_default = {
    # Use mathtext of Matplotlib, not LaTeX for math rendering,
    # mathtext is much faster
    'text.usetex' : False,
    'font.size' : params['FontsizeReg'],
    'font.family' : 'sans-serif',
    'font.sans-serif' : 'Arial',
    'font.stretch' : 'normal',
    #'mathtext.fontset' : 'custom',
    'mathtext.fontset' : 'cm',
    # 'mathtext.rm': 'Arial',
    # 'mathtext.it': 'Arial:italic',
    # 'mathtext.bf': 'Arial:bold',
    # 'mathtext.sf': 'Arial',
    # 'mathtext.cal': 'Arial:italic',
    # 'mathtext.fallback': 'cm',
#    # Default figure size (in)
    'figure.figsize': (pagewidth, 3.5),
#    # DPI of display figures (adapt to screen as needed)
    'figure.dpi': 200,
#    # DPI of saved figures
    'savefig.dpi': 600,
#    # Use unicode minus symbol
    'axes.unicode_minus':  True,
#    # Width of axes
    'axes.linewidth': 0.7,
#    # Width of axes x and y ticks
    'xtick.major.width': 0.7,
    'xtick.minor.width': 0.7,
    'ytick.major.width': 0.7,
    'ytick.minor.width': 0.7,
#    # Setting pdf.fonttype to 3 will outline fonts in the PDF,
#    # which is necessary for math text
    'pdf.fonttype': 3,
    }

# Function definitions
def set_params(params_rc=None):
    ''' Set Matplotlib parameters '''

    if params_rc is None:
        params_rc = {}
    plt.rcParams.update({
        **params_rc_default, **params_rc})

def set_fontsize(fontsize_id):
    ''' Set Matplotlib fontsize '''

    params_rc = {'font.size' : params[fontsize_id]}
    plt.rcParams.update(params_rc)

def get_marker(i, markers=None):
    '''
    Get marker with index i from list markers,
    repeating after i has reached length of list
    '''

    if markers is None:
        markers = ['o', 'd', 's', 'p', 'v', '^']
    return markers[np.mod(i, len(markers))]

def get_linestyle(i, linestyles=None):
    '''
    Get marker with index i from list markers,
    repeating after i has reached length of list
    '''

    if linestyles is None:
        linestyles = ['-', '--', ':', '-.']
    return linestyles[np.mod(i, len(linestyles))]

def get_line_colors(num_colors, unique_colors=10, cmap=None):
    '''
    Get array of integer num_colors line colors,
    repeating after integer unique_colors
    '''

    if cmap is None:
        if unique_colors <= 10:
            cmap = plt.cm.tab10
        elif unique_colors <= 20:
            cmap = plt.cm.tab20
        else:
            cmap = plt.cm.tab20
            unique_colors = 20
    else:
        cmap = cmap
    colors = cmap(np.linspace(0, 1, unique_colors))
    colors = (np.tile(
        colors,
        [int(np.ceil(num_colors/unique_colors)), 1])
        [:num_colors])
    return colors

def get_cont_colors(num, start, stop, cmap=None):
    '''
    Get list of colors of length num from colormap cmap,
    from colormap index start to stop
    '''

    if cmap is None:
        cmap = 'GnBu'
    x = np.linspace(start, stop, num)
    return cm.get_cmap(cmap)(x)[:, :3]

def autofmt_xdate(ax, rotation=30, ha='right'):
    '''
    Rotate and align date labels on x-axis,
    imitating plt.autofmt_xdate but with support for constrained layout
    '''

    for label in ax.get_xticklabels():
        label.set_ha(ha)
        label.set_rotation(rotation)

# Set default Matplotlib parameters
set_params()

# Get default font
font_default = font_manager.FontProperties()

# Get condensend font
font_condensed = font_default.copy()
font_condensed.set_stretch('condensed')
# Set font manually by directly pointing to file
#fpath = 'C:\\Windows\\Fonts\\MyriadPro-Cond.otf'
#if os.path.exists(fpath):
#    font_condensed = font_manager.FontProperties(fname=fpath)

# Title parameters
title_params = {
    'fontsize': params['FontsizeReg']
    }

def get_title_params(**kwargs):
    params_ = {**title_params, **kwargs}
    return params_

# Legend parameters
leg_params = {
    'fontsize': params['FontsizeSmall'],
    # 'handlelength': 0.7,
    'handlelength': 1.2,
    'handletextpad': 0.5,
    'borderpad': 0.2,
    'columnspacing': 0.7,
    'edgecolor': '1',
    'facecolor': '1',
    'framealpha': 0.7,
    'frameon': True,
    'fancybox': True,
    'loc': 'upper right',
    }

def get_leg_params(**kwargs):
    params_ = {**leg_params, **kwargs}
    font_leg_ = font_condensed.copy()
    font_leg_.set_size(params_['fontsize'])
    params_ = {**params_, 'prop': font_leg_}
    return params_

point_params = {
    'marker':'o',
    'linestyle': '',
    'linewidth': 0.7,
    'markersize': 2,
}

def get_point_params(**kwargs):
    params_ = {**point_params, **kwargs}
    return params_

errorbar_params = {
    'marker':'o',
    'linestyle': '',
    'capsize': 1,
    'markersize': 1,
    'linewidth': 0.7,
#    'mew': 1,
#    'mec': 'k',
}

def get_errorbar_params(**kwargs):
    params_ = {**errorbar_params, **kwargs}
    return params_

line_params = {
    'marker':'',
    'linestyle': '-',
    'linewidth': 0.7,
}

def get_line_params(**kwargs):
    params_ = {**line_params, **kwargs}
    return params_

errorbar_capthick = 2
# Get default line colors
colors = get_line_colors(20)
chs_alpha_lims = [0.5, 1]

# Default output folder for plots
out_folder = 'plots//'
