# -*- coding: utf-8 -*-
"""
Created on Thu Mar 20 10:36:18 2023

@author: Vit
"""
import numpy as np
import scipy.optimize
import scipy.special
import pandas as pd
import matplotlib.pyplot as plt
from scripts.statisticsfunctions import *
import os
import matplotlib.gridspec as gridspec
from scipy.stats import norm

from scripts.fitfunctions import *
from config import *

import scripts.sim_plts_thesis as plts
plts.set_params()



def analysis_all_FCaver(datagroups,plotFile,plotTitle):

    if use_comb_freq:
        if correct_for_maser:
            currentframe=pd.read_pickle(folder_results_fits_comb_freq_maser_corr+datagroups[0]+'_VoigtFits'+'_FCaver.pickle').convert_dtypes()
            frames=[currentframe]
            for data_group in datagroups[1::]:
                currentframe=pd.read_pickle(folder_results_fits_comb_freq_maser_corr+data_group+'_VoigtFits'+'_FCaver.pickle').convert_dtypes()
                frames.append(currentframe)
        else:
            currentframe=pd.read_pickle(folder_results_fits_comb_freq+datagroups[0]+'_VoigtFits'+'_FCaver.pickle').convert_dtypes()
            frames=[currentframe]
            for data_group in datagroups[1::]:
                currentframe=pd.read_pickle(folder_results_fits_comb_freq+data_group+'_VoigtFits'+'_FCaver.pickle').convert_dtypes()
                frames.append(currentframe)
    else:
        currentframe=pd.read_pickle(folder_results_fits_blind+datagroups[0]+'_VoigtFits'+'_FCaver.pickle').convert_dtypes()
        frames=[currentframe]
        for data_group in datagroups[1::]:
            currentframe=pd.read_pickle(folder_results_fits_blind+data_group+'_VoigtFits'+'_FCaver.pickle').convert_dtypes()
            frames.append(currentframe)


    dfFits_FC_aver = pd.concat(frames)



    plot_params = {
        # 'Color1': colors[0],
        # 'Color2': colors[6],
        'FontsizeFitResults': plts.params['FontsizeReg'],
        'FontsizeLegend': 7,
        }
    plot_params['Data'] = {
        'linestyle': '',
        'marker': 'o',
        'elinewidth': 0.2,
        'linewidth': 0.5,
        'markeredgewidth': 0.6,
        'capsize': 0.8,
        'markersize': 0.5}


    figure_height=4
    myfig = plt.figure(
        figsize=(0.8*plts.pagewidth, figure_height), constrained_layout=True)
    myfig.clf()
    widths = [1]
    heights = [1,1,1]

    # Create main layout splitting figure:
    gs_main = gridspec.GridSpec(
        ncols=1, nrows=3, figure=myfig,
        height_ratios=heights, width_ratios=widths
        )

    # One could split the figure layout further with .GridSpecFromSubplotSpec:
    # gs_sub = gridspec.GridSpecFromSubplotSpec(
    #     ncols=2, nrows=1, subplot_spec=gs_main[0],
    #     width_ratios=[2, 1],
    #     )

    # colors = plts.get_line_colors(20, unique_colors=20)

    colors =['tab:blue','tab:orange','tab:red','tab:gray']

    #Create axes
    # ax_nu_a = myfig.add_subplot(gs_main[0])
    # ax_nu_e = myfig.add_subplot(gs_main[3],sharex=ax_nu_a)
    # ax_kappa = myfig.add_subplot(gs_main[6],sharex=ax_nu_a)
    #Create axes
    # ax_nu_a_hist = myfig.add_subplot(gs_main[1])
    ax_nu_a_FCaver = myfig.add_subplot(gs_main[0])

    # ax_nu_e_hist = myfig.add_subplot(gs_main[4])
    ax_nu_e_FCaver = myfig.add_subplot(gs_main[1],sharex=ax_nu_a_FCaver)

    # ax_kappa_hist = myfig.add_subplot(gs_main[7])
    ax_kappa_FCaver = myfig.add_subplot(gs_main[2],sharex=ax_nu_a_FCaver)
    # ax_nu_e = myfig.add_subplot(gs_main[3],sharex=ax_nu_a)
    # ax_kappa = myfig.add_subplot(gs_main[6],sharex=ax_nu_a)

    # Dont show tick labels on shared axes 
    plt.setp(ax_nu_a_FCaver.get_xticklabels(), visible=False)
    plt.setp(ax_nu_e_FCaver.get_xticklabels(), visible=False)

    plt.setp(ax_nu_a_FCaver.get_xticklabels(), visible=False)
    plt.setp(ax_nu_e_FCaver.get_xticklabels(), visible=False)


    nu_a_list=dfFits_FC_aver['nu_a'].values
    nu_a_err_list=dfFits_FC_aver['nu_a_err'].values
    nu_a_err_scaled_list=dfFits_FC_aver['nu_a_err_scaled'].values


    nu_e_list=dfFits_FC_aver['nu_e'].values
    nu_e_err_list=dfFits_FC_aver['nu_e_err'].values
    nu_e_err_scaled_list=dfFits_FC_aver['nu_e_err_scaled'].values

    kappa_list=dfFits_FC_aver['kappa'].values
    kappa_err_list=dfFits_FC_aver['kappa_err'].values
    kappa_err_scaled_list=dfFits_FC_aver['kappa_err_scaled'].values


    no_FCs=len(nu_a_list)
    i_scans=np.arange(1,len(nu_a_list)+1,1)
    xvals=np.arange(0.2,len(nu_a_list)+1,0.5)

    ax_nu_a_FCaver.errorbar(i_scans+0.2,nu_a_list,yerr=nu_a_err_scaled_list,color='tab:blue',**plot_params['Data'],label=r'FC avg. scaled')
    # ax_nu_a_FCaver.errorbar(i_scans,nu_a_list,yerr=nu_a_err_list,color='tab:gray',**plot_params['Data'],label=r'FC avg.')

    avg=avg_uncorr_meas(nu_a_list,nu_a_err_scaled_list)
    ax_nu_a_FCaver.plot(xvals,np.full(len(xvals), avg['Mean']),linestyle='-',linewidth=0.7,color='tab:blue',alpha=0.5,label=r'Avg.: '
                    +compact_uncert_str(1e-3*avg['Mean'],1e-3*avg['Sigma'],3)+' kHz'
                    +', $\chi_\mathrm{red}^2$='+compact_uncert_str(avg['RedChiSq'],avg['RedChiSqSigma'],2)+ ', N={:.0f}'.format(len(nu_a_list)))
                    


    ax_nu_e_FCaver.errorbar(i_scans+0.2,nu_e_list,yerr=nu_e_err_scaled_list,color='tab:blue',**plot_params['Data'],label=r'FC avg. scaled')
    # ax_nu_e_FCaver.errorbar(i_scans,nu_e_list,yerr=nu_e_err_list,color='tab:gray',**plot_params['Data'],label=r'FC avg.')

    avg=avg_uncorr_meas(nu_e_list,nu_e_err_scaled_list)
    avg_nu_e=avg
    ax_nu_e_FCaver.plot(xvals,np.full(len(xvals), avg['Mean']),linestyle='-',linewidth=0.7,color='tab:blue',alpha=0.5,label=r'Avg.: '
                    +compact_uncert_str(1e-3*avg['Mean'],1e-3*avg['Sigma'],3)+' kHz'
                    +', $\chi_\mathrm{red}^2$='+compact_uncert_str(avg['RedChiSq'],avg['RedChiSqSigma'],2))
        
    #%%
    ax_kappa_FCaver.errorbar(i_scans+0.2,kappa_list,yerr=kappa_err_scaled_list,color='tab:blue',**plot_params['Data'],label=r'FC avg. scaled')
    # ax_kappa_FCaver.errorbar(i_scans,kappa_list,yerr=kappa_err_list,color='tab:gray',**plot_params['Data'],label=r'FC avg.')

    avg=avg_uncorr_meas(kappa_list,kappa_err_scaled_list)
    ax_kappa_FCaver.plot(xvals,np.full(len(xvals), avg['Mean']),linestyle='-',linewidth=0.7,color='tab:blue',alpha=0.5,label=r'Avg.: '
                    +compact_uncert_str(avg['Mean'],avg['Sigma'],3)+' Hz/(m/s)'
                    +', $\chi_\mathrm{red}^2$='+compact_uncert_str(avg['RedChiSq'],avg['RedChiSqSigma'],2))


    #%%
    import matplotlib.ticker as ticker
    #Convert y axis :
    # ticks = ticker.FuncFormatter(lambda y, pos: '{0:g}'.format( y*1e-3))
    ticks = ticker.FuncFormatter(lambda y, pos: '${:.0f}$'.format( y*1e-3))
    # ticks = ticker.FuncFormatter(lambda y, pos: '{%.1f}'%y)
    # ax_nu_a.yaxis.set_major_formatter(ticks)
    ax_nu_a_FCaver.yaxis.set_major_formatter(ticks)

    #Convert y axis :
    # ticks = ticker.FuncFormatter(lambda y, pos: '{0:g}'.format( y*1e-3))
    ticks = ticker.FuncFormatter(lambda y, pos: '${:.0f}$'.format( y*1e-3))
    # ticks = ticker.FuncFormatter(lambda y, pos: '{%.1f}'%y)
    # ax_nu_e.yaxis.set_major_formatter(ticks)
    ax_nu_e_FCaver.yaxis.set_major_formatter(ticks)


    ax_nu_a_FCaver.set_ylabel(r'$\nu_\mathrm{0,a}$ (kHz)')
    ax_nu_e_FCaver.set_ylabel(r'$\nu_\mathrm{0,e}$ (kHz)')
    ax_kappa_FCaver.set_ylabel(r'$\kappa$ (Hz/(m/s))')
    # ax_kappa.set_xlabel(r'Resonance line scan number')

    ax_kappa_FCaver.set_xlabel(r'Freezing cycle (FC) number')
    # ax1.set_xlabel(r'Mean speed $\overline{v}$ (m/s)')


    legend=ax_nu_a_FCaver.legend( **plts.get_leg_params(**{
                'loc': 'upper left',
                'ncol': 3,
                'frameon': True,
                'fontsize': plot_params['FontsizeLegend']}))
    frame = legend.get_frame()
    frame.set_edgecolor('gray')
    frame.set_linewidth(0.5)


    legend=ax_nu_e_FCaver.legend( **plts.get_leg_params(**{
                'loc': 'upper left',
                'ncol': 3,
                'frameon': True,
                'fontsize': plot_params['FontsizeLegend']}))
    frame = legend.get_frame()
    frame.set_edgecolor('gray')
    frame.set_linewidth(0.5)

    legend=ax_kappa_FCaver.legend( **plts.get_leg_params(**{
                'loc': 'upper left',
                'ncol': 3,
                'frameon': True,
                'fontsize': plot_params['FontsizeLegend']}))
    frame = legend.get_frame()
    frame.set_edgecolor('gray')
    frame.set_linewidth(0.5)


    suptitle=plotTitle
    if use_comb_freq:
        if datagroups[0] in FS6P32groups:
            suptitle=suptitle+'\n'+r'Frequency values for $\nu_\mathrm{0,a}$ and $\nu_\mathrm{0,e}$ are shown with an offset of '+u"\u2212"+str(nu2s6p_offset_32)+' Hz'
        else:
            suptitle=suptitle+'\n'+r'Frequency values for $\nu_\mathrm{0,a}$ and $\nu_\mathrm{0,e}$ are shown with an offset of '+u"\u2212"+str(nu2s6p_offset_12)+' Hz'
    myfig.suptitle(suptitle)

        
    if use_comb_freq:
        if correct_for_maser:
            plotFolder = folder_plots_comb_maser_corr
            valuesFolder = folder_results_values_comb_freq_maser_corr
        else:   
            plotFolder = folder_plots_comb
            valuesFolder = folder_results_values_comb_freq
    else:
        plotFolder = folder_plots_blind
        valuesFolder = folder_results_values_blind

    recoil_shift=-1176027
    Delta_HFS_2S=177556834.3 #Directly measured by Kolachevsky et al PRL 102 (2009) with uncertainty of 6.7 Hz, theoretical derivation from 1S HFS is 20 times more accurate, see Jentschura et al PRA 73 (2006): 177556838.2(3) Hz
    if use_comb_freq:
        if datagroups[0] in FS6P32groups:
            transition_text='2S(J=1/2,F=0)-6P(J=3/2,F=1)'
            nu2s6p_offset=nu2s6p_offset_32
            dcStark_shift=-20 #dc-Stark shift: -0.02(6)kHz
            BBR_shift=250 #BBR-induced shift: 0.25(4)kHz
            other_uncert=1e3*np.sqrt(0.22**2 #simulations
                                 +0.08**2 #sampling bias
                                 +0.04**2 #background
                                 +0.06**2 #dc-Stark
                                 +0.04**2 #BBR
                                 +0.11**2 #Zeeman
                                 +0.01**2 #Pressure
                                 +0.07**2 #Maser
                                 )
            Delta_HFS_6P32=875992 # uncertainty of 9 Hz, see Kramida et al Atomic Data and Nuclear Data Tables 96 (2010)
            Delta_HFS_OffDiag32=92 # Eq (2.20) thesis Vitaly
            HFS_corr=-3/4*Delta_HFS_2S+5/8*Delta_HFS_6P32-3/8*Delta_HFS_OffDiag32
        else:
            transition_text='2S(J=1/2,F=0)-6P(J=1/2,F=1)'
            nu2s6p_offset=nu2s6p_offset_12
            dcStark_shift=200 #dc-Stark shift: 0.20(21)kHz
            BBR_shift=250 #BBR-induced shift: 0.25(4)kHz
            other_uncert=1e3*np.sqrt(0.43**2 #simulations
                                 +0.04**2 #sampling bias
                                 +0.03**2 #background
                                 +0.21**2 #dc-Stark
                                 +0.04**2 #BBR
                                 +0.02**2 #Zeeman
                                 +0.01**2 #Pressure
                                 +0.07**2 #Maser
                                 )
            Delta_HFS_6P12=2191470 # uncertainty of 22 Hz, see Kramida et al Atomic Data and Nuclear Data Tables 96 (2010)
            Delta_HFS_OffDiag12=-92 # Eq (2.20) thesis Vitaly
            HFS_corr=-3/4*Delta_HFS_2S-1/4*Delta_HFS_6P12-3/4*Delta_HFS_OffDiag12
    else:
        nu2s6p_offset=0

    with open(valuesFolder+plotFile+' nu_e FCaver value.txt', "w") as text_file:
        if use_comb_freq:
            print('Velocity-extrapolated '+ transition_text +' frequency, corrected for Light Force Shift, Second-order Doppler Shift and Quantum Interference (Big Model):', file=text_file)
            print('nu_e = '+compact_uncert_str(avg_nu_e['Mean']+nu2s6p_offset,avg_nu_e['Sigma'],1).replace(u"\u2212","-")  + ' Hz', file=text_file)
            print('Correcting for recoil shift of {:.0f} Hz and correcting for dc-Stark shift of {:.0f} Hz and BBR-induced shift of {:.0f} Hz, adding systematic uncertainty of {:.0f} Hz in quadrature: \n'.format(recoil_shift,dcStark_shift,BBR_shift,other_uncert)+
                    'nu_e = '+compact_uncert_str(avg_nu_e['Mean']+nu2s6p_offset+recoil_shift+dcStark_shift+BBR_shift,np.sqrt(avg_nu_e['Sigma']**2+other_uncert**2),1).replace(u"\u2212","-")  + ' Hz', file=text_file)
            print('Hyperfine centroid value obtained from above value with {:.1f} Hz correction: \n'.format(HFS_corr)+
                    'nu_e = '+compact_uncert_str(avg_nu_e['Mean']+nu2s6p_offset+recoil_shift+dcStark_shift+BBR_shift+HFS_corr,np.sqrt(avg_nu_e['Sigma']**2+other_uncert**2),1).replace(u"\u2212","-")  + ' Hz', file=text_file)
        else:
            print('Blinded velocity-extrapolated frequency result:', file=text_file)
            print('nu_e = '+compact_uncert_str(avg_nu_e['Mean'],avg_nu_e['Sigma'],1).replace(u"\u2212","-")  + ' Hz', file=text_file)



    dfFits_FC_aver['nu_a'] = dfFits_FC_aver['nu_a'] + nu2s6p_offset
    dfFits_FC_aver['nu_e'] = dfFits_FC_aver['nu_e'] + nu2s6p_offset

    dfFits_FC_aver.to_excel(valuesFolder+plotFile+'.xlsx',index=False,
                            header=['Datagroup',
                                    'FC',
                                    'nu_a (Hz)',
                                    'nu_a_err (Hz)',
                                    'nu_a_redChi2',
                                    'nu_a_err_scaled (Hz)',	
                                    'nu_e (Hz)',
                                    'nu_e_err (Hz)',
                                    'nu_e_redChi2',
                                    'nu_e_err_scaled (Hz)',
                                    'kappa (Hz/(m/s))',
                                    'kappa_err (Hz/(m/s))',
                                    'kappa_redChi2',
                                    'kappa_err_scaled (Hz/(m/s))'])  

#    import datetime
    filename = (
#       datetime.datetime.utcnow().strftime('%Y-%m-%d') + ' '+
        plotFile)
    filepath = os.path.join(
        plotFolder, filename)
    myfig.savefig(filepath + '.pdf', dpi=400)
 #   myfig.savefig(filepath + '.png', dpi=400)


    plt.close('all')

# %%
