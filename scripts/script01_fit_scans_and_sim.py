# -*- coding: utf-8 -*-
"""
Created on Thu Mar 16 16:36:18 2023

@author: Vitaly Wirthl
"""
import numpy as np
import scipy.optimize
import scipy.special
import pandas as pd
import matplotlib.pyplot as plt
import os
import matplotlib.gridspec as gridspec
import datetime

use_jac=True
sort_data=False

import scripts.sim_plts_thesis as plts
plts.set_params()

from scripts.fitfunctions import *
from scripts.statisticsfunctions import *
from config import *

if use_comb_freq:
    dfCombData=pd.read_pickle(folder_results_comb+CombFile)#+'.pickle')

if correct_for_maser:
    dfMaser=pd.read_pickle(folder_results_maser+MaserResultsFile)#+'.pickle')

def fit_scans_and_sim(current_group):



    if current_group in LFSgroups:
        if current_group=='G13':
            alpha0=7.5e-3 #8
        if current_group=='G14':
            alpha0=12e-3 #13
    else:
        alpha0=0

    print('*********** Fitting scans and simulations for datagroup {:s} **********'.format(current_group))

    
    folder_velocities=folder_all_groups+current_group+'\\DelayVels'
    velocities_df=pd.read_csv(folder_velocities+'\\'+current_group+'_delay_vels.csv', delimiter=",")
    velocities=np.array(velocities_df['V_Mean'].values)
    
    delta_nu_SOD=-np.array(velocities_df['V_RMS'].values)**2*730.690e12/2/299792458**2 
    

    folder_scans=folder_all_groups+current_group+'\\Exp'
    
    
    scanfiles=os.listdir(folder_scans)
    
    folder_sim=folder_all_groups+current_group+'\\Sim'
    
    
    if alpha0<6e-3:
        sampling_points=np.array([1.2546e6,1.8268e6,2.3104e6,2.7650e6,3.2190e6,3.6946e6,4.2171e6,4.8251e6,5.5937e6,6.7157e6,8.9994e6,22.6662e6,36.3331e6,50.0e6])
    elif alpha0<10e-3:
        sampling_points=np.array([1.0702e6,2.1403e6,3.1276e6,3.9738e6,4.7845e6,5.6168e6,6.5261e6,7.5970e6,9.0174e6,11.4855e6,15.7649e6,24.3237e6,37.1618e6,50.0e6])
    else:
        sampling_points=np.array([2.2173e6,3.2279e6,4.0820e6,4.8856e6,5.6899e6,6.5368e6,7.4773e6,8.5973e6,10.0931e6,12.6965e6,16.8413e6,25.1310e6,37.5655e6,50.0e6])


    freq_vals_no_offset=np.concatenate((-sampling_points,[0],sampling_points))
    freq_centered=freq_vals_no_offset

    print('Fitting BM and LFS simulations')    
    for sim_ID in ['BM','LFS']:
        dfSim=pd.read_csv(folder_sim+'\\'+current_group+'_'+sim_ID+'_offset_sim.csv')
     
        dfFits = pd.DataFrame()
    
        for CEM_ID in ['Ch0','Ch1']:
            for DelayID in ['1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16']: 
            
                mask=[np.round(dfSim['Detuning'].values[i],3) in np.round(freq_centered,3) for i in range(len(dfSim['Detuning'].values))]
                xvals=dfSim[mask]['Detuning'].values
                yvals=dfSim[mask][CEM_ID+'D'+DelayID].values

                
                if current_group in LFSgroups:
                    kL=2*np.pi/(410.2867e-9)
                    vx_val=alpha0*velocities[int(DelayID)-1]
                    doppler_shift=vx_val*kL/(2*np.pi)

                    popt, pcov = scipy.optimize.curve_fit(DoubleVoigt,xvals,yvals,(-1.05*doppler_shift,np.max(yvals),5e6,3e6,1.05*doppler_shift,np.max(yvals),np.min(yvals)/2),sigma=np.sqrt(yvals), jac=(Df_VoigtDoublet if use_jac else None),absolute_sigma=True,ftol=1e-14, xtol=1e-14, gtol=1e-14,epsfcn=1e-14,maxfev=1000)    
                    
                    nu0=(popt[0]*popt[1]+popt[4]*popt[5])/(popt[1]+popt[5])

                    x1 = popt[0]
                    x2 = popt[4]
                    a1 = popt[1]
                    a2 = popt[5]
                    varx1=pcov[0][0]
                    varx2=pcov[4][4]
                    vara1=pcov[1][1]
                    vara2=pcov[5][5]
                    covx1x2=pcov[0][4]
                    covx1a1=pcov[0][1]
                    covx1a2=pcov[0][5]
                    covx2a1=pcov[4][1]
                    covx2a2=pcov[4][5]
                    cova1a2=pcov[1][5]
                    
                    nu0_uncert = nu0uncert_DoubleVoigt(x1,x2,a1,a2,varx1,varx2,vara1,vara2,covx1x2,covx1a1,covx1a2,covx2a1,covx2a2,cova1a2)
                    
                    ### Calculating alpha angle from Double Voigt fit, taking average of +- peak frequency as the Doppler shift:
                    nu0_avg= (np.abs(popt[0])+np.abs(popt[4]))/2
                    vx=nu0_avg*410e-9
                    alpha=vx/velocities[int(DelayID)-1]

                    results = {
                            'Delay': DelayID,
                            'CEM_ID': CEM_ID,
                            'popt': popt,
                            'pcov': pcov,
                            'nu0':nu0,
                            'nu0_uncert': nu0_uncert,
                            'alpha': alpha,
                            }

                else:
                    popt, pcov = scipy.optimize.curve_fit(Voigt,xvals,yvals,(0,np.max(yvals),np.min(yvals),3e6,6e6),sigma=np.sqrt(yvals), jac=(Df_Voigt if use_jac else None),absolute_sigma=True,ftol=1e-14, xtol=1e-14, gtol=1e-14,epsfcn=1e-14,maxfev=1000)
                    nu0=popt[0]
                    nu0_uncert=np.sqrt(pcov[0][0])
                    results = {
                            'Delay': DelayID,
                            'CEM_ID': CEM_ID,
                            'popt': popt,
                            'pcov': pcov,
                            'nu0':nu0,
                            'nu0_uncert': nu0_uncert,
                            }
                
    
                    
                dfFits = pd.concat([dfFits,pd.Series({**results}).to_frame().T],ignore_index=True)
                       
        dfFits.to_pickle(folder_results_fits_sim+current_group+'_Sim'+sim_ID+'_VoigtFits'+'.pickle')
    
    dfFits_SimBM=pd.read_pickle(folder_results_fits_sim+current_group+'_SimBM_VoigtFits.pickle')
    dfFits_SimLFS=pd.read_pickle(folder_results_fits_sim+current_group+'_SimLFS_VoigtFits.pickle')
    
    
    #################### FIT SCANS ######################
       
    dfFits = pd.DataFrame()
    
    print('Fitting line scans')  
    for CEM_ID in ['Ch0','Ch1']:
        print('----- CEM_ID={:s} -----'.format(CEM_ID))
        for DelayID in ['1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16']: 
            print('------ DelayID={:s} -----'.format(DelayID))
            nu0_list=[]
            nu0uncert_list=[]
            for iScan in range(len(scanfiles)): 
                current_scanDID=scanfiles[iScan].split(current_group+'_')[1].split('_exp')[0]#scanfiles[iScan] #myscans_DIDs[iScan]
                #print('scanDID={:s}'.format(current_scanDID))
                path=folder_scans+'\\'+scanfiles[iScan]#+'.csv'
                current_scan_pd=pd.read_csv(path)
               
                ######################## Checking in metadata whether this scan is included in Lothars analysis (e.g. if the fit not converged, the scan was excluded) ###############
                ### Two masks are provided per data group:
                ###"DataGroupID/Metadata/DataGroupID_scans_valid_mask.csv": valid scan-detector-delay combinations according to the exclusion criteria of the data group and scan metadata. For invalid data according to this mask, the counts have been set to -1 in the corresponding data files.
                ### "DataGroupID/Metadata/DataGroupID_scans_valid_main_analysis_mask.csv": same as above, but also excluding scan-detector-delay combinations for which the line shape fit did not converge in the main data analysis. For invalid data according to this mask, the counts are not set to -1.
                ########################## using  ..scans_valid_main_analysis_mask.csv mask here ###################
                path_metadata_scans=folder_scans.split('Exp')[0]+'Metadata\\'+current_group+'_scans.csv'
                scans_metadata_pd=pd.read_csv(path_metadata_scans)
    
                scanDID_noFUID=int(current_scanDID.split('_')[1])
                scanFUID=int(current_scanDID.split('_')[0])
    
                mask_UID= (scans_metadata_pd['dfScans_ScanDID']==scanDID_noFUID) &  (scans_metadata_pd['dfScans_ExpParams_FCUID']==scanFUID)
                current_scanUID=scans_metadata_pd[mask_UID]['ScanUID'].values[0]
                
                path_metadata_analysis=folder_scans.split('Exp')[0]+'Metadata\\'+current_group+'_scans_valid_main_analysis_mask.csv'
                analysis_metadata_pd=pd.read_csv(path_metadata_analysis)
                
                mask_analysis=(analysis_metadata_pd['ScanUID']==current_scanUID) & (analysis_metadata_pd['MCSchannelID'] ==  CEM_ID) & (analysis_metadata_pd['DelayID']==int(DelayID))
    
                include_in_analysis=analysis_metadata_pd[mask_analysis]['Value'].values[0]
                #################################################
                
                if use_comb_freq:
                    if correct_for_maser:                     
                        meashour=int(scans_metadata_pd[mask_UID]['dfScans_Timestamp'].values[0].split(' ')[1].split(':')[0])
                        current_day=scans_metadata_pd[mask_UID]['dfScans_Timestamp'].values[0].split(' ')[0]
                        if meashour < 9: #if scans after midnight, use previous day
                            prev_day=datetime.datetime.strptime(current_day,'%Y-%m-%d')-datetime.timedelta(days=1)
                            current_measday=prev_day.strftime('%Y-%m-%d')
                        else:
                            current_measday=current_day
                        measday_mask= (dfMaser['Daystring'] == current_measday)
                        if len(dfMaser[measday_mask]['Deltaf_to_fCs'].values) == 0 :
                            print('Missing maser data for day '+current_measday+', setting maser corr. factor to 1 on this day')
                            maser_corr_factor=1
                        else:
                            maser_corr_factor=1+dfMaser[measday_mask]['Deltaf_to_fCs'].values[0]
                    else:
                        maser_corr_factor=1
                    #maser_corr_factor=1+1e-12
                    #print(maser_corr_factor)

                    mask_combdata= (dfCombData['ScanUID'] == current_scanUID)
                    #mask_nan= pd.isnull(dfCombData[mask_combdata]['FP3freq_from40th'])
                    #if len(dfCombData[mask_combdata][mask_nan])
                    if pd.isnull(dfCombData[mask_combdata]['FP3freq_from40th']).values[0] == False:
                        if current_group in FS6P32groups:
                            xdata=maser_corr_factor*(2*dfCombData[mask_combdata]['FP3freq_from40th'].values[0]+2*current_scan_pd['Freq_AOM2S6P_Freq'].values+current_scan_pd['Freq_AOM2S6PFSOffset_Freq'].values)-nu2s6p_offset_32
                        else:
                            xdata=maser_corr_factor*(2*dfCombData[mask_combdata]['FP3freq_from40th'].values[0]+2*current_scan_pd['Freq_AOM2S6P_Freq'].values)-nu2s6p_offset_12
                    else:
                        print('Missing comb data for scan '+str(current_scanDID))
                        include_in_analysis = False
                else:
                    xdata=current_scan_pd['Freq_Blind'].values
                ydata=current_scan_pd[CEM_ID+'D'+DelayID].values
                
                if sort_data:
                    sort_ind = xdata.argsort()
                    xdata = xdata[sort_ind[::1]]
                    ydata = ydata[sort_ind[::1]]
                
                if include_in_analysis:         
                    if current_group in LFSgroups:
                        kL=2*np.pi/(410.2867e-9)
                        vx_val=alpha0*velocities[int(DelayID)-1]
                        doppler_shift=vx_val*kL/(2*np.pi)
                       
                        # popt, pcov = scipy.optimize.curve_fit(DoubleVoigt,xdata,ydata,(-1.05*doppler_shift,np.max(ydata),5e6,3e6,1.05*doppler_shift,np.max(ydata),0),sigma=np.sqrt(ydata), jac=(Df_VoigtDoublet if use_jac else None),absolute_sigma=True,ftol=1e-14, xtol=1e-14, gtol=1e-14,epsfcn=1e-14,maxfev=1000)    

                        # nu0=(popt[0]*popt[1]+popt[4]*popt[5])/(popt[1]+popt[5])

                        # x1 = popt[0]
                        # x2 = popt[4]
                        # a1 = popt[1]
                        # a2 = popt[5]
                        # varx1=pcov[0][0]
                        # varx2=pcov[4][4]
                        # vara1=pcov[1][1]
                        # vara2=pcov[5][5]
                        # covx1x2=pcov[0][4]
                        # covx1a1=pcov[0][1]
                        # covx1a2=pcov[0][5]
                        # covx2a1=pcov[4][1]
                        # covx2a2=pcov[4][5]
                        # cova1a2=pcov[1][5]

                        #delta_nu=1.05*2*alpha0*730.690e12/299792458*velocities[int(DelayID)-1]
                        # poptV, pcov = scipy.optimize.curve_fit(DoubleVoigt_LotharOrder,xdata,ydata,(-delta_nu/2,np.max(ydata),0,5e6,3e6,delta_nu/2,np.max(ydata)),sigma=np.sqrt(ydata), jac=(Df_VoigtDoublet_LotharOrder if use_jac else None),absolute_sigma=True,ftol=1e-14, xtol=1e-14, gtol=1e-14,epsfcn=1e-14,maxfev=1000)    
                        poptV, pcov = scipy.optimize.curve_fit(DoubleVoigt_LotharOrder,xdata,ydata,(-1.05*doppler_shift,np.max(ydata),0,5e6,3e6,1.05*doppler_shift,np.max(ydata)),sigma=np.sqrt(ydata), jac=(Df_VoigtDoublet_LotharOrder if use_jac else None),absolute_sigma=True,ftol=1e-14, xtol=1e-14, gtol=1e-14,epsfcn=1e-14,maxfev=1000)    

                        nu0=(poptV[0]*poptV[1]+poptV[5]*poptV[6])/(poptV[1]+poptV[6])
                        x1 = poptV[0]
                        x2 = poptV[5]
                        a1 = poptV[1]
                        a2 = poptV[6]
                        varx1=pcov[0][0]
                        varx2=pcov[5][5]
                        vara1=pcov[1][1]
                        vara2=pcov[6][6]
                        covx1x2=pcov[0][5]
                        covx1a1=pcov[0][1]
                        covx1a2=pcov[0][6]
                        covx2a1=pcov[5][1]
                        covx2a2=pcov[5][6]
                        cova1a2=pcov[1][6]
                        
                        nu0_uncert = nu0uncert_DoubleVoigt(x1,x2,a1,a2,varx1,varx2,vara1,vara2,covx1x2,covx1a1,covx1a2,covx2a1,covx2a2,cova1a2)
                        
                        ### Calculating alpha angle from Double Voigt fit, taking average of +- peak frequency as the Doppler shift:
                        nu0_avg= (np.abs(popt[0])+np.abs(popt[4]))/2
                        vx=nu0_avg*410e-9
                        alpha=vx/velocities[int(DelayID)-1]

                
                    else:
                        poptL, pcov = scipy.optimize.curve_fit(Lor,xdata,ydata,(0,max(ydata),min(ydata),10e6),sigma=np.sqrt(ydata), absolute_sigma=True)#,ftol=1e-14, xtol=1e-14, gtol=1e-14,epsfcn=1e-14,maxfev=1000)
                        popt, pcov = scipy.optimize.curve_fit(Voigt,xdata,ydata,(poptL[0],poptL[1],poptL[2],poptL[3],poptL[3]),sigma=np.sqrt(ydata), jac=(Df_Voigt if use_jac else None), absolute_sigma=True,ftol=1e-14, xtol=1e-14, gtol=1e-14,epsfcn=1e-14,maxfev=2000)
                        nu0=popt[0]
                        nu0_uncert=np.sqrt(pcov[0][0])
                        alpha=np.nan

                    nu0_uncert_cutoff = 5e10 #5e5
                    if nu0_uncert < nu0_uncert_cutoff:      
                        nu0_list.append(nu0)
                        nu0uncert_list.append(nu0_uncert)
                        
                        
                        ##### CORRECTIONS: #########
                        mask_SimBM = (dfFits_SimBM['CEM_ID']==CEM_ID) & (dfFits_SimBM['Delay']==DelayID) 
                        nu0_BM=dfFits_SimBM[mask_SimBM]['nu0'].values[0]
                        
                        mask_SimLFS = (dfFits_SimLFS['CEM_ID']==CEM_ID) & (dfFits_SimLFS['Delay']==DelayID) 
                        nu0_LFS=dfFits_SimLFS[mask_SimLFS]['nu0'].values[0]
                        
                        nu0_SOD=delta_nu_SOD[int(DelayID)-1]
                        ##### ######### ###########
                        
                        
                        results = {
                        'ScanUID': current_scanUID,
                        'ScanDIDwFC': current_scanDID,
                        'ScanDID': int(current_scanDID.split('_')[1]),
                        'FC': int(current_scanDID.split('_')[0]),
                        'Delay': DelayID,
                        'CEM_ID': CEM_ID,
                        'popt': popt,
                        'pcov': pcov,
                        'nu0': nu0,
                        'nu0_BM': nu0_BM,
                        'nu0_LFS': nu0_LFS,
                        'nu0_SOD': nu0_SOD,
                        'nu0_corr': nu0-nu0_BM-nu0_LFS-nu0_SOD,
                        'nu0_uncert': nu0_uncert,
                        'alpha': alpha,
                        }
                    
                        dfFits = pd.concat([dfFits,pd.Series({**results}).to_frame().T],ignore_index=True)
                    else:
                        print('nu0_uncert > 5e5 for scan '+current_scanDID)                
    
    if use_comb_freq:
        if correct_for_maser:
            dfFits.to_pickle(folder_results_fits_comb_freq_maser_corr+current_group+'_VoigtFits'+'.pickle')
        else:
            dfFits.to_pickle(folder_results_fits_comb_freq+current_group+'_VoigtFits'+'.pickle')
    else:
        dfFits.to_pickle(folder_results_fits_blind+current_group+'_VoigtFits'+'.pickle')

