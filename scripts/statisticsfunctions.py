# Selected functions from pyhs.statfunc (package written by Lothar Maisenbacher)

from scipy.stats import chi2
from scipy.stats import norm
import numpy as np
import pandas as pd

def avg_uncorr_meas(a, sa=None, axis=0, return_dict=True, full_output=True):
    """
    Weighted average of values `a` with associated uncertainties `sa`.
    The values in `a` are assumed to be uncorrelated.
    """
    # a = array_no_nan(a)
    # sa = array_no_nan(sa)

    a_masked=np.ma.array(a, mask=pd.isnull(a))
    a=np.ma.getdata(a_masked[a_masked.mask == False])
    if sa is not None:
        sa=np.array(sa)[a_masked.mask == False]

    a = np.atleast_1d(a)

    mean = np.nan
    sigma = np.nan
    chi_sq = np.nan
    red_chi_sq = np.nan
    chi_sq_sigma = np.nan
    red_chi_sq_sigma = np.nan
    p_value = np.nan
    significance = np.nan
    if sa is None:
        mean = np.mean(a, axis=axis)
    elif a.shape[axis] > 0:
        sa = np.atleast_1d(sa)
        sigma2 = 1/np.sum(1/sa**2, axis=axis)
        mean = np.sum(a/sa**2, axis=axis)*sigma2
        sigma = np.sqrt(sigma2).squeeze()
        dof = len(a)-1
        chi_sq = (
            np.sum((a-mean)**2/sa**2, axis=axis)
            .squeeze())
        chi_sq_sigma = np.sqrt(2*dof)
        mean = mean.squeeze()
        if len(a) > 1:
            red_chi_sq = chi_sq/dof
            red_chi_sq_sigma = chi_sq_sigma/dof
        else:
            red_chi_sq = chi_sq
            red_chi_sq_sigma = chi_sq_sigma
        p_value = get_p_value_from_chi_sq(chi_sq, dof)
        significance = get_significance(p_value)

    if return_dict:
        output = {
            'Mean': mean,
            'Sigma': sigma,
            'ChiSq': chi_sq,
            'RedChiSq': red_chi_sq,
            }
        if full_output:
            output = {
                **output,
                'NPoints': len(a),
                'ChiSqSigma': chi_sq_sigma,
                'RedChiSqSigma': red_chi_sq_sigma,
                'PValue': p_value,
                'Significance': significance,
                }
    else:
        if full_output:
            output = np.array([
                mean, sigma,
                chi_sq, red_chi_sq,
                len(a),
                chi_sq_sigma, red_chi_sq_sigma,
                p_value, significance,
                ])
        else:
            output = np.array([
                mean, sigma,
                chi_sq, red_chi_sq])

    return output

def avg_corr_meas(a, b, sa, sb, r, return_dict=False, wa=None):
    """
    Average N pairs of values `a` and `b` with associated uncertainties `sa` and `sb`,
    taking into account the correlation (given by the linear correlation
    coefficient `r`) between the values in `a` and in `b`. The values within `a` or `b`
    are assumed to be not correlated.
    I.e., values `a[i]` and `b[j]` are correlated with `r` for `i = j`, while `r = 0`
    for `i != j`.
    The weights of the elements of `a` can be supplied as keyword argument `wa` (list-like),
    either as a single value (or 1-element array) that applies to all N values, or N values.
    Otherwise, they are determined for each element such that the combined uncertainty is minimized.
    The weights are normalized, that is, the weights `wb` of the elements of `b` are `1-wa`.
    """
    a = np.atleast_1d(a)
    b = np.atleast_1d(b)
    sa = np.atleast_1d(sa)
    sb = np.atleast_1d(sb)
    r = np.atleast_1d(r)

    if wa is None:
        # Find optimal weights
        if np.all(sa == sb):
            wa = np.array([0.5])
        else:
            covab = sa*sb*r
            wa = (sb**2-covab)/(sa**2+sb**2-2*covab)
    else:
        wa = np.atleast_1d(wa)
    wb = 1-wa

    # Combined uncertainty
    sc = np.sqrt(wa**2*sa**2+wb**2*sb**2+2*wa*wb*sa*sb*r)

    # Weighted average
    c = wa*a+wb*b

    if len(a) == 1:
        if return_dict:
            output = {
                'Mean': c[0], 'Sigma': sc[0],
                'WeightA': wa[0], 'WeightB': wb[0],
                }
        else:
            output = c[0], sc[0], wa[0], wb[0]
    else:
        output = c, sc, wa, wb
    return output

def get_p_value_from_chi_sq(chi_sq, dof):
    """
    Get single-sided p-value from chi^2 `chi_sq` and DOF `dof`,
    see PDG2018, Eq. (39.71).
    This is the probability of finding a chi^2 at least as large as `chi_sq`,
    assuming the chi^2 distribution with DOF `dof` describes the data.
    """
    return 1-chi2.cdf(chi_sq, dof)

def get_significance(p_value):
    """
    Get the single-sided significance Z associated with the p-value `p_value`,
    see PDG2018, Eq. (39.46):
    "Often one converts the p-value into an equivalent significance Z,
    defined so that a Z standard deviation upward fluctuation of a
    Gaussian random variable would have an upper tail area equal to p."
    I.e. the value Z for which the area of a normal distribution with standard
    deviation 1 integrated within -infinity - +Z is equal to 1-`p_value`.
    """
    return norm.isf(p_value)

def compact_uncert_str(value, error, sigErrDigits=None, expNot=False, tex=True):
    """
    Convert a measurement value float `value` with associated uncertainty
    float `error` into a compact uncertainty string, with the uncertainty in the
    last digits given in parenthesis. The number of digits of the uncertainty
    given is set by int `sigErrDigits`. Exponential notation can be switched on
    with bool `expNot`.
    """
    if sigErrDigits is None:
        sigErrDigits = 1
    else:
        sigErrDigits = int(sigErrDigits)
    notation = 'f'
    if any([np.isnan(value), np.isnan(error)]):
        output = 'NaN(NaN)'
    else:
        error = np.abs(error)
        if expNot:
            if value == 0.:
                scale = 1
            else:
                scale = int(np.floor(np.log10(np.abs(value))))
            scaleStr = (
                r'$\times$10$^{{{}}}$'.format(scale) if tex
                else 'E{:d}'.format(scale))
            value = value*10**-scale
            error = error*10**-scale
        else:
            scale = 1
            scaleStr = ''
        if error == 0.:
            digits = sigErrDigits-1
        else:
            digits = -int(np.floor(np.log10(error)))+sigErrDigits-1
            error_ = np.round(error, digits)
            digits = -int(np.floor(np.log10(error_)))+sigErrDigits-1
        if digits < 0:
            output = (
                '{1:.{3}{0}}({2:.0{0}}){4}'
                .format(notation, value, error, 0, scaleStr))
        else:
            if np.round(error, digits) >= 1:
                output = (
                    '{1:.{3}{0}}({2:.{4}{0}}){5}'
                    .format(
                        notation, value, error,
                        digits, digits, scaleStr))
            else:
                output = (
                    '{1:.{3}{0}}({2:.0{0}}){4}'
                    .format(
                        notation, value, error*10**digits,
                        digits, scaleStr))
    output=output.replace("-",u"\u2212") #replace hyphen by minus sign
    return output

def format_number(value, sig_digits=None,
                  exp_not=False, tex=True, auto_exp_not=True):
    """
    Convert a measurement value float `value` into a string.
    The number of digits of the value given is set by int `sig_digits`.
    Exponential notation can be switched on with bool `exp_not`.
    """
    error = value
    if sig_digits is None:
        sig_digits = 1
    else:
        sig_digits = int(sig_digits)
    notation = 'f'
    if np.isnan(value):
        output = 'NaN'
    else:
        if auto_exp_not and (
                value > 10**sig_digits or value < 10**-(sig_digits-1)):
            exp_not = True
        error = np.abs(error)
        if exp_not:
            if value == 0.:
                scale = 1
            else:
                scale = int(np.floor(np.log10(np.abs(value))))
            scale_str = (
                r'$\times$10$^{{{}}}$'.format(scale) if tex
                else 'E{:d}'.format(scale))
            value = value*10**-scale
            error = error*10**-scale
        else:
            scale = 1
            scale_str = ''
        if error == 0.:
            digits = sig_digits-1
        else:
            digits = -int(np.floor(np.log10(error)))+sig_digits-1
            error_ = np.round(error, digits)
            digits = -int(np.floor(np.log10(error_)))+sig_digits-1
        if digits < 0:
            output = (
                '{1:.{2}{0}}{3}'
                .format(notation, value, 0, scale_str))
        else:
            if np.round(error, digits) >= 1:
                output = (
                    '{1:.{2}{0}}{3}'
                    .format(
                        notation, value,
                        digits, scale_str))
            else:
                output = (
                    '{1:.{2}{0}}{3}'
                    .format(
                        notation, value,
                        digits, scale_str))
    output=output.replace("-",u"\u2212") #replace hyphen by minus sign
    return output
