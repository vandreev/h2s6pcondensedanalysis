# -*- coding: utf-8 -*-
"""
Created on Thu Mar 20 10:36:18 2023

@author: Vitaly Wirthl
"""
import numpy as np
import scipy.optimize
import scipy.special
import pandas as pd
import matplotlib.pyplot as plt
import os
import matplotlib.gridspec as gridspec
from scipy.stats import norm

def array_no_nan(myarray):
    data=np.ma.array(myarray, mask=np.isnan(myarray))
    return data[data.mask == False]
    
import scripts.sim_plts_thesis as plts
plts.set_params()

from scripts.fitfunctions import *
from scripts.statisticsfunctions import *
from config import *


def plot_analysis_scans_individually(current_group):
    print('*********** Plotting individual scan analysis Group {:s} ***********'.format(current_group))

    FitsFileName=current_group+'_VoigtFits_DelayAnalysisIndividualScans'
    if use_comb_freq:
        if correct_for_maser:
             dfFitsDelay=pd.read_pickle(folder_results_fits_comb_freq_maser_corr+FitsFileName+'.pickle').convert_dtypes()
        else:
            dfFitsDelay=pd.read_pickle(folder_results_fits_comb_freq+FitsFileName+'.pickle').convert_dtypes()    
    else:
        dfFitsDelay=pd.read_pickle(folder_results_fits_blind+FitsFileName+'.pickle').convert_dtypes()
    
    
    
    plot_params = {
        'FontsizeFitResults': plts.params['FontsizeReg'],
        'FontsizeLegend': 7,
        }
    plot_params['Data'] = {
        'linestyle': '',
        'marker': 'o',
        'elinewidth': 0.2,
        'linewidth': 0.5,
        'markeredgewidth': 0.6,
        'capsize': 0.8,
        'markersize': 0.5}
    
    
    figure_height=5
    myfig = plt.figure(
        figsize=(1.3*plts.pagewidth, figure_height), constrained_layout=True)
    myfig.clf()
    widths = [8,1.1,1.1]
    heights = [1,1,1]
    
    # Create main layout splitting figure:
    gs_main = gridspec.GridSpec(
        ncols=3, nrows=3, figure=myfig,
        height_ratios=heights, width_ratios=widths
        )

    
    #Create axes
    ax_nu_a = myfig.add_subplot(gs_main[0])
    ax_nu_e = myfig.add_subplot(gs_main[3],sharex=ax_nu_a)
    ax_kappa = myfig.add_subplot(gs_main[6],sharex=ax_nu_a)
    #Create axes
    ax_nu_a_hist_Ch0 = myfig.add_subplot(gs_main[1])
    ax_nu_a_hist_Ch1 = myfig.add_subplot(gs_main[2])
    
    ax_nu_e_hist_Ch0 = myfig.add_subplot(gs_main[4])
    ax_nu_e_hist_Ch1 = myfig.add_subplot(gs_main[5])
    
    ax_kappa_hist_Ch0 = myfig.add_subplot(gs_main[7])
    ax_kappa_hist_Ch1 = myfig.add_subplot(gs_main[8])
    
    # Dont show tick labels on shared axes 
    plt.setp(ax_nu_a.get_xticklabels(), visible=False)
    plt.setp(ax_nu_e.get_xticklabels(), visible=False)

    
    nu_a_list_Ch0=np.array(dfFitsDelay['nu_a_list_Ch0'].values[0])
    nu_a_list_Ch1=np.array(dfFitsDelay['nu_a_list_Ch1'].values[0])
    

    mask_bothChData= ~(np.isnan(nu_a_list_Ch0) | np.isnan(nu_a_list_Ch1))
    
    nu_e_list_Ch0=np.array(dfFitsDelay['nu_e_list_Ch0'].values[0])#[mask]
    k_list_Ch0=np.array(dfFitsDelay['k_list_Ch0'].values[0])#[mask]
    
    nu_a_err_list_Ch0=np.array(dfFitsDelay['nu_a_err_list_Ch0'].values[0])#[mask]
    nu_e_err_list_Ch0=np.array(dfFitsDelay['nu_e_err_list_Ch0'].values[0])#[mask]
    k_err_list_Ch0=np.array(dfFitsDelay['k_err_list_Ch0'].values[0])#[mask]
    
    nu_e_list_Ch1=np.array(dfFitsDelay['nu_e_list_Ch1'].values[0])#[mask]
    k_list_Ch1=np.array(dfFitsDelay['k_list_Ch1'].values[0])#[mask]
    
    nu_a_err_list_Ch1=np.array(dfFitsDelay['nu_a_err_list_Ch1'].values[0])#[mask]
    nu_e_err_list_Ch1=np.array(dfFitsDelay['nu_e_err_list_Ch1'].values[0])#[mask]
    k_err_list_Ch1=np.array(dfFitsDelay['k_err_list_Ch1'].values[0])#[mask]
     
    i_scans=np.arange(1,len(nu_a_list_Ch0)+1,1)
    
    avg=avg_uncorr_meas(nu_a_list_Ch0,nu_a_err_list_Ch0)
    avgCh0=avg
    ax_nu_a.errorbar(i_scans-0.5,nu_a_list_Ch0,yerr=nu_a_err_list_Ch0,color='tab:blue',**plot_params['Data'],label=r'Top detector, N={:.0f}'.format(len(nu_a_list_Ch0[~np.isnan(nu_a_list_Ch0)]))+'\n'+'Avg.: '
                     +compact_uncert_str(1e-3*avg['Mean'],1e-3*avg['Sigma'],3)+' kHz'
                     +', $\chi_\mathrm{red}^2$='+compact_uncert_str(avg['RedChiSq'],avg['RedChiSqSigma'],2))
    avg=avg_uncorr_meas(nu_a_list_Ch1,nu_a_err_list_Ch1)
    avgCh1=avg
    ax_nu_a.errorbar(i_scans,nu_a_list_Ch1,yerr=nu_a_err_list_Ch1,color='tab:orange',**plot_params['Data'],label=r'Bottom detector, N={:.0f}'.format(len(nu_a_list_Ch1[~np.isnan(nu_a_list_Ch1)]))+'\n'+'Avg.: '
                     +compact_uncert_str(1e-3*avg['Mean'],1e-3*avg['Sigma'],3)+' kHz'
                      +', $\chi_\mathrm{red}^2$='+compact_uncert_str(avg['RedChiSq'],avg['RedChiSqSigma'],2))
    
    r,rerr=scipy.stats.pearsonr(nu_a_list_Ch0[mask_bothChData],nu_a_list_Ch1[mask_bothChData])

    avg_vals=avg_corr_meas(nu_a_list_Ch0[mask_bothChData],nu_a_list_Ch1[mask_bothChData],nu_a_err_list_Ch0[mask_bothChData],nu_a_err_list_Ch1[mask_bothChData],r)
    
    #adding values to the average where one or other channel has nan values (not included in above correlated average)
    nan_indices_Ch1=np.argwhere(np.isnan(nu_a_list_Ch1))
    nan_indices_Ch0=np.argwhere(np.isnan(nu_a_list_Ch0))
    concat_avg_vals=np.concatenate((avg_vals[0],[nu_a_list_Ch0[i][0] for i in nan_indices_Ch1],[nu_a_list_Ch1[i][0] for i in nan_indices_Ch0]))
    concat_avg_err_vals=np.concatenate((avg_vals[1],[nu_a_err_list_Ch0[i][0] for i in nan_indices_Ch1],[nu_a_err_list_Ch1[i][0] for i in nan_indices_Ch0]))
    avg=avg_uncorr_meas(concat_avg_vals,concat_avg_err_vals)

    r_nu_a=r
    
    xvals=np.arange(-10,340,0.5)
    ax_nu_a.plot(xvals,np.full(len(xvals), avg['Mean']),linestyle='-',linewidth=0.7,color='gray',alpha=0.5,label=r'Corr. avg. top and bot. det., N={:.0f}'.format(len(nu_a_list_Ch0[mask_bothChData]))+' (r='+format_number(r,2)+'):'+'\n'
                    +str(np.round(avg['Mean']/1e3,2))+'('+str(int(np.round(1e-1*avg['Sigma'])))+') kHz'
                    +', $\chi_\mathrm{red}^2$='+compact_uncert_str(avg['RedChiSq'],avg['RedChiSqSigma'],2))

    avg=avg_uncorr_meas(nu_e_list_Ch0,nu_e_err_list_Ch0)
    avgCh0=avg
    ax_nu_e.errorbar(i_scans-0.5,nu_e_list_Ch0,yerr=nu_e_err_list_Ch0,color='tab:blue',**plot_params['Data'],label=r'Top det.'+'\n'+'Avg.: '
                     +compact_uncert_str(avg['Mean']/1e3,avg['Sigma']/1e3,3)+' kHz'
                      +', $\chi_\mathrm{red}^2$='+compact_uncert_str(avg['RedChiSq'],avg['RedChiSqSigma'],2))

    avg=avg_uncorr_meas(nu_e_list_Ch1,nu_e_err_list_Ch1)
    avgCh1=avg
    ax_nu_e.errorbar(i_scans,nu_e_list_Ch1,yerr=nu_e_err_list_Ch1,color='tab:orange',**plot_params['Data'],label=r'Bottom det.'+'\n'+'Avg.: '
                    +compact_uncert_str(avg['Mean']/1e3,avg['Sigma']/1e3,3)+' kHz'
                    +', $\chi_\mathrm{red}^2$='+compact_uncert_str(avg['RedChiSq'],avg['RedChiSqSigma'],2))

    
    r,rerr=scipy.stats.pearsonr(nu_e_list_Ch0[mask_bothChData],nu_e_list_Ch1[mask_bothChData])
    
    avg_vals=avg_corr_meas(nu_e_list_Ch0[mask_bothChData],nu_e_list_Ch1[mask_bothChData],nu_e_err_list_Ch0[mask_bothChData],nu_e_err_list_Ch1[mask_bothChData],r)
    #adding values to the average where one or other channel has nan values (not included in above correlated average)
    concat_avg_vals=np.concatenate((avg_vals[0],[nu_e_list_Ch0[i][0] for i in nan_indices_Ch1],[nu_e_list_Ch1[i][0] for i in nan_indices_Ch0]))
    concat_avg_err_vals=np.concatenate((avg_vals[1],[nu_e_err_list_Ch0[i][0] for i in nan_indices_Ch1],[nu_e_err_list_Ch1[i][0] for i in nan_indices_Ch0]))
    avg=avg_uncorr_meas(concat_avg_vals,concat_avg_err_vals)
    
    r_nu_e=r

    xvals=np.arange(-10,340,0.5)
    ax_nu_e.plot(xvals,np.full(len(xvals), avg['Mean']),linestyle='-',linewidth=0.7,color='gray',alpha=0.5,label=r'Corr. avg. top and bot. det. (r='+str(np.round(r,2))+'):'+'\n'
                     +compact_uncert_str(avg['Mean']/1e3,avg['Sigma']/1e3,3)+' kHz'
                      +', $\chi_\mathrm{red}^2$='+compact_uncert_str(avg['RedChiSq'],avg['RedChiSqSigma'],2))

    avg=avg_uncorr_meas(k_list_Ch0,k_err_list_Ch0)
    ax_kappa.errorbar(i_scans-0.5,k_list_Ch0,yerr=k_err_list_Ch0,color='tab:blue',**plot_params['Data'],label=r'Top det.'+'\n'+'Avg.: '
                      +compact_uncert_str(avg['Mean'],avg['Sigma'],3)+' Hz/(m/s)'
                      +', $\chi_\mathrm{red}^2$='+compact_uncert_str(avg['RedChiSq'],avg['RedChiSqSigma'],2))
     
    avg=avg_uncorr_meas(k_list_Ch1,k_err_list_Ch1)
    ax_kappa.errorbar(i_scans,k_list_Ch1,yerr=k_err_list_Ch1,color='tab:orange',**plot_params['Data'],label=r'Top det.'+'\n'+'Avg.: '
                      +compact_uncert_str(avg['Mean'],avg['Sigma'],3)+' Hz/(m/s)'
                    +', $\chi_\mathrm{red}^2$='+compact_uncert_str(avg['RedChiSq'],avg['RedChiSqSigma'],2))
 
    r,rerr=scipy.stats.pearsonr(k_list_Ch0[mask_bothChData],k_list_Ch1[mask_bothChData])
    avg_vals=avg_corr_meas(k_list_Ch0[mask_bothChData],k_list_Ch1[mask_bothChData],k_err_list_Ch0[mask_bothChData],k_err_list_Ch1[mask_bothChData],r)
    
    #adding values to the average where one or other channel has nan values (not included in above correlated average)
    concat_avg_vals=np.concatenate((avg_vals[0],[k_list_Ch0[i][0] for i in nan_indices_Ch1],[k_list_Ch1[i][0] for i in nan_indices_Ch0]))
    concat_avg_err_vals=np.concatenate((avg_vals[1],[k_err_list_Ch0[i][0] for i in nan_indices_Ch1],[k_err_list_Ch1[i][0] for i in nan_indices_Ch0]))
    avg=avg_uncorr_meas(concat_avg_vals,concat_avg_err_vals)
    
    
    r_kappa=r
    
    xvals=np.arange(-10,340,0.5)
    ax_kappa.plot(xvals,np.full(len(xvals), avg['Mean']),linestyle='-',linewidth=0.7,color='gray',alpha=0.5,label=r'Corr. avg. top and bot. det. (r='+str(np.round(r,2))+'):'+'\n'
                    +compact_uncert_str(avg['Mean'],avg['Sigma'],3)+' Hz/(m/s)'
                    +', $\chi_\mathrm{red}^2$='+compact_uncert_str(avg['RedChiSq'],avg['RedChiSqSigma'],2))
  
    no_hist_bins=30
    
    ylim_nu_a=[-55e3,105e3]

    n, bins, patches = ax_nu_a_hist_Ch0.hist( array_no_nan(nu_a_list_Ch0),no_hist_bins,facecolor='tab:blue', density=True,alpha=0.7,edgecolor='k', linewidth=0.4,orientation=u'horizontal')
    (mu, sigma) = norm.fit( array_no_nan(nu_a_list_Ch0))
    y = norm.pdf( bins, mu, sigma)
    l = ax_nu_a_hist_Ch0.plot(y,bins,  '-', color='tab:blue',alpha=1,linewidth=1,label='Top det.')
    ax_nu_a_hist_Ch0.annotate('norm. distr. fit:\n'+'$\,\,\,\,\mu=$'
                                +str(np.round(1e-3*mu,2))+' kHz \n'
                                +'$\,\,\,\,\sigma=$'+str(np.round(1e-3*sigma,2))+' kHz',(0.15,0.65),xycoords='axes fraction',fontsize=7,fontstretch='condensed')
    
    n, bins, patches = ax_nu_a_hist_Ch1.hist( array_no_nan(nu_a_list_Ch1),no_hist_bins,facecolor='tab:orange', density=True,alpha=0.7,edgecolor='k', linewidth=0.4,orientation=u'horizontal')
    (mu, sigma) = norm.fit( array_no_nan(nu_a_list_Ch1))
    y = norm.pdf( bins, mu, sigma)
    l = ax_nu_a_hist_Ch1.plot(y,bins,  '-', color='tab:orange',alpha=1,linewidth=1,label='Bot. det.')
    ax_nu_a_hist_Ch1.annotate('norm. distr. fit:\n'+'$\,\,\,\,\mu=$'
                                +str(np.round(1e-3*mu,2))+' kHz \n'
                                +'$\,\,\,\,\sigma=$'+str(np.round(1e-3*sigma,2))+' kHz',(0.15,0.65),xycoords='axes fraction',fontsize=7,fontstretch='condensed')
    
    ax_nu_a.set_ylim(ylim_nu_a)
    ax_nu_a_hist_Ch0.set_ylim(ylim_nu_a)
    ax_nu_a_hist_Ch1.set_ylim(ylim_nu_a)
    
    
    legend=ax_nu_a_hist_Ch0.legend(#[handles[idx] for idx in order],[labels[idx] for idx in order],#bbox_to_anchor=(1.1, 1.05),
            **plts.get_leg_params(**{
                  'loc': 'upper right',
                  'ncol': 1,
                  'frameon': False,
                'fontsize': plot_params['FontsizeLegend']}))
    legend=ax_nu_a_hist_Ch1.legend(#[handles[idx] for idx in order],[labels[idx] for idx in order],#bbox_to_anchor=(1.1, 1.05),
            **plts.get_leg_params(**{
                  'loc': 'upper right',
                  'ncol': 1,
                  'frameon': False,
                'fontsize': plot_params['FontsizeLegend']}))
    

    ylim_nu_e=[-115e3,160e3]
    
    n, bins, patches = ax_nu_e_hist_Ch0.hist(array_no_nan(nu_e_list_Ch0),no_hist_bins,facecolor='tab:blue', density=True,alpha=0.7,edgecolor='k', linewidth=0.4,orientation=u'horizontal')
    (mu, sigma) = norm.fit(array_no_nan(nu_e_list_Ch0))
    y = norm.pdf( bins, mu, sigma)
    l = ax_nu_e_hist_Ch0.plot(y,bins,  '-', color='tab:blue',alpha=1,linewidth=1,label='Top det.')#'\n Gauss fit')
    ax_nu_e_hist_Ch0.annotate('norm. distr. fit:\n'+'$\,\,\,\,\mu=$'
                                +str(np.round(1e-3*mu,2))+' kHz \n'
                                +'$\,\,\,\,\sigma=$'+str(np.round(1e-3*sigma,2))+' kHz',(0.15,0.65),xycoords='axes fraction',fontsize=7,fontstretch='condensed')
    
    n, bins, patches = ax_nu_e_hist_Ch1.hist(array_no_nan(nu_e_list_Ch1),no_hist_bins,facecolor='tab:orange', density=True,alpha=0.7,edgecolor='k', linewidth=0.4,orientation=u'horizontal')
    (mu, sigma) = norm.fit(array_no_nan(nu_e_list_Ch1))
    y = norm.pdf( bins, mu, sigma)
    l = ax_nu_e_hist_Ch1.plot(y,bins,  '-', color='tab:orange',alpha=1,linewidth=1,label='Bot. det.')#'\n norm. distr.\n fit')
    ax_nu_e_hist_Ch1.annotate('norm. distr. fit:\n'+'$\,\,\,\,\mu=$'
                                +str(np.round(1e-3*mu,2))+' kHz \n'
                                +'$\,\,\,\,\sigma=$'+str(np.round(1e-3*sigma,2))+' kHz',(0.15,0.65),xycoords='axes fraction',fontsize=7,fontstretch='condensed')
    
    ax_nu_e.set_ylim(ylim_nu_e)
    ax_nu_e_hist_Ch0.set_ylim(ylim_nu_e)
    ax_nu_e_hist_Ch1.set_ylim(ylim_nu_e)
    
    legend=ax_nu_e_hist_Ch0.legend(#[handles[idx] for idx in order],[labels[idx] for idx in order],#bbox_to_anchor=(1.1, 1.05),
            **plts.get_leg_params(**{
                  'loc': 'upper right',
                  'ncol': 1,
                  'frameon': False,
                'fontsize': plot_params['FontsizeLegend']}))
    legend=ax_nu_e_hist_Ch1.legend(#[handles[idx] for idx in order],[labels[idx] for idx in order],#bbox_to_anchor=(1.1, 1.05),
            **plts.get_leg_params(**{
                  'loc': 'upper right',
                  'ncol': 1,
                  'frameon': False,
                'fontsize': plot_params['FontsizeLegend']}))


    ylim_kappa=[-640,699]
    
    n, bins, patches = ax_kappa_hist_Ch0.hist(array_no_nan(k_list_Ch0),no_hist_bins,facecolor='tab:blue', density=True,alpha=0.7,edgecolor='k', linewidth=0.4,orientation=u'horizontal')
    (mu, sigma) = norm.fit(array_no_nan(k_list_Ch0))
    y = norm.pdf( bins, mu, sigma)
    l = ax_kappa_hist_Ch0.plot(y,bins,  '-', color='tab:blue',alpha=1,linewidth=1,label='Top det.')#'\n Gauss fit')
    ax_kappa_hist_Ch0.annotate('norm. distr. fit:\n'+'$\,\,\,\,\,\,\,\,\mu=$'
                                +str(np.round(mu,1))+' Hz/(m/s) \n'
                                +'$\,\,\,\,\,\,\,\,\sigma=$'+str(int(np.round(sigma,0)))+' Hz/(m/s)',(0.05,0.65),xycoords='axes fraction',fontsize=7,fontstretch='condensed')
     
    
    n, bins, patches = ax_kappa_hist_Ch1.hist(array_no_nan(k_list_Ch1),no_hist_bins,facecolor='tab:orange', density=True,alpha=0.7,edgecolor='k', linewidth=0.4,orientation=u'horizontal')
    (mu, sigma) = norm.fit(array_no_nan(k_list_Ch1))
    y = norm.pdf( bins, mu, sigma)
    l = ax_kappa_hist_Ch1.plot(y,bins,  '-', color='tab:orange',alpha=1,linewidth=1,label='Bot. det.')#'\n norm. distr.\n fit')
    ax_kappa_hist_Ch1.annotate('norm. distr. fit:\n'+'$\,\,\,\,\,\,\,\,\mu=$'
                                +str(np.round(mu,1))+' Hz/(m/s) \n'
                                +'$\,\,\,\,\,\,\,\,\sigma=$'+str(int(np.round(sigma,0)))+' Hz/(m/s)',(0.05,0.65),xycoords='axes fraction',fontsize=7,fontstretch='condensed')
  
    ax_kappa.set_ylim(ylim_kappa)
    ax_kappa_hist_Ch0.set_ylim(ylim_kappa)
    ax_kappa_hist_Ch1.set_ylim(ylim_kappa)
    
    legend=ax_kappa_hist_Ch0.legend(#[handles[idx] for idx in order],[labels[idx] for idx in order],#bbox_to_anchor=(1.1, 1.05),
            **plts.get_leg_params(**{
                  'loc': 'upper right',
                  'ncol': 1,
                  'frameon': False,
                'fontsize': plot_params['FontsizeLegend']}))
    legend=ax_kappa_hist_Ch1.legend(#[handles[idx] for idx in order],[labels[idx] for idx in order],#bbox_to_anchor=(1.1, 1.05),
            **plts.get_leg_params(**{
                  'loc': 'upper right',
                  'ncol': 1,
                  'frameon': False,
                'fontsize': plot_params['FontsizeLegend']}))

    
    
    ax_nu_a_hist_Ch0.tick_params(axis='both',which='both',bottom=False,top=False,labelbottom=False,left=False,labelleft=False)
    ax_nu_a_hist_Ch1.tick_params(axis='both',which='both',bottom=False,top=False,labelbottom=False,left=False,labelleft=False)
    
    ax_nu_e_hist_Ch0.tick_params(axis='both',which='both',bottom=False,top=False,labelbottom=False,left=False,labelleft=False)
    ax_nu_e_hist_Ch1.tick_params(axis='both',which='both',bottom=False,top=False,labelbottom=False,left=False,labelleft=False)
    
    ax_kappa_hist_Ch0.tick_params(axis='both',which='both',bottom=False,top=False,labelbottom=False,left=False,labelleft=False)
    ax_kappa_hist_Ch1.tick_params(axis='both',which='both',bottom=False,top=False,labelbottom=False,left=False,labelleft=False)
    
    
    import matplotlib.ticker as ticker
    #Convert y axis :
    ticks = ticker.FuncFormatter(lambda y, pos: '${:.0f}$'.format( y*1e-3))
    ax_nu_a.yaxis.set_major_formatter(ticks)
    
    #Convert y axis :
    ticks = ticker.FuncFormatter(lambda y, pos: '${:.0f}$'.format( y*1e-3))
    ax_nu_e.yaxis.set_major_formatter(ticks)
    
    
    handles, labels = ax_nu_a.get_legend_handles_labels()
    order = [1,2,0]
    legend=ax_nu_a.legend([handles[idx] for idx in order],[labels[idx] for idx in order],#bbox_to_anchor=(1.1, 1.05),
            **plts.get_leg_params(**{
                  'loc': 'upper left',
                  'ncol': 3,
                  'frameon': True,
                'fontsize': plot_params['FontsizeLegend']}))
    frame = legend.get_frame()
    frame.set_edgecolor('gray')
    frame.set_linewidth(0.5)
    
    handles, labels = ax_nu_e.get_legend_handles_labels()
    order = [1,2,0]
    legend=ax_nu_e.legend([handles[idx] for idx in order],[labels[idx] for idx in order],#bbox_to_anchor=(1.1, 1.05),
            **plts.get_leg_params(**{
                  'loc': 'upper left',
                  'ncol': 3,
                  'frameon': True,
                'fontsize': plot_params['FontsizeLegend']}))
    frame = legend.get_frame()
    frame.set_edgecolor('gray')
    frame.set_linewidth(0.5)
    
    handles, labels = ax_kappa.get_legend_handles_labels()
    order = [1,2,0]
    legend=ax_kappa.legend([handles[idx] for idx in order],[labels[idx] for idx in order],#bbox_to_anchor=(1.1, 1.05),
            **plts.get_leg_params(**{
                  'loc': 'upper left',
                  'ncol': 3,
                  'frameon': True,
                'fontsize': plot_params['FontsizeLegend']}))
    frame = legend.get_frame()
    frame.set_edgecolor('gray')
    frame.set_linewidth(0.5)
    
    ax_nu_a.set_ylabel(r'$\nu_\mathrm{0,a}$ (kHz)')
    ax_nu_e.set_ylabel(r'$\nu_\mathrm{0,e}$ (kHz)')
    ax_kappa.set_ylabel(r'$\kappa$ (Hz/(m/s))')
    ax_kappa.set_xlabel(r'Resonance line scan number')
    
    ax_nu_a.set_xlim([-10,len(k_list_Ch0)+10])
    ax_nu_e.set_xlim([-10,len(k_list_Ch0)+10])
    ax_kappa.set_xlim([-10,len(k_list_Ch0)+10])
    
    suptitle='Data group: '+current_group
    if use_comb_freq:
        if current_group in FS6P32groups:
            suptitle=suptitle+'\n'+r'Frequency values for $\nu_\mathrm{0,a}$ and $\nu_\mathrm{0,e}$ are shown with an offset of '+u"\u2212"+str(nu2s6p_offset_32)+' Hz'
        else:
            suptitle=suptitle+'\n'+r'Frequency values for $\nu_\mathrm{0,a}$ and $\nu_\mathrm{0,e}$ are shown with an offset of '+u"\u2212"+str(nu2s6p_offset_12)+' Hz'
    myfig.suptitle(suptitle)

    
    plotFile='IndividualScanAnalysis_'+FitsFileName
          
    if use_comb_freq:
        if correct_for_maser:
            plotFolder = folder_plots_comb_maser_corr+current_group
            if not os.path.exists(plotFolder):
                os.makedirs(plotFolder)
        else:
            plotFolder = folder_plots_comb+current_group
            if not os.path.exists(plotFolder):
                os.makedirs(plotFolder)
    else:
        plotFolder = folder_plots_blind+current_group
        if not os.path.exists(plotFolder):
            os.makedirs(plotFolder)
    
 #   import datetime
    filename = (
  #      datetime.datetime.utcnow().strftime('%Y-%m-%d') + ' '+
        plotFile)
    filepath = os.path.join(
        plotFolder, filename)
    myfig.savefig(filepath + '.pdf', dpi=400)
    # myfig.savefig(filepath + '.png', dpi=400)
    
    dfFits = pd.DataFrame()
    
    results = {'r_nu_a': r_nu_a,
                'r_kappa': r_kappa,
                'r_nu_e': r_nu_e,
            }
            
    dfFits = pd.concat([dfFits,pd.Series({**results}).to_frame().T],ignore_index=True)
    
    if use_comb_freq:
        if correct_for_maser:
            dfFits.to_pickle(folder_results_fits_comb_freq_maser_corr+current_group+'_corr_coeff.pickle')
        else:
            dfFits.to_pickle(folder_results_fits_comb_freq+current_group+'_corr_coeff.pickle')
    else:
        dfFits.to_pickle(folder_results_fits_blind+current_group+'_corr_coeff.pickle')

    plt.close('all')
