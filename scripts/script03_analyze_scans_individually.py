# -*- coding: utf-8 -*-
"""
Created on Thu Mar 20 10:36:18 2023

@author: Vitaly Wirthl
"""
import numpy as np
import scipy.optimize
import scipy.special
import pandas as pd
import matplotlib.pyplot as plt
import os
import matplotlib.gridspec as gridspec


import scripts.sim_plts_thesis as plts
plts.set_params()

delete_points=[]   

from scripts.fitfunctions import *
from scripts.statisticsfunctions import *
from config import *

def analyze_scans_individually(current_group):
    
    print('*********** Analyzing scans individually Group {:s} ***********'.format(current_group))

    folder_scans=folder_all_groups+current_group+'\\Exp'
    scanfiles=os.listdir(folder_scans)
      
    FitsFileName=current_group+'_VoigtFits'
    if use_comb_freq:
        if correct_for_maser:
             dfFits=pd.read_pickle(folder_results_fits_comb_freq_maser_corr+FitsFileName+'.pickle').convert_dtypes()
        else:
            dfFits=pd.read_pickle(folder_results_fits_comb_freq+FitsFileName+'.pickle').convert_dtypes()    
    else:
        dfFits=pd.read_pickle(folder_results_fits_blind+FitsFileName+'.pickle').convert_dtypes()
    
    folder_velocities=folder_all_groups+current_group+'\\DelayVels'
    velocities_df=pd.read_csv(folder_velocities+'\\'+current_group+'_delay_vels.csv', delimiter=",")
    velocities=np.array(velocities_df['V_Mean'].values)
    
    CEM_ID='Ch0'
      
    nu_a_list=[]
    nu_e_list=[]
    k_list=[]
    
    nu_a_err_list=[]
    nu_e_err_list=[]
    k_err_list=[]
    
    
    for iScan in range(len(scanfiles)):

        current_scanDID=scanfiles[iScan].split(current_group+'_')[1].split('_exp')[0]
        path=folder_scans+'\\'+scanfiles[iScan]
        current_scan_pd=pd.read_csv(path)
        
        nu0_list=[]
        nu0_err_list=[]
        for DelayID in ['1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16']: 
            
            mask = (dfFits['ScanDIDwFC']== current_scanDID) & (dfFits['CEM_ID']==CEM_ID) & (dfFits['Delay']==DelayID) 
            
            if dfFits[mask].empty:
                nu0_list.append(np.nan)
                nu0_err_list.append(np.nan)
            
            else:
                nu0=dfFits[mask]['nu0_corr'].values[0]
                
                nu0_list.append(nu0)
                nu0_err_list.append(dfFits[mask]['nu0_uncert'].values[0])
        
        nu0_list=np.array(nu0_list)
        nu0_err_list=np.array(nu0_err_list)
        maskNoNan=(~np.isnan(nu0_list)) & (~np.isnan(nu0_err_list))
        if len(nu0_list[maskNoNan]) >0:
            ### Average over delays ##    
            avg=avg_uncorr_meas(nu0_list[maskNoNan],nu0_err_list[maskNoNan])
            #print(avg)
            nu_a_list.append(avg['Mean'])
            nu_a_err_list.append(avg['Sigma'])
            
            ## Extrapolation over delays###
            xdata=np.array(velocities)[maskNoNan]
            ydata=np.array(nu0_list)[maskNoNan]
            ydata_err=np.array(nu0_err_list)[maskNoNan]
            popt, pcov = scipy.optimize.curve_fit(lin_func,xdata,ydata,(0,0),sigma=ydata_err, absolute_sigma=True,ftol=1e-14, xtol=1e-14, gtol=1e-14,epsfcn=1e-14,maxfev=1000)
            nu_e_list.append(popt[1])
            k_list.append(popt[0])
            nu_e_err_list.append(np.sqrt(pcov[1][1]))
            k_err_list.append(np.sqrt(pcov[0][0]))
        else:
            nu_a_list.append(np.nan)
            nu_a_err_list.append(np.nan)
            nu_e_list.append(np.nan)
            nu_e_err_list.append(np.nan)
            k_list.append(np.nan)
            k_err_list.append(np.nan)
    
    
    nu_a_list_Ch0=nu_a_list
    nu_e_list_Ch0=nu_e_list
    k_list_Ch0=k_list
    
    nu_a_err_list_Ch0=nu_a_err_list
    nu_e_err_list_Ch0=nu_e_err_list
    k_err_list_Ch0=k_err_list

    CEM_ID='Ch1'
      
    nu_a_list=[]
    nu_e_list=[]
    k_list=[]
    
    nu_a_err_list=[]
    nu_e_err_list=[]
    k_err_list=[]
    
    
    for iScan in range(len(scanfiles)):
        
        current_scanDID=scanfiles[iScan].split(current_group+'_')[1].split('_exp')[0]
        path=folder_scans+'\\'+scanfiles[iScan]#+'.csv'
        current_scan_pd=pd.read_csv(path)
        
        nu0_list=[]
        nu0_err_list=[]
        for DelayID in ['1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16']: 

            mask = (dfFits['ScanDIDwFC']== current_scanDID) & (dfFits['CEM_ID']==CEM_ID) & (dfFits['Delay']==DelayID) 
              
            if dfFits[mask].empty:
                nu0_list.append(np.nan)
                nu0_err_list.append(np.nan)
            
            else:
                nu0=dfFits[mask]['nu0_corr'].values[0]
        
                nu0_list.append(nu0)
                nu0_err_list.append(dfFits[mask]['nu0_uncert'].values[0])
                
        nu0_list=np.array(nu0_list)
        nu0_err_list=np.array(nu0_err_list)
        maskNoNan=(~np.isnan(nu0_list)) & (~np.isnan(nu0_err_list))
        if len(nu0_list[maskNoNan]) >0:
            ### Average over delays ##    
            avg=avg_uncorr_meas(nu0_list[maskNoNan],nu0_err_list[maskNoNan])
            nu_a_list.append(avg['Mean'])
            nu_a_err_list.append(avg['Sigma'])
            
            ## Extrapolation over delays###
            xdata=np.array(velocities)[maskNoNan]
            ydata=np.array(nu0_list)[maskNoNan]
            ydata_err=np.array(nu0_err_list)[maskNoNan]
            popt, pcov = scipy.optimize.curve_fit(lin_func,xdata,ydata,(0,0),sigma=ydata_err, absolute_sigma=True,ftol=1e-14, xtol=1e-14, gtol=1e-14,epsfcn=1e-14,maxfev=1000)
            nu_e_list.append(popt[1])
            k_list.append(popt[0])
            nu_e_err_list.append(np.sqrt(pcov[1][1]))
            k_err_list.append(np.sqrt(pcov[0][0]))
        else:
            nu_a_list.append(np.nan)
            nu_a_err_list.append(np.nan)
            nu_e_list.append(np.nan)
            nu_e_err_list.append(np.nan)
            k_list.append(np.nan)
            k_err_list.append(np.nan)
    
    
    nu_a_list_Ch1=np.array(nu_a_list)
    nu_e_list_Ch1=np.array(nu_e_list)
    k_list_Ch1=np.array(k_list)
    
    nu_a_err_list_Ch1=np.array(nu_a_err_list)
    nu_e_err_list_Ch1=np.array(nu_e_err_list)
    k_err_list_Ch1=np.array(k_err_list)
    
    dfFits = pd.DataFrame()
    
    results = {'nu_a_list_Ch0': nu_a_list_Ch0,
                'nu_a_err_list_Ch0': nu_a_err_list_Ch0,
                'nu_e_list_Ch0': nu_e_list_Ch0,
                'nu_e_err_list_Ch0': nu_e_err_list_Ch0,
                'k_list_Ch0': k_list_Ch0,
                'k_err_list_Ch0': k_err_list_Ch0,
                
                'nu_a_list_Ch1': nu_a_list_Ch1,
                'nu_a_err_list_Ch1': nu_a_err_list_Ch1,
                'nu_e_list_Ch1': nu_e_list_Ch1,
                'nu_e_err_list_Ch1': nu_e_err_list_Ch1,
                'k_list_Ch1': k_list_Ch1,
                'k_err_list_Ch1': k_err_list_Ch1,
            }
            
    dfFits = pd.concat([dfFits,pd.Series({**results}).to_frame().T],ignore_index=True)
    
    if use_comb_freq:
        if correct_for_maser:
            dfFits.to_pickle(folder_results_fits_comb_freq_maser_corr+current_group+'_VoigtFits'+'_DelayAnalysisIndividualScans.pickle')
        else:
            dfFits.to_pickle(folder_results_fits_comb_freq+current_group+'_VoigtFits'+'_DelayAnalysisIndividualScans.pickle')
    else:
        dfFits.to_pickle(folder_results_fits_blind+current_group+'_VoigtFits'+'_DelayAnalysisIndividualScans.pickle')

