from scripts.fitfunctions import *
from config import *
from scripts.statisticsfunctions import *

def calculate_prad_and_FS_centroid(filename2S612,filename2S632):
    if correct_for_maser:
        valuesFolder = folder_results_values_comb_freq_maser_corr
    else:   
        valuesFolder = folder_results_values_comb_freq

    with open(valuesFolder+filename2S612+' nu_e FCaver value.txt', "r") as text_file:
        content_lines = text_file.readlines() 
        HFScentroid_2S612_value=float(content_lines[5].split('= ')[1].split('(')[0])
        HFScentroid_2S612_uncert=float(content_lines[5].split('= ')[1].split('(')[1].split(')')[0])

    with open(valuesFolder+filename2S632+' nu_e FCaver value.txt', "r") as text_file:
        content_lines = text_file.readlines() 
        HFScentroid_2S632_value=float(content_lines[5].split('= ')[1].split('(')[0])
        HFScentroid_2S632_uncert=float(content_lines[5].split('= ')[1].split('(')[1].split(')')[0])

    FS_centroid_value=HFScentroid_2S612_value*1/3+HFScentroid_2S632_value*2/3

    w1=1/3*1/HFScentroid_2S612_uncert**2
    w2=2/3*1/HFScentroid_2S632_uncert**2
    FS_centroid_uncert=np.sqrt((w1*HFScentroid_2S612_uncert/(w1+w2))**2+(w2*HFScentroid_2S632_uncert/(w1+w2))**2)

    # CODATA 2018 values from Tiesinga et al Rev Mod Phys (2021)
    c=299792458
    alpha=7.2973525693e-3  #Fine-structure constant
    lambdaC=3.8615926796e-13 #reduced Compton wavelength
    mpme= 1/5.44617021487e-4 #proton-electron mass ratio
    CNS=4*alpha**2/(3*lambdaC**2)*(1/(1+1/mpme))**3 # constant pre-factor nuclear size term, see Eq. (2.4) thesis Vitaly (note that a reduced mass factor needs to be taken into account!)

    nu1S2S=2466061413187035 # Parthey et al, PRL 107 (2011)

    def rp_uncert(nu2S6P_uncert): # Eq (2.17) thesis Vitaly
        # Approximate values of Rydberg constant and proton radius for uncertainty evaluation
        Ryd_approx=1.09737316e7 
        rp_approx=0.84e-15
        n=6
        return 1/(1-7/n**2)*3*nu2S6P_uncert/(CNS*c*Ryd_approx*rp_approx)

    def rp_value(A,B,nu1S2S,nu2S6P): # Eq. (2.16) thesis Vitaly
        return 2*np.sqrt(2/CNS)*np.sqrt((B*nu1S2S-A*nu2S6P)/(nu1S2S-7*nu2S6P))

    # See Mathematica script 'Hydrogen energy levels CODATA2018 calculating A, B_12, B_32 coefficients.nb'
    A=0.7495987478887882
    #A_uncert=8.02e-13 
    B_12=0.2221048873283651
    #B_12_uncert=1.04e-13
    rp_12=rp_value(A,B_12,nu1S2S,HFScentroid_2S612_value)
    B_32=0.2221050108175093
    #B_32_uncert=1.04e-13
    rp_32=rp_value(A,B_32,nu1S2S,HFScentroid_2S632_value)

    rp_12_uncert=rp_uncert(HFScentroid_2S612_uncert)
    rp_32_uncert=rp_uncert(HFScentroid_2S632_uncert)

    rp_avg=(rp_12+rp_32)/2
    w1=1/rp_12_uncert**2
    w2=1/rp_32_uncert**2
    rp_avg_uncert=np.sqrt((w1*rp_12_uncert/(w1+w2))**2+(w2*rp_32_uncert/(w1+w2))**2)

    text_output='2S-6P fine-structure centroid:' + '\n' + \
        compact_uncert_str(FS_centroid_value,FS_centroid_uncert,1)  + ' Hz' + '\n' + \
        'Proton radius values with only experimental uncertainty (i.e. theory uncertainty neglected):' + '\n' + \
        '...from 2S-6P12 HFS centroid:' + '\n' + \
        compact_uncert_str(rp_12*1e15,rp_12_uncert*1e15,3)  + ' fm'  + '\n' + \
        '...from 2S-6P32 HFS centroid:'  + '\n' + \
        compact_uncert_str(rp_32*1e15,rp_32_uncert*1e15,3)  + ' fm'   + '\n' + \
        '...average:' + '\n' + \
        compact_uncert_str(rp_avg*1e15,rp_avg_uncert*1e15,3)  + ' fm'
        
    print(text_output)
    with open(valuesFolder+'2S-6P FS centroid result and proton radius value.txt', "w") as text_file:
        print(text_output, file=text_file)



    

    




