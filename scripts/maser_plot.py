# -*- coding: utf-8 -*-
"""
Created Apr 2024

@author: Vitaly Wirthl
"""
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import scipy.optimize
import scripts.sim_plts_thesis as plts
plts.set_params()

from scripts.fitfunctions import lin_func
import matplotlib.gridspec as gridspec
import os
from config import *    

maser_folder=folder_data+'maser\\'
filenames=os.listdir(maser_folder) 

#modified julian day from Nov 17, 1858
mjd_start = np.datetime64('1858-11-17')

mjd_plot_offset=58556

def maser_plot():
    print('Analyzing maser data')

    dfMaser = pd.DataFrame()

    alldata_xvals=[]
    alldata_yvals=[]
    alldata_fit=[]
    alldata_res=[]
    alldata_corr_xval=[]
    alldata_corr_yval=[]

    fig = plt.figure(
    figsize=(0.8*plts.pagewidth, 4.5), constrained_layout=True)

    gs = gridspec.GridSpec(
        ncols=1, nrows=3, figure=fig,
        height_ratios=[2,1,1],
        )

    ax1 = fig.add_subplot(gs[0])
    ax2 = fig.add_subplot(gs[1],sharex=ax1)
    ax3 = fig.add_subplot(gs[2],sharex=ax1)
    
    plt.setp(ax1.get_xticklabels(), visible=False)
    plt.setp(ax2.get_xticklabels(), visible=False)

    for filename in filenames: 

        maserdata=pd.read_csv(maser_folder+filename, header = None,sep='\t')
        maserdata.columns = ["MJD","TimeDiff"]


        #Convert to UTC daytime
        maserdata['Daytime']=(maserdata['MJD']-float(filename.split('-')[0]))*24


        xvals=maserdata['Daytime']
        yvals=maserdata['TimeDiff']

        xvals_mjd=maserdata['MJD'].values

        #taking only date from around 16:00 to 24:00 on the two days where the maser was steered 
        #if filename == '58619-58619.clk' or filename == '58616-58616.clk': 
        if filename == '58619-58619.clk': 
            # xvals=xvals[int(len(xvals)/2)::]
            # yvals=yvals[int(len(yvals)/2)::]
            xvals=xvals[int(2*len(xvals)/3)::]
            xvals_mjd=xvals_mjd[int(2*len(xvals_mjd)/3)::]
            yvals=yvals[int(2*len(yvals)/3)::]

        popt, pcov = scipy.optimize.curve_fit(lin_func,xvals, yvals,(0, 1))
        redshift=482*9.807/299792458**2

        day=np.datetime64(pd.Timestamp(mjd_start) + pd.DateOffset(days=int(filename.split('-')[0])))
        daystring=day.astype('datetime64[D]').astype(str)
        print('Analyzed maser drift on '+daystring+' Delta_f/f(Cs) = {:.3f}'.format(1e13*1e-9*popt[0]/60**2-1e13*redshift)+r'e-13')

        ax1.plot(xvals/24+int(maserdata['MJD'][0])-mjd_plot_offset,yvals,linestyle='',marker='o',color='tab:blue',markersize=0.5,markeredgewidth=0,alpha=1)      
        ax1.plot(xvals/24+int(maserdata['MJD'][0])-mjd_plot_offset,lin_func(xvals,*popt),linestyle='-',alpha=0.7,color='black',linewidth=0.2)#: \n  $\Delta$f/f(UTC) = {:.2f} ns/day = {:.3f}'.format(popt[0]*24,1e13*1e-9*popt[0]/60**2)+r'$\times 10^{-13}$'+'\n $\Delta$f/f(Cs) = {:.3f}'.format(1e13*1e-9*popt[0]/60**2-1e13*redshift)+r' $\times 10^{-13}$')

        #alldata_xvals=np.concatenate((alldata_xvals,maserdata['MJD'][0]-mjd_plot_offset+xvals.values/24),axis=None)
        alldata_xvals=np.concatenate((alldata_xvals,xvals_mjd-mjd_plot_offset),axis=None)
        alldata_yvals=np.concatenate((alldata_yvals,yvals.values),axis=None)
        alldata_fit=np.concatenate((alldata_fit,lin_func(xvals,*popt).values),axis=None)
        alldata_res=np.concatenate((alldata_res,lin_func(xvals,*popt).values-yvals.values),axis=None)
        alldata_corr_xval.append(maserdata['MJD'][0]-mjd_plot_offset+0.5)
        alldata_corr_yval.append(1e13*1e-9*popt[0]/60**2-1e13*redshift)

        results = {
            'Day': day,
            'Daystring': daystring,
            'MJD': int(maserdata['MJD'][0]),
            'Filename': filename,
            'Deltaf_to_fUTC': 1e-9*popt[0]/60**2,
            'Redshift': redshift,
            'Deltaf_to_fCs': 1e-9*popt[0]/60**2-redshift,
        }

        dfMaser = pd.concat([dfMaser, pd.Series({**results}).to_frame().T],ignore_index=True)

        if plot_individual_maser_days:
            fig_ = plt.figure(
            figsize=(0.8*plts.pagewidth, 3), constrained_layout=True)

            gs_ = gridspec.GridSpec(
                ncols=1, nrows=2, figure=fig_,
                height_ratios=[2,1],
                )

            ax1_ = fig_.add_subplot(gs_[0])
            ax2_ = fig_.add_subplot(gs_[1],sharex=ax1_)
            
            plt.setp(ax1_.get_xticklabels(), visible=False)

            ax1_.plot(maserdata['Daytime'].values,maserdata['TimeDiff'].values,linestyle='',marker='o',color='tab:blue',markersize=1,markeredgewidth=0,alpha=1,label='Data')        
            ax1_.plot(xvals,lin_func(xvals,*popt),linestyle='--',alpha=0.7,color='black',linewidth=0.5,label='Linear fit: \n  $\Delta$f/f(UTC) = {:.2f} ns/day = {:.3f}'.format(popt[0]*24,1e13*1e-9*popt[0]/60**2)+r'$\times 10^{-13}$'+'\n $\Delta$f/f(Cs) = {:.3f}'.format(1e13*1e-9*popt[0]/60**2-1e13*redshift)+r' $\times 10^{-13}$')
            ax2_.plot(xvals,lin_func(xvals,*popt)-yvals,linestyle='',marker='o',color='tab:blue',markersize=1,markeredgewidth=0,alpha=1)

            
            ax1_.set_ylabel('Time difference of \n MPQ maser minus UTC (ns)',fontsize=7)

            ax2_.set_xlabel('Daytime UTC (hour)')
            ax2_.set_ylabel('Residuals (ns)',fontsize=7)


            ax1_.legend(**plts.get_leg_params(**{
                    # 'ncol': 1,
                    'loc': 'upper left',
                    'frameon': False,
                    'markerscale': 2
                    # 'bbox_to_anchor': (0.205, 0.01, 1, 0.12),
                    # 'fontsize': plot_params['FontsizeLegend'],
                    }))

            plt.suptitle(daystring+', file:'+filename)
            filename = (
                # datetime.utcnow().strftime('%Y-%m-%d') + 
                ' MaserFreq '
                + daystring +', file '+filename+'.clk')
            filepath = os.path.join(
                folder_results_maser_plot, filename)
            fig_.savefig(filepath + '.pdf', dpi=400)
            # fig.savefig(filepath + '.png', dpi=400)

            plt.close('all')


    #ax1.plot(alldata_xvals,alldata_yvals,linestyle='',marker='o',color='tab:blue',markersize=1,markeredgewidth=0,alpha=1,label='Data')        
    #ax1.plot(alldata_xvals,alldata_fit,linestyle='--',alpha=0.7,color='black',linewidth=0.1,label='Linear fits')#: \n  $\Delta$f/f(UTC) = {:.2f} ns/day = {:.3f}'.format(popt[0]*24,1e13*1e-9*popt[0]/60**2)+r'$\times 10^{-13}$'+'\n $\Delta$f/f(Cs) = {:.3f}'.format(1e13*1e-9*popt[0]/60**2-1e13*redshift)+r' $\times 10^{-13}$')
    ax2.plot(alldata_xvals,alldata_res,linestyle='',marker='o',color='tab:blue',markersize=1,markeredgewidth=0,alpha=1)

    ax3.plot(alldata_corr_xval,alldata_corr_yval,linestyle='',marker='o',color='tab:blue',markersize=1,markeredgewidth=0,alpha=1)

    ax1.set_ylabel('Time difference of \n MPQ maser minus UTC (ns)',fontsize=7)

    
    ax2.set_ylabel('Residuals (ns)',fontsize=7)

    ax3.set_ylabel(r'$\Delta$f/f(Cs) (1e-13)',fontsize=7)

    ax3.set_xlabel('Days since MJD=58556 (14.3.2019)')

    #ax2.set_ylim(-0.2,0.2)
    ax3.set_ylim(-1.8,1.8)


    # ax1.legend(**plts.get_leg_params(**{
    #         # 'ncol': 1,
    #         'loc': 'upper left',
    #         'frameon': False,
    #         'markerscale': 2
    #         # 'bbox_to_anchor': (0.205, 0.01, 1, 0.12),
    #         # 'fontsize': plot_params['FontsizeLegend'],
    #         }))

    plt.suptitle('Maserdata from 14.3-7.8.2019 during H2S-6P campaign')
    filename = ('Maserdata H2S-6P campaign Mar-Aug 2019')
    filepath = os.path.join(
        folder_results_maser, filename)
    fig.savefig(filepath + '.pdf', dpi=400)
    # fig.savefig(filepath + '.png', dpi=400)

    plt.close('all')

    filepath_results = os.path.join(folder_results_maser, MaserResultsFile)
    dfMaser.to_pickle(filepath_results)