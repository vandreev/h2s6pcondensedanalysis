# -*- coding: utf-8 -*-
"""
Created on Thu Mar 20 10:36:18 2023

@author: Vitaly Wirthl
"""
import numpy as np
import scipy.optimize
import scipy.special
import pandas as pd
import matplotlib.pyplot as plt
import os
import matplotlib.gridspec as gridspec


import scripts.sim_plts_thesis as plts
plts.set_params()

delete_points=[]   

from scripts.fitfunctions import *
from scripts.statisticsfunctions import *
from config import *


def analyze_scans_separate_FC_with_corr(current_group):

    print('************** Analyzing separately for each FC, Group {:s} ************'.format(current_group))
    
    folder_scans=folder_all_groups+current_group+'\\Exp'
      
    FitsFileName=current_group+'_VoigtFits'

    if use_comb_freq:
        if correct_for_maser:
            dfFits=pd.read_pickle(folder_results_fits_comb_freq_maser_corr+FitsFileName+'.pickle')    
            dfFits_corr_coeff=pd.read_pickle(folder_results_fits_comb_freq_maser_corr+current_group+'_corr_coeff.pickle')
        else:
            dfFits=pd.read_pickle(folder_results_fits_comb_freq+FitsFileName+'.pickle')    
            dfFits_corr_coeff=pd.read_pickle(folder_results_fits_comb_freq+current_group+'_corr_coeff.pickle')
    else:
        dfFits=pd.read_pickle(folder_results_fits_blind+FitsFileName+'.pickle')    
        dfFits_corr_coeff=pd.read_pickle(folder_results_fits_blind+current_group+'_corr_coeff.pickle')

    folder_velocities=folder_all_groups+current_group+'\\DelayVels'
    velocities_df=pd.read_csv(folder_velocities+'\\'+current_group+'_delay_vels.csv', delimiter=",")
    velocities=np.array(velocities_df['V_Mean'].values)
     
    FCs=np.unique(dfFits['FC'].values)

    dfFits_FC = pd.DataFrame()
    
    dfFits_FC_aver = pd.DataFrame()
    
    
    for iFC in range(len(FCs)): 
    
    
        current_FC=FCs[iFC]

        print('-- FC {:s}'.format(str(current_FC)))
        
        mask= (dfFits['FC']==current_FC)
        
        myscans_DIDs=np.unique(dfFits[mask]['ScanDID'].values)
        
        
        nu_a_list=[]
        nu_e_list=[]
        k_list=[]
        
        nu_a_err_list=[]
        nu_e_err_list=[]
        k_err_list=[]
        
        for iScan in range(len(myscans_DIDs)):
        
            current_scanDID=myscans_DIDs[iScan]

            #print('---- scanDID {:s}'.format(str(current_scanDID)))

            CEM_ID='Ch0'
            
            nu0_list=[]
            nu0_err_list=[]
            for DelayID in ['1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16']: 
                
                mask = (dfFits['ScanDID']== current_scanDID) & (dfFits['CEM_ID']==CEM_ID) & (dfFits['Delay']==DelayID) 
                
                if dfFits[mask].empty or dfFits[mask]['nu0_uncert'].values[0]==np.inf:
                    nu0_list.append(np.nan)
                    nu0_err_list.append(np.nan)
                
                else:
                    nu0=dfFits[mask]['nu0_corr'].values[0]
                    nu0_list.append(nu0)
                    nu0_err_list.append(dfFits[mask]['nu0_uncert'].values[0])
            
            nu0_list=np.array(nu0_list)
            nu0_err_list=np.array(nu0_err_list)
            maskNoNan= (~np.isnan(nu0_list))
            if len(nu0_list[maskNoNan]) < 4: #must have at least 3 frequencies for different delays to extrapolate
                nan_val_Ch0=True
            else:
                nan_val_Ch0=False
                ### Average over delays ##    
                avg=avg_uncorr_meas(nu0_list[maskNoNan],nu0_err_list[maskNoNan])
                nu_a_Ch0=avg['Mean']#np.mean(nu0_list)
                nu_a_err_Ch0=avg['Sigma']#np.std(nu0_list)/np.sqrt(len(nu0_list))
                
                ## Extrapolation over delays###
                xdata=velocities[maskNoNan]
                ydata=nu0_list[maskNoNan]
                ydata_err=nu0_err_list[maskNoNan]
                popt, pcov = scipy.optimize.curve_fit(lin_func,xdata,ydata,(0,0),sigma=ydata_err, absolute_sigma=True,ftol=1e-14, xtol=1e-14, gtol=1e-14,epsfcn=1e-14,maxfev=1000)
                nu_e_Ch0=popt[1]
                kappa_Ch0=popt[0]
                nu_e_err_Ch0=np.sqrt(pcov[1][1])
                kappa_err_Ch0=np.sqrt(pcov[0][0])
                 
            CEM_ID='Ch1'    
            
            nu0_list=[]
            nu0_err_list=[]
            for DelayID in ['1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16']: 
            
                mask = (dfFits['ScanDID']== current_scanDID) & (dfFits['CEM_ID']==CEM_ID) & (dfFits['Delay']==DelayID) 
                  
                if dfFits[mask].empty or dfFits[mask]['nu0_uncert'].values[0]==np.inf:
                    nu0_list.append(np.nan)
                    nu0_err_list.append(np.nan)
                
                else:
                    nu0=dfFits[mask]['nu0_corr'].values[0]
            
                    nu0_list.append(nu0)
                    nu0_err_list.append(dfFits[mask]['nu0_uncert'].values[0])
            
            nu0_list=np.array(nu0_list)
            nu0_err_list=np.array(nu0_err_list)
            maskNoNan= (~np.isnan(nu0_list))
            if len(nu0_list[maskNoNan]) < 4: #must have at least 3 frequencies for different delays to extrapolate
                nan_val_Ch1=True
            else:
                nan_val_Ch1=False
                ### Average over delays ##    
                avg=avg_uncorr_meas(nu0_list[maskNoNan],nu0_err_list[maskNoNan])
                nu_a_Ch1=avg['Mean']#np.mean(nu0_list)
                nu_a_err_Ch1=avg['Sigma']#np.std(nu0_list)/np.sqrt(len(nu0_list))
                
                ## Extrapolation over delays###
                xdata=velocities[maskNoNan]
                ydata=nu0_list[maskNoNan]
                ydata_err=nu0_err_list[maskNoNan]
                popt, pcov = scipy.optimize.curve_fit(lin_func,xdata,ydata,(0,0),sigma=ydata_err, absolute_sigma=True,ftol=1e-14, xtol=1e-14, gtol=1e-14,epsfcn=1e-14,maxfev=1000)
                nu_e_Ch1=popt[1]
                kappa_Ch1=popt[0]
                nu_e_err_Ch1=np.sqrt(pcov[1][1])
                kappa_err_Ch1=np.sqrt(pcov[0][0])
        
            if (nan_val_Ch0 or nan_val_Ch1):
                if nan_val_Ch1:
                    nu_a=nu_a_Ch0
                    nu_a_err=nu_a_err_Ch0
                    nu_e=nu_e_Ch0
                    nu_e_err=nu_e_err_Ch0
                    kappa=kappa_Ch0
                    kappa_err=kappa_err_Ch0
                if nan_val_Ch0:
                    nu_a=nu_a_Ch1
                    nu_a_err=nu_a_err_Ch1
                    nu_e=nu_e_Ch1
                    nu_e_err=nu_e_err_Ch1
                    kappa=kappa_Ch1
                    kappa_err=kappa_err_Ch1
            else:
                avg=avg_corr_meas(nu_a_Ch0,nu_a_Ch1,nu_a_err_Ch0,nu_a_err_Ch1,dfFits_corr_coeff['r_nu_a'].values[0])
                nu_a, nu_a_err=avg[0],avg[1]
                
                avg=avg_corr_meas(nu_e_Ch0,nu_e_Ch1,nu_e_err_Ch0,nu_e_err_Ch1,dfFits_corr_coeff['r_nu_e'].values[0])
                nu_e, nu_e_err=avg[0],avg[1]
                
                avg=avg_corr_meas(kappa_Ch0,kappa_Ch1,kappa_err_Ch0,kappa_err_Ch1,dfFits_corr_coeff['r_kappa'].values[0])
                kappa, kappa_err=avg[0],avg[1]
        
            nu_a_list.append(nu_a)
            nu_e_list.append(nu_e)
            k_list.append(kappa)
            
            nu_a_err_list.append(nu_a_err)
            nu_e_err_list.append(nu_e_err)
            k_err_list.append(kappa_err)
        
            results = {'Datagroup': current_group,
                       'ScanDIDwFC': current_group+'_'+str(int(current_FC))+'_'+str(int(current_scanDID)),
                        'ScanDID': int(current_scanDID),
                        'FC': int(current_FC),
                        'nu_a': nu_a,
                        'nu_a_err': nu_a_err,
                        'nu_e': nu_e,
                        'nu_e_err': nu_e_err,
                        'kappa': kappa,
                        'kappa_err': kappa_err,
                        }
            dfFits_FC = pd.concat([dfFits_FC,pd.Series({**results}).to_frame().T],ignore_index=True)#.convert_dtypes(convert_string=False)
        
        
        nu_a_list=np.array(nu_a_list)
        nu_e_list=np.array(nu_e_list)
        k_list=np.array(k_list)
        
        nu_a_err_list=np.array(nu_a_err_list)
        nu_e_err_list=np.array(nu_e_err_list)
        k_err_list=np.array(k_err_list)
        
        
        avg_nu_a=avg_uncorr_meas(nu_a_list,nu_a_err_list)
        avg_nu_e=avg_uncorr_meas(nu_e_list,nu_e_err_list)
        avg_kappa=avg_uncorr_meas(k_list,k_err_list)
        
        
        
        results = {'Datagroup': current_group,
                 'FC': int(current_FC),
                    'nu_a': avg_nu_a['Mean'],
                    'nu_a_err': avg_nu_a['Sigma'],
                    'nu_a_redChi2': avg_nu_a['RedChiSq'],
                    'nu_a_err_scaled': np.sqrt(avg_nu_a['RedChiSq'])*avg_nu_a['Sigma'] if avg_nu_a['RedChiSq'] > 1 else  avg_nu_a['Sigma'],
                    'nu_e': avg_nu_e['Mean'],
                    'nu_e_err': avg_nu_e['Sigma'],
                    'nu_e_redChi2': avg_nu_e['RedChiSq'],
                    'nu_e_err_scaled': np.sqrt(avg_nu_e['RedChiSq'])*avg_nu_e['Sigma'] if avg_nu_e['RedChiSq'] > 1 else  avg_nu_e['Sigma'],
                    'kappa': avg_kappa['Mean'],
                    'kappa_err': avg_kappa['Sigma'],
                    'kappa_redChi2': avg_kappa['RedChiSq'],
                    'kappa_err_scaled': np.sqrt(avg_kappa['RedChiSq'])*avg_kappa['Sigma'] if avg_kappa['RedChiSq'] > 1 else  avg_kappa['Sigma'],
                    }
        
        
        dfFits_FC_aver = pd.concat([dfFits_FC_aver,pd.Series({**results}).to_frame().T],ignore_index=True)#.convert_dtypes(convert_string=False)
    
    
    if use_comb_freq:
        if correct_for_maser:
            dfFits_FC.to_pickle(folder_results_fits_comb_freq_maser_corr+current_group+'_VoigtFits'+'_IndividualFC.pickle')
            dfFits_FC_aver.to_pickle(folder_results_fits_comb_freq_maser_corr+current_group+'_VoigtFits'+'_FCaver.pickle')
        else:
            dfFits_FC.to_pickle(folder_results_fits_comb_freq+current_group+'_VoigtFits'+'_IndividualFC.pickle')
            dfFits_FC_aver.to_pickle(folder_results_fits_comb_freq+current_group+'_VoigtFits'+'_FCaver.pickle')
    else:
        dfFits_FC.to_pickle(folder_results_fits_blind+current_group+'_VoigtFits'+'_IndividualFC.pickle')
        dfFits_FC_aver.to_pickle(folder_results_fits_blind+current_group+'_VoigtFits'+'_FCaver.pickle')
    
            