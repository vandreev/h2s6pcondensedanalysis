# -*- coding: utf-8 -*-
"""
Created Apr 2024

@author: Vitaly Wirthl
"""
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import scipy.optimize
import scripts.sim_plts_thesis as plts
import matplotlib.gridspec as gridspec
import os
from scripts.fitfunctions import lin_func
from config import *

def plot_FP3laserFreq_whole_campaign(start,end,title):
    dfFits=pd.read_pickle(folder_results_comb+CombFile)
    freq_offset= 365344707350e3

    fig = plt.figure(
            figsize=(0.8*plts.pagewidth, 4), constrained_layout=True)

    gs = gridspec.GridSpec(
        ncols=1, nrows=1, figure=fig,
        # width_ratios=[1],
        )

    ax1 = fig.add_subplot(gs[0])

    mask= (dfFits[~pd.isnull(dfFits['FP3freq_from40th'])]['TimestampFirstPoint'] > np.datetime64(start)) & (dfFits[~pd.isnull(dfFits['FP3freq_from40th'])]['TimestampFirstPoint'] < np.datetime64(end))

    xvals=dfFits[~pd.isnull(dfFits['FP3freq_from40th'])][mask]['TimestampFirstPoint'].values
    yvals=1e-3*(dfFits[~pd.isnull(dfFits['FP3freq_from40th'])][mask]['FP3freq_from40th']-freq_offset).values.astype('float')

    ax1.errorbar(xvals,yvals,yerr=1e-3*dfFits[~pd.isnull(dfFits['FP3freq_from40th'])][mask]['FP3freq_err_40th'],linestyle='',marker='o',color='tab:blue',markersize=2.5,markeredgewidth=0)

    start=xvals[0]
    timediff_vals=np.array([pd.Timedelta(np.timedelta64( xval-start ,'s')).total_seconds() for xval in xvals])
    popt, pcov = scipy.optimize.curve_fit(lin_func,timediff_vals, yvals.astype('float'),(0, 1))

    ax1.plot(xvals,lin_func(timediff_vals,*popt),linestyle='--',linewidth=1,alpha=0.6,marker='',color='tab:red',label='Fit: {:.3f} kHz/day'.format(popt[0]*60*60*24))


    fig.suptitle(title)

    ax1.set_xlabel('Time')

    ax1.set_ylabel('FP3 laser freq. - {:.0f} kHz (kHz)'.format(1e-3*freq_offset))

    import matplotlib.dates as mdates
    xfmt = mdates.DateFormatter('%Y-%m-%d')

    ax1.xaxis.set_major_formatter(xfmt)
    for tick in ax1.get_xticklabels():
        tick.set_rotation(45)

    ax1.legend()


#    from datetime import datetime
    plotFile=title
    filename = (#datetime.utcnow().strftime('%Y-%m-%d') + ' ' +
        plotFile)
    filepath = os.path.join(
        folder_results_comb, filename)
    fig.savefig(filepath + '.pdf', dpi=400)
    # fig.savefig(filepath + '.png', dpi=400)

    plt.close()



