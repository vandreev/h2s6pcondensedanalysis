# -*- coding: utf-8 -*-
"""
Created Apr 2024

@author: Vitaly Wirthl
"""
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import scripts.sim_plts_thesis as plts
import matplotlib.gridspec as gridspec
import os
from config import *

def plot_FP3laserFreq_EachMeasDay():

    dfFits=pd.read_pickle(folder_results_comb+CombFile)

    measdates=np.unique(dfFits['Measdate'].values)

    freq_offset= 365344707350e3


    for measdate in measdates:
        print('Plotting FP3 laser frequency for date '+measdate.astype('datetime64[D]').astype(str))

        mask_measdate= (dfFits['Measdate'] == measdate) & (~pd.isnull(dfFits['FP3freq_from40th']))

        
        fig = plt.figure(
            figsize=(plts.pagewidth, 5), constrained_layout=True)

        gs = gridspec.GridSpec(
            ncols=1, nrows=1, figure=fig,
            # width_ratios=[1],
            )

        ax1 = fig.add_subplot(gs[0])

        #ax1.errorbar(dfFits[mask_measdate]['Daytime']/60/60,1e-3*dfFits[mask_measdate]['FP3freq'],yerr=1e-3*dfFits[mask_measdate]['FP3freq_err'],linestyle='',marker='o',color='tab:blue',markersize=3,markeredgewidth=0)
        ax1.errorbar(dfFits[mask_measdate]['Daytime']/60/60,1e-3*(dfFits[mask_measdate]['FP3freq_from40th']-freq_offset),yerr=1e-3*dfFits[mask_measdate]['FP3freq_err_40th'],linestyle='',marker='o',color='tab:blue',markersize=3,markeredgewidth=0)
        
        ax1.set_rasterized(True)

        year_month_day_string=measdate.astype('datetime64[D]').astype(str).replace('-','_')
        if max(dfFits[mask_measdate]['Daytime']) > 24*60*60:
            year_month_day_string2=year_month_day_string.split('_')[0]+'_'+year_month_day_string.split('_')[1]+'_'+str(int(year_month_day_string.split('_')[-1])+1).zfill(2)
            print('Scans after midnight, i.e. extending to next day '+year_month_day_string2)
            plot_title=year_month_day_string+'-'+year_month_day_string2
        else:
            plot_title=year_month_day_string

        fig.suptitle('FP3 laser frequency for precision scans on day '+plot_title)

        ax1.set_xlabel('Daytime UTC (hours)')

        ax1.set_ylabel('FP3 laser freq. - {:.0f} kHz (kHz)'.format(1e-3*freq_offset))

        #from datetime import datetime
        plotFile='FP3LaserFreq '+plot_title#+str(measdates[0]).split('T')[0]
        filename = (
            # datetime.utcnow().strftime('%Y-%m-%d %H-%M-%S') + ' '
            #datetime.utcnow().strftime('%Y-%m-%d') + ' ' +
            plotFile)
        filepath = os.path.join(
            folder_results_comb_plot, filename)
        fig.savefig(filepath + '.pdf', dpi=400)
        #fig.savefig(filepath + '.png', dpi=400)

        plt.close()

