# -*- coding: utf-8 -*-
"""
Created Apr 2024

@author: Vitaly Wirthl
"""
import numpy as np
import pandas as pd
import os
from config import *

def compute_laserfreq_eachscan():
    folder_combdata=folder_data+'comb\\'
    scans_metatable=pd.read_excel(folder_data+'2020-11-19_2S-6P 2019 measurement_scansmeta.xlsx')

    measdates=np.unique(scans_metatable['MeasDate'].values)

    filter_data=True

    scanUIDs_outliers=[]
    dfCombData = pd.DataFrame()
    for measdate in measdates:
        print('Analyzing comb freq. of meas. date '+measdate.astype('datetime64[D]').astype(str))

        scanUIDs=scans_metatable[scans_metatable['MeasDate'] == measdate]['ScanUID'].values

        scan_time_start_daysec_list=[]
        scan_time_end_daysec_list=[]

        for scanUID in scanUIDs:

            mask= (scans_metatable['ScanUID'] == scanUID )

            scan_time_start=scans_metatable[mask]['TimestampFirstPoint'].values[0]
            scan_time_end=scans_metatable[mask]['TimestampLastPoint'].values[0]

            day_start=measdate
            scan_time_start_daysec=pd.Timedelta(np.timedelta64( scan_time_start-day_start ,'s')).total_seconds() 
            scan_time_end_daysec=pd.Timedelta(np.timedelta64( scan_time_end-day_start ,'s')).total_seconds() 

            scan_time_start_daysec_list.append(scan_time_start_daysec)
            scan_time_end_daysec_list.append(scan_time_end_daysec)

        # Counter data channels:
        # Ch5: ULN frep 40th harmonic - 9.980121089 GHz
        # Ch6: ULN frep 4th harmonic - 980 MHz
        # Ch11: FP3 @ 820 nm, ~ 47 MHz (since 5.12.18 for 2S-6P, before in Sr configuration for 1S-2S at 21 MHz)
        # Ch12: FP3 redundancy

        year_month_day_string=measdate.astype('datetime64[D]').astype(str).replace('-','_')

        filename='KK_'+year_month_day_string+'__09_00_00.pickle'
        kkdata=pd.read_pickle(folder_combdata+'/'+filename)

        nextdaymask= (kkdata['Daytime'] < 9*60*60) 
        kkdata.loc[nextdaymask,'Daytime']=kkdata[nextdaymask]['Daytime']+24*60*60
        kkdata["ScanUID"] = " "
        kkdata["ScanUID_Dual"] = " "

        timemask_list=[]
        for i in range(len(scan_time_start_daysec_list)):
            if (i == 0) or (scan_time_start_daysec_list[i] > scan_time_end_daysec_list[i-1]):
                timemask= (kkdata['Daytime'] > scan_time_start_daysec_list[i])  &   (kkdata['Daytime'] < scan_time_end_daysec_list[i]) 
                timemask_list.append(timemask)
                kkdata.loc[timemask,'ScanUID']=scanUIDs[i]
            else:
                timemask= (kkdata['Daytime'] > scan_time_start_daysec_list[i])  &   (kkdata['Daytime'] < scan_time_end_daysec_list[i]) 
                timemask_list.append(timemask)
                kkdata.loc[timemask,'ScanUID_Dual']=scanUIDs[i]


        timemask_combined=timemask_list[0]
        for i in range(len(scan_time_start_daysec_list)-1):
            timemask_combined=np.ma.mask_or(timemask_combined,timemask_list[i+1])

        kkdata.loc[kkdata['Daytime'] < 32400, 'Daytime'] = kkdata[kkdata['Daytime'] < 32400]['Daytime'] + 24*60*60


        for scanUID in scanUIDs:
            outliers=False
            maskScanUID= (kkdata['ScanUID']==scanUID) 
            maskScanUID_Dual = (kkdata['ScanUID_Dual']==scanUID)
            if len(kkdata[maskScanUID_Dual])==0:
                timemask_combined=maskScanUID
            else: 
                timemask_combined=maskScanUID_Dual

            ########### Rep. Rate Freq. #######
            y_offset=20.0e6
            if filter_data:
                mask=((kkdata[timemask_combined]['Ch6']-y_offset) > 2) | ((kkdata[timemask_combined]['Ch6']-y_offset) < 0.5)
                mask=np.abs(kkdata[timemask_combined]['Ch6']-np.mean(kkdata[timemask_combined][~mask]['Ch6'])) > 0.01
                if len(kkdata[timemask_combined][mask]['ScanUID'].values) >0:
                    scanUIDs_outliers.extend(np.unique(kkdata[timemask_combined][mask]['ScanUID'].values))
                    outliers=True
            else:
                mask= kkdata[timemask_combined]['Daytime'] <0
         

            frep=(980e6+np.mean(kkdata[timemask_combined][~mask]['Ch6']))/4
            frep_err=np.std(kkdata[timemask_combined][~mask]['Ch6'])/np.sqrt(len(kkdata[timemask_combined][~mask]['Ch6']))/4

            frep_from40thHarm=(np.mean(kkdata[timemask_combined][~mask]['Ch5'])+ 9980121089)/40
            frep_err_40th=np.std(kkdata[timemask_combined][~mask]['Ch5'])/np.sqrt(len(kkdata[timemask_combined][~mask]['Ch5']))/40

                  
            ############# FP3 beat freq. ##############

            y_offset=47.079e6
            if filter_data:
                mask=np.abs(kkdata[timemask_combined]['Ch11']-y_offset) > 0.05
                if len(kkdata[timemask_combined][mask]['ScanUID'].values) >0:
                    scanUIDs_outliers.extend(np.unique(kkdata[timemask_combined][mask]['ScanUID'].values))
                    outliers=True
            else:
                mask= kkdata[timemask_combined]['Daytime'] <0

            FP3beat=np.mean(kkdata[timemask_combined][~mask]['Ch11'])

            maskScanUID_Metadata=scans_metatable['ScanUID'] == scanUID 

            fCEO=-45e6
            nFP3=1461379
            FP3beat_setvalue=47.079e6
            nuFP3=2*fCEO+nFP3*frep+FP3beat_setvalue
            nuFP3_measFP3beat=2*fCEO+nFP3*frep+FP3beat
            nuFP3_from40th=2*fCEO+nFP3*frep_from40thHarm+FP3beat_setvalue

            if len(kkdata[timemask_combined])==0:
                daytime=np.nan
                daytime_max=np.nan
                daytime_min=np.nan
                print('No usable comb data for scan: '+str(scanUID))
            else:
                daytime_min=min(kkdata[timemask_combined]['Daytime'])
                daytime_max=max(kkdata[timemask_combined]['Daytime'])
                daytime=daytime_min+(daytime_max-daytime_min)/2

            results = {
                'ScanUID': scanUID,
                'ScanDID': scans_metatable[maskScanUID_Metadata]['ScanDID'].values[0],
                'Measdate': scans_metatable[maskScanUID_Metadata]['MeasDate'].values[0],
                'Daytime': daytime,
                'DaytimeMin': daytime_min,
                'DaytimeMax': daytime_max,
                'TimestampFirstPoint': scans_metatable[maskScanUID_Metadata]['TimestampFirstPoint'].values[0],
                'TimestampLastPoint': scans_metatable[maskScanUID_Metadata]['TimestampLastPoint'].values[0],
                'RRE': frep,
                'RRE_from40th': frep_from40thHarm,
                'FP3beat': FP3beat,
                'FP3freq': nuFP3,
                'FP3freq_err': nFP3*frep_err,
                'FP3freq_err_40th': nFP3*frep_err_40th,
                'FP3freq_measFP3beat': nuFP3_measFP3beat,
                'FP3freq_from40th': nuFP3_from40th,
                'Npoints': len(kkdata[timemask_combined]['Ch6'].values),
                'Outliers_FP3orRREbeats': outliers
            }

            dfCombData = pd.concat([dfCombData ,pd.Series({**results}).to_frame().T],ignore_index=True)


    filepath = os.path.join(
        folder_results_comb, CombFile)

    dfCombData.to_pickle(filepath)