# -*- coding: utf-8 -*-
"""
Created on Thu Mar 20 10:36:18 2023

@author: Vitaly Wirthl
"""
import numpy as np
import scipy.special
import pandas as pd
import matplotlib.pyplot as plt
import os
import matplotlib.gridspec as gridspec
from scipy.stats import norm

import scripts.sim_plts_thesis as plts
plts.set_params()

from scripts.fitfunctions import *
from scripts.statisticsfunctions import *
from config import *

def plot_analysis_scans_FCaver(current_group):
    print('*********** Plotting FC average analysis Group {:s} ***********'.format(current_group))

    folder_scans=folder_all_groups+current_group+'\\Exp'
    
    FitsFileName=current_group+'_VoigtFits'

    if use_comb_freq:
        if correct_for_maser:
            dfFits_FC=pd.read_pickle(folder_results_fits_comb_freq_maser_corr+FitsFileName+'_IndividualFC.pickle').convert_dtypes()
            dfFits_FC_aver=pd.read_pickle(folder_results_fits_comb_freq_maser_corr+FitsFileName+'_FCaver.pickle').convert_dtypes()
        else:
            dfFits_FC=pd.read_pickle(folder_results_fits_comb_freq+FitsFileName+'_IndividualFC.pickle').convert_dtypes()
            dfFits_FC_aver=pd.read_pickle(folder_results_fits_comb_freq+FitsFileName+'_FCaver.pickle').convert_dtypes()
    else:
        dfFits_FC=pd.read_pickle(folder_results_fits_blind+FitsFileName+'_IndividualFC.pickle').convert_dtypes()
        dfFits_FC_aver=pd.read_pickle(folder_results_fits_blind+FitsFileName+'_FCaver.pickle').convert_dtypes()
    
    
    plot_params = {
        'FontsizeFitResults': plts.params['FontsizeReg'],
        'FontsizeLegend': 7,
        }
    plot_params['Data'] = {
        'linestyle': '',
        'marker': 'o',
        'elinewidth': 0.2,
        'linewidth': 0.5,
        'markeredgewidth': 0.6,
        'capsize': 0.8,
        'markersize': 0.5}
    
    figure_height=5
    myfig = plt.figure(
        figsize=(1.3*plts.pagewidth, figure_height), constrained_layout=True)
    myfig.clf()
    widths = [8,2,8]
    heights = [1,1,1]
    
    # Create main layout splitting figure:
    gs_main = gridspec.GridSpec(
        ncols=3, nrows=3, figure=myfig,
        height_ratios=heights, width_ratios=widths
        )
    
    #Create axes
    ax_nu_a = myfig.add_subplot(gs_main[0])
    ax_nu_e = myfig.add_subplot(gs_main[3],sharex=ax_nu_a)
    ax_kappa = myfig.add_subplot(gs_main[6],sharex=ax_nu_a)
    #Create axes
    ax_nu_a_hist = myfig.add_subplot(gs_main[1])
    ax_nu_a_FCaver = myfig.add_subplot(gs_main[2])
    
    ax_nu_e_hist = myfig.add_subplot(gs_main[4])
    ax_nu_e_FCaver = myfig.add_subplot(gs_main[5])
    
    ax_kappa_hist = myfig.add_subplot(gs_main[7])
    ax_kappa_FCaver = myfig.add_subplot(gs_main[8])
    
    # Dont show tick labels on shared axes 
    plt.setp(ax_nu_a.get_xticklabels(), visible=False)
    plt.setp(ax_nu_e.get_xticklabels(), visible=False)
    
    plt.setp(ax_nu_a_FCaver.get_xticklabels(), visible=False)
    plt.setp(ax_nu_e_FCaver.get_xticklabels(), visible=False)
    

    
    nu_a_list=dfFits_FC_aver['nu_a'].values
    nu_a_err_list=dfFits_FC_aver['nu_a_err'].values
    nu_a_err_scaled_list=dfFits_FC_aver['nu_a_err_scaled'].values
    
    
    nu_e_list=dfFits_FC_aver['nu_e'].values
    nu_e_err_list=dfFits_FC_aver['nu_e_err'].values
    nu_e_err_scaled_list=dfFits_FC_aver['nu_e_err_scaled'].values
    
    kappa_list=dfFits_FC_aver['kappa'].values
    kappa_err_list=dfFits_FC_aver['kappa_err'].values
    kappa_err_scaled_list=dfFits_FC_aver['kappa_err_scaled'].values
    
    
    no_FCs=len(nu_a_list)
    i_scans=np.arange(1,len(nu_a_list)+1,1)
    xvals=np.arange(0.2,len(nu_a_list)+1,0.5)
    
    ax_nu_a_FCaver.errorbar(i_scans+0.2,nu_a_list,yerr=nu_a_err_scaled_list,color='tab:blue',**plot_params['Data'],label=r'FC avg. scaled')
    ax_nu_a_FCaver.errorbar(i_scans,nu_a_list,yerr=nu_a_err_list,color='tab:gray',**plot_params['Data'],label=r'FC avg.')
    
    avg=avg_uncorr_meas(nu_a_list,nu_a_err_scaled_list)
    ax_nu_a_FCaver.plot(xvals,np.full(len(xvals), avg['Mean']),linestyle='-',linewidth=0.7,color='tab:blue',alpha=0.5,label=r'Avg.: '
                     +compact_uncert_str(1e-3*avg['Mean'],1e-3*avg['Sigma'],3)+' kHz'
                      +', $\chi_\mathrm{red}^2$='+compact_uncert_str(avg['RedChiSq'],avg['RedChiSqSigma'],2))
                     
    
    
    ax_nu_e_FCaver.errorbar(i_scans+0.2,nu_e_list,yerr=nu_e_err_scaled_list,color='tab:blue',**plot_params['Data'],label=r'FC avg. scaled')
    ax_nu_e_FCaver.errorbar(i_scans,nu_e_list,yerr=nu_e_err_list,color='tab:gray',**plot_params['Data'],label=r'FC avg.')
    
    avg=avg_uncorr_meas(nu_e_list,nu_e_err_scaled_list)
    ax_nu_e_FCaver.plot(xvals,np.full(len(xvals), avg['Mean']),linestyle='-',linewidth=0.7,color='tab:blue',alpha=0.5,label=r'Avg.: '
                     +compact_uncert_str(1e-3*avg['Mean'],1e-3*avg['Sigma'],3)+' kHz'
                      +', $\chi_\mathrm{red}^2$='+compact_uncert_str(avg['RedChiSq'],avg['RedChiSqSigma'],2))
          
    ax_kappa_FCaver.errorbar(i_scans+0.2,kappa_list,yerr=kappa_err_scaled_list,color='tab:blue',**plot_params['Data'],label=r'FC avg. scaled')
    ax_kappa_FCaver.errorbar(i_scans,kappa_list,yerr=kappa_err_list,color='tab:gray',**plot_params['Data'],label=r'FC avg.')
    
    avg=avg_uncorr_meas(kappa_list,kappa_err_scaled_list)
    ax_kappa_FCaver.plot(xvals,np.full(len(xvals), avg['Mean']),linestyle='-',linewidth=0.7,color='tab:blue',alpha=0.5,label=r'Avg.: '
                      +compact_uncert_str(avg['Mean'],avg['Sigma'],3)+' Hz/(m/s)'
                      +', $\chi_\mathrm{red}^2$='+compact_uncert_str(avg['RedChiSq'],avg['RedChiSqSigma'],2))

    
    nu_a_list=dfFits_FC['nu_a'].values
    nu_a_err_list=dfFits_FC['nu_a_err'].values
    
    
    nu_e_list=dfFits_FC['nu_e'].values
    nu_e_err_list=dfFits_FC['nu_e_err'].values
    
    k_list=dfFits_FC['kappa'].values
    k_err_list=dfFits_FC['kappa_err'].values
    
     
    i_scans=np.arange(1,len(nu_a_list)+1,1)
    xvals=np.arange(-10,len(nu_a_list)+10,0.5)
    
    avg=avg_uncorr_meas(nu_a_list,nu_a_err_list)
    ax_nu_a.errorbar(i_scans,nu_a_list,yerr=nu_a_err_list,color='tab:gray',**plot_params['Data'],label=r'Corr. det. avg.')
    ax_nu_a.plot(xvals,np.full(len(xvals), avg['Mean']),linestyle='-',linewidth=0.7,color='gray',alpha=0.5,label=r'Avg.: '
                     +compact_uncert_str(1e-3*avg['Mean'],1e-3*avg['Sigma'],3)+' kHz'
                      +', $\chi_\mathrm{red}^2$='+compact_uncert_str(avg['RedChiSq'],avg['RedChiSqSigma'],2))
    
    ylim_nu_a=[-55e3,105e3]
    ax_nu_a_FCaver.set_ylim((ylim_nu_a-avg['Mean'])/np.sqrt(no_FCs)+avg['Mean'])
    ax_nu_a_hist.set_ylim(ylim_nu_a)
                     
    
    avg=avg_uncorr_meas(nu_e_list,nu_e_err_list)
    ax_nu_e.errorbar(i_scans-0.5,nu_e_list,yerr=nu_e_err_list,color='tab:gray',**plot_params['Data'],label=r'Corr. det. avg.')         
    ax_nu_e.plot(xvals,np.full(len(xvals), avg['Mean']),linestyle='-',linewidth=0.7,color='gray',alpha=0.5,label=r'Avg.:'
                      +compact_uncert_str(avg['Mean']/1e3,avg['Sigma']/1e3,3)+' kHz'
                      +', $\chi_\mathrm{red}^2$='+compact_uncert_str(avg['RedChiSq'],avg['RedChiSqSigma'],2))
    

    ylim_nu_e=[-115e3,160e3]
    ax_nu_e.set_ylim(ylim_nu_e)
    ax_nu_e_FCaver.set_ylim((ylim_nu_e-avg['Mean'])/np.sqrt(no_FCs)+avg['Mean'])
    ax_nu_e_hist.set_ylim(ylim_nu_e)

    avg=avg_uncorr_meas(k_list,k_err_list)
    ax_kappa.errorbar(i_scans-0.5,k_list,yerr=k_err_list,color='tab:gray',**plot_params['Data'],label=r'Corr. det. avg.')        
    
    
    ax_kappa.plot(xvals,np.full(len(xvals), avg['Mean']),linestyle='-',linewidth=0.7,color='gray',alpha=0.5,label=r'Avg.:'
                  +compact_uncert_str(avg['Mean'],avg['Sigma'],3)+' Hz/(m/s)'
                      +', $\chi_\mathrm{red}^2$='+compact_uncert_str(avg['RedChiSq'],avg['RedChiSqSigma'],2))
    
    
    ylim_kappa=[-640,699]
    
    ax_kappa.set_ylim(ylim_kappa)
    ax_kappa_FCaver.set_ylim((ylim_kappa-avg['Mean'])/np.sqrt(no_FCs)+avg['Mean'])
    ax_kappa_hist.set_ylim(ylim_kappa)
    
    
    no_hist_bins=30
    
    
    n, bins, patches = ax_nu_a_hist.hist(nu_a_list,no_hist_bins,facecolor='tab:gray', density=True,alpha=0.7,edgecolor='k', linewidth=0.4,orientation=u'horizontal')
    (mu, sigma) = norm.fit(nu_a_list)
    y = norm.pdf( bins, mu, sigma)
    l = ax_nu_a_hist.plot(y,bins,  '-', color='tab:gray',alpha=1,linewidth=1,label='Det. avg.')#'\n Gauss fit')
    ax_nu_a_hist.annotate('norm. distr. fit:\n'+'$\,\,\,\,\mu=$'
                                +str(np.round(1e-3*mu,2))+' kHz \n'
                                +'$\,\,\,\,\sigma=$'+str(np.round(1e-3*sigma,2))+' kHz',(0.15,0.64),xycoords='axes fraction',fontsize=7,fontstretch='condensed')
    
    
    ax_nu_a.set_ylim(ylim_nu_a)
    
    
    
    legend=ax_nu_a_hist.legend(#[handles[idx] for idx in order],[labels[idx] for idx in order],#bbox_to_anchor=(1.1, 1.05),
            **plts.get_leg_params(**{
                  'loc': 'upper right',
                  'ncol': 1,
                  'frameon': False,
                'fontsize': plot_params['FontsizeLegend']}))
    
    
    
    n, bins, patches = ax_nu_e_hist.hist(nu_e_list,no_hist_bins,facecolor='tab:gray', density=True,alpha=0.7,edgecolor='k', linewidth=0.4,orientation=u'horizontal')
    (mu, sigma) = norm.fit(nu_e_list)
    y = norm.pdf( bins, mu, sigma)
    l = ax_nu_e_hist.plot(y,bins,  '-', color='tab:gray',alpha=1,linewidth=1,label='Det. avg.')#'\n Gauss fit')
    ax_nu_e_hist.annotate('norm. distr. fit:\n'+'$\,\,\,\,\mu=$'
                                +str(np.round(1e-3*mu,2))+' kHz \n'
                                +'$\,\,\,\,\sigma=$'+str(np.round(1e-3*sigma,2))+' kHz',(0.15,0.64),xycoords='axes fraction',fontsize=7,fontstretch='condensed')
    
    
    
    legend=ax_nu_e_hist.legend(#[handles[idx] for idx in order],[labels[idx] for idx in order],#bbox_to_anchor=(1.1, 1.05),
            **plts.get_leg_params(**{
                  'loc': 'upper right',
                  'ncol': 1,
                  'frameon': False,
                'fontsize': plot_params['FontsizeLegend']}))
    
    n, bins, patches = ax_kappa_hist.hist(k_list,no_hist_bins,facecolor='tab:gray', density=True,alpha=0.7,edgecolor='k', linewidth=0.4,orientation=u'horizontal')
    (mu, sigma) = norm.fit(k_list)
    y = norm.pdf( bins, mu, sigma)
    l = ax_kappa_hist.plot(y,bins,  '-', color='tab:gray',alpha=1,linewidth=1,label='Det. avg.')#'\n Gauss fit')
    ax_kappa_hist.annotate('norm. distr. fit:\n'+'$\,\,\,\,\,\,\,\,\mu=$'
                                +str(np.round(mu,1))+' Hz/(m/s) \n'
                                +'$\,\,\,\,\,\,\,\,\sigma=$'+str(int(np.round(sigma,0)))+' Hz/(m/s)',(0.05,0.64),xycoords='axes fraction',fontsize=7,fontstretch='condensed')

    
    legend=ax_kappa_hist.legend(#[handles[idx] for idx in order],[labels[idx] for idx in order],#bbox_to_anchor=(1.1, 1.05),
            **plts.get_leg_params(**{
                  'loc': 'upper right',
                  'ncol': 1,
                  'frameon': False,
                'fontsize': plot_params['FontsizeLegend']}))
    
    
    ax_nu_a_hist.tick_params(axis='both',which='both',bottom=False,top=False,labelbottom=False,left=False,labelleft=False)
    
    ax_nu_e_hist.tick_params(axis='both',which='both',bottom=False,top=False,labelbottom=False,left=False,labelleft=False)
    
    ax_kappa_hist.tick_params(axis='both',which='both',bottom=False,top=False,labelbottom=False,left=False,labelleft=False)
    
    
    import matplotlib.ticker as ticker
    #Convert y axis :
    ticks = ticker.FuncFormatter(lambda y, pos: '${:.0f}$'.format( y*1e-3))
    ax_nu_a.yaxis.set_major_formatter(ticks)
    ax_nu_a_FCaver.yaxis.set_major_formatter(ticks)
    
    #Convert y axis :
    ticks = ticker.FuncFormatter(lambda y, pos: '${:.0f}$'.format( y*1e-3))
    ax_nu_e.yaxis.set_major_formatter(ticks)
    ax_nu_e_FCaver.yaxis.set_major_formatter(ticks)
    
    
    legend=ax_nu_a.legend(#[handles[idx] for idx in order],[labels[idx] for idx in order],#bbox_to_anchor=(1.1, 1.05),
            **plts.get_leg_params(**{
                  'loc': 'upper left',
                  'ncol': 3,
                  'frameon': True,
                'fontsize': plot_params['FontsizeLegend']}))
    frame = legend.get_frame()
    frame.set_edgecolor('gray')
    frame.set_linewidth(0.5)
    

    legend=ax_nu_e.legend(#[handles[idx] for idx in order],[labels[idx] for idx in order],#bbox_to_anchor=(1.1, 1.05),
            **plts.get_leg_params(**{
                  'loc': 'upper left',
                  'ncol': 3,
                  'frameon': True,
                'fontsize': plot_params['FontsizeLegend']}))
    frame = legend.get_frame()
    frame.set_edgecolor('gray')
    frame.set_linewidth(0.5)
    

    legend=ax_kappa.legend(#[handles[idx] for idx in order],[labels[idx] for idx in order],#bbox_to_anchor=(1.1, 1.05),
            **plts.get_leg_params(**{
                  'loc': 'upper left',
                  'ncol': 3,
                  'frameon': True,
                'fontsize': plot_params['FontsizeLegend']}))
    frame = legend.get_frame()
    frame.set_edgecolor('gray')
    frame.set_linewidth(0.5)
    
    ax_nu_a.set_ylabel(r'$\nu_\mathrm{0,a}$ (kHz)')
    ax_nu_e.set_ylabel(r'$\nu_\mathrm{0,e}$ (kHz)')
    ax_kappa.set_ylabel(r'$\kappa$ (Hz/(m/s))')
    ax_kappa.set_xlabel(r'Resonance line scan number')
    
    ax_kappa_FCaver.set_xlabel(r'Freezing cycle (FC) number')

    legend=ax_nu_a_FCaver.legend( **plts.get_leg_params(**{
                  'loc': 'upper left',
                  'ncol': 3,
                  'frameon': True,
                'fontsize': plot_params['FontsizeLegend']}))
    frame = legend.get_frame()
    frame.set_edgecolor('gray')
    frame.set_linewidth(0.5)
    
    
    legend=ax_nu_e_FCaver.legend( **plts.get_leg_params(**{
                  'loc': 'upper left',
                  'ncol': 3,
                  'frameon': True,
                'fontsize': plot_params['FontsizeLegend']}))
    frame = legend.get_frame()
    frame.set_edgecolor('gray')
    frame.set_linewidth(0.5)
    
    legend=ax_kappa_FCaver.legend( **plts.get_leg_params(**{
                  'loc': 'upper left',
                  'ncol': 3,
                  'frameon': True,
                'fontsize': plot_params['FontsizeLegend']}))
    frame = legend.get_frame()
    frame.set_edgecolor('gray')
    frame.set_linewidth(0.5)
    
    ax_nu_a.set_xlim([-10,len(k_list)+10])
    ax_nu_e.set_xlim([-10,len(k_list)+10])
    ax_kappa.set_xlim([-10,len(k_list)+10])
    
    suptitle='Data group: '+current_group+', Freezing Cycle (FC) averages'
    if use_comb_freq:
        if current_group in FS6P32groups:
            suptitle=suptitle+'\n'+r'Frequency values for $\nu_\mathrm{0,a}$ and $\nu_\mathrm{0,e}$ are shown with an offset of '+u"\u2212"+str(nu2s6p_offset_32)+' Hz'
        else:
            suptitle=suptitle+'\n'+r'Frequency values for $\nu_\mathrm{0,a}$ and $\nu_\mathrm{0,e}$ are shown with an offset of '+u"\u2212"+str(nu2s6p_offset_12)+' Hz'
    myfig.suptitle(suptitle)
    
    plotFile='FCsAnalysis_'+FitsFileName
          
    if use_comb_freq:
        if correct_for_maser:
            plotFolder = folder_plots_comb_maser_corr+current_group
            if not os.path.exists(plotFolder):
                os.makedirs(plotFolder)
        else:
            plotFolder = folder_plots_comb+current_group
            if not os.path.exists(plotFolder):
                os.makedirs(plotFolder)
    else:
        plotFolder = folder_plots_blind+current_group
        if not os.path.exists(plotFolder):
            os.makedirs(plotFolder)
    
 #   import datetime
    filename = (
 #       datetime.datetime.utcnow().strftime('%Y-%m-%d') + ' '+
        plotFile)
    filepath = os.path.join(
        plotFolder, filename)
    myfig.savefig(filepath + '.pdf', dpi=400)
 #   myfig.savefig(filepath + '.png', dpi=400)

    plt.close('all')