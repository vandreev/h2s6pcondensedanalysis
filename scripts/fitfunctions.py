# Df_Voigt and Df_Voigt_Doublet_LotharOrder from pyhs.fitfunc (package written by Lothar Maisenbacher), Df_Voigt function has been modified for different parameter order in the DoubleVoigt function

import numpy as np
import pandas as pd
import scipy.special

def Voigt(x, x0, amp, y0, gamma_l, gamma_g):
    ca = -2*np.sqrt(np.log(2))
    cb = np.sqrt(np.log(2))
    a = ca*(x-x0)/gamma_g
    b = cb*gamma_l/gamma_g
    y = amp/scipy.special.wofz(1j*b).real * scipy.special.wofz(a+1j*b).real + y0
    return y

def DoubleVoigt(x, x01, amp1, gamma1_l,gamma1_g,x02, amp2, y0):
    return Voigt(x, x01, amp1, y0/2, gamma1_l, gamma1_g)+Voigt(x, x02, amp2, y0/2, gamma1_l, gamma1_g)

def Lor(nu,nu0,amp,y0,gamma):
    return amp*(gamma/2)**2/((nu-nu0)**2+(gamma/2)**2)+y0

def lin_func(x,a,b):
    return x*a+b

def Df_Voigt(x, *p):
    """
    Partial derivatives for function `Voigt`.
    """
    x0 = p[0]
    amp = p[1]
    gamma_l = p[3]
    gamma_g = p[4]
    ca = -2*np.sqrt(np.log(2))
    cb = np.sqrt(np.log(2))
    a = ca*(x-x0)/gamma_g
    b = cb*gamma_l/gamma_g

    h = scipy.special.wofz(a+1j*b).real
    Invw = 1/scipy.special.wofz(1j*b).real
    V = amp*Invw * scipy.special.wofz(a+1j*b).real
    H1 = (-2*a) * scipy.special.wofz(a+1j*b).real + (2*b) * scipy.special.wofz(a+1j*b).imag
    H2 = (2*b) * scipy.special.wofz(a+1j*b).real + (2*a) * scipy.special.wofz(a+1j*b).imag - 2/np.sqrt(np.pi)

    Dfun = np.zeros((5, len(x)))
    Dfun[0, :] = -amp*Invw*ca/gamma_g * H1
    Dfun[1, :] = V/amp
    Dfun[2, :] = 1
    Dfun[3, :] = amp*Invw*cb/gamma_g * (-2*b*h+2*h*Invw/np.sqrt(np.pi)+H2)
    Dfun[4, :] = amp*Invw/gamma_g**2 * (2*cb*gamma_l*h* (b-Invw/np.sqrt(np.pi)) - H1*ca*(x-x0) - H2*cb*gamma_l)
    Dfun = Dfun.transpose()
    return Dfun

def Df_VoigtDoublet(x, *p):
    """
    Partial derivatives for function `VoigtDoublet`, order of parameters as in DoubleVoigt(..).
    """
    p1 = np.hstack((p[0],p[1],p[6], p[2], p[3]))
    p2 = np.hstack((p[4],p[5],p[6], p[2], p[3]))
    Dfun_1 = Df_Voigt(x, *p1).transpose()
    Dfun_2 = Df_Voigt(x, *p2).transpose()

    Dfun = np.zeros((7, len(x)))
    Dfun[0, :] = Dfun_1[0, :]
    Dfun[1, :] = Dfun_1[1, :]
    Dfun[6, :] = Dfun_1[2, :]/2 + Dfun_2[2, :]/2
    Dfun[2, :] = Dfun_1[3, :] + Dfun_2[3, :]
    Dfun[3, :] = Dfun_1[4, :] + Dfun_2[4, :]
    Dfun[4, :] = Dfun_2[0, :]
    Dfun[5, :] = Dfun_2[1, :]
    Dfun = Dfun.transpose()
    return Dfun


def DoubleVoigt_LotharOrder(x, x01, amp1, y0,gamma1_l,gamma1_g,x02, amp2):
    return Voigt(x, x01, amp1, y0/2, gamma1_l, gamma1_g)+Voigt(x, x02, amp2, y0/2, gamma1_l, gamma1_g)

def Df_VoigtDoublet_LotharOrder(x, *p):
    """
    Partial derivatives for function `VoigtDoublet`, order of parameters as in DoubleVoigt_LotharOrder(..).
    """
    p1 = p[:5]
    p2 = np.hstack((p[5:], p[2:5]))
    Dfun_1 = Df_Voigt(x, *p1).transpose()
    Dfun_2 = Df_Voigt(x, *p2).transpose()

    Dfun = np.zeros((7, len(x)))
    Dfun[0, :] = Dfun_1[0, :]
    Dfun[1, :] = Dfun_1[1, :]
    Dfun[2, :] = Dfun_1[2, :]/2 + Dfun_2[2, :]/2
    Dfun[3, :] = Dfun_1[3, :] + Dfun_2[3, :]
    Dfun[4, :] = Dfun_1[4, :] + Dfun_2[4, :]
    Dfun[5, :] = Dfun_2[0, :]
    Dfun[6, :] = Dfun_2[1, :]
    Dfun = Dfun.transpose()
    return Dfun

# Uncertainty of center frequency from Double Voigt fit (see derivation in Mathematica DoubleVoigtNu0uncert.nb)
def nu0uncert_DoubleVoigt(x1,x2,a1,a2,varx1,varx2,vara1,vara2,covx1x2,covx1a1,covx1a2,covx2a1,covx2a2,cova1a2):
    return np.sqrt((2*a1*a2*covx1x2)/(a1 + a2)**2 
                   + (a1**2*varx1)/(a1 + a2)**2 
                   + (a2**2*varx2)/(a1 + a2)**2 
                   + (2*a1*covx1a1*(x1/(a1 + a2) - (a1*x1 + a2*x2)/(a1 + a2)**2))/(a1 + a2) 
                   + (2*a2*covx2a1*(x1/(a1 + a2) - (a1*x1 + a2*x2)/(a1 + a2)**2))/(a1 + a2) 
                   + vara1*(x1/(a1 + a2) - (a1*x1 + a2*x2)/(a1 + a2)**2)**2 
                   + (2*a1*covx1a2*(x2/(a1 + a2) - (a1*x1 + a2*x2)/(a1 + a2)**2))/(a1 + a2) 
                   + (2*a2*covx2a2*(x2/(a1 + a2) - (a1*x1 + a2*x2)/(a1 + a2)**2))/(a1 + a2) 
                   + 2*cova1a2*(x1/(a1 + a2) - (a1*x1 + a2*x2)/(a1 + a2)**2)*(x2/(a1 + a2) - (a1*x1 + a2*x2)/(a1 + a2)**2) 
                   + vara2*(x2/(a1 + a2) - (a1*x1 + a2*x2)/(a1 + a2)**2)**2)

